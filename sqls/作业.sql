
-- 练习题目
-- 1.计算每个人的年薪并按照年薪的从高到底排序
SELECT
	ename,
	(sal + ifnull(comm, 0)) * 12 AS y_sal 
FROM
	emp 
ORDER BY
	y_sal DESC;


-- 2.查询奖金不为空的员工信息
SELECT * FROM emp WHERE comm is not NULL
SELECT * FROM emp WHERE isnull(comm) = 0



-- 3.查询月薪在[800,1500）之间的员工信息
SELECT
	* 
FROM
	emp 
WHERE
	(sal + ifnull(comm, 0)) >= 800 (sal + ifnull(comm, 0)) < 1500


-- 4.查询部门下的职位（去重部门编号、职位） 
SELECT
  deptno,
	job
FROM
  emp
GROUP BY
  deptno, job


select 
  distinct deptno, job
from
  emp


-- 5.查询薪水大于2000的员工信息，并且按照部门编号的升序、名字的降序方式排序
select * from emp where sal > 2000 order by deptno asc, ename desc


-- 6.查询薪水大于1500的员工个数
select count(*) from emp where sal > 1500


-- 7.查询薪水大于2500的员工所属部门的数量(高薪部门的个数)
select count(distinct deptno) from emp where sal > 2500

select 
  count(*)
from
(
  select 
	  deptno 
	from 
	  emp 
	where 
	  sal > 2500 
	group by d
	  eptno
) t1


-- 8.查询每个部门的平均薪水
-- 如果是分组聚合查询，出现在 select后面的字段，要么在group by 后，要么在聚会函数中
select deptno, avg(sal) "平均薪水", max(sal) max_sal, count(*) amount from emp group by deptno 

-- 9.查询每个部门岗位相同薪资最高的信息
select deptno, job, max(sal) from emp GROUP BY deptno, job


-- 10.按照部门编号进行分组求部门平均薪且平均薪水大于2000的，并按照部门编号升序排序

-- SQL语句执行顺序：先 from 然后 where 再 GROUP BY 再 SELECT 再 HAVING 最后 ORDER BY

select deptno, avg(sal) avg_sal from emp group by deptno HAVING avg_sal > 2000

select 
  *
from
(
  select deptno, avg(sal) avg_sal from emp group by deptno
) t1
where
  t1.avg_sal > 2000

-- 11.查询工资大于2000的雇员,按照部门编号进行分组,分组后平均薪水大于1500,按平均工资倒序排列
select deptno, avg(sal) avg_sal from emp where sal > 2000 group by deptno order by avg_sal

-- 12.查询那些薪水在平均薪水之上的员工信息
select * from emp where sal > (select avg(sal) avg_sal from emp)

-- 13.查找员工姓名和部门名称

-- SQL 92规范
select ename, dname from emp, dept where emp.deptno = dept.deptno

-- SQL 99规范
select 
  ename, 
  t1.deptno, 
  dname 
from 
  emp t1
join
  dept t2
on
  t1.deptno = t2.deptno

-- 14.查找每个部门挣钱最多的那个人的名字

select
  dname,
  ename,
	sal
from
(
  -- 每个部门薪水最高的员工信息
  select 
    t1.empno,
  	t1.deptno,
  	t1.ename,
  	t1.sal
  from 
    emp t1
  join
  (
  	-- 计算每个部门的最大薪水
    select 
  	  deptno, 
  		max(sal) max_sal
  	from 
  	  emp 
  	group by 
  	  deptno
  ) t2
  on
    t1.deptno = t2.deptno and t1.sal = t2.max_sal
) t3
join
  dept t4
on
  t3.deptno = t4.deptno	


-- 15.求员工的部门名称和薪水等级
select
    t3.*,
    grade
from
(
    select
        ename,
        dname,
        sal
    from
        emp t1
    join
        dept t2
    on
        t1.deptno = t2.deptno
) t3
join
    salgrade t4
on
    t3.sal >= t4.losal and t3.sal <= t4.hisal

-- 16.查询每个员工的上级领导
select
    t1.ename,
    t2.ename leader
from
    emp t1
join
    emp t2
on
    t1.mgr = t2.empno

-- 17.求部门平均薪水的等级
SELECT
    dname,
    avg_sal,
    grade
from
    (
    select
        t1.deptno,
        avg_sal,
        dname
    from
        (
            select
                deptno,
                avg(sal) avg_sal
            from
                emp
            group by
                deptno
        ) t1
        join
            dept t2
        on t1.deptno = t2.deptno
) t3
join
    salgrade t4
on
    t3.avg_sal >= t4.losal and t3.avg_sal <= t4.hisal


-- 18.求部门平均的薪水等级 
-- 先计算每个人的薪水等级，然后按照部门分组求薪水等级的平均数
SELECT
    deptno,
    AVG(grade) avg_grade
from
(
    SELECT
        deptno,
        grade
    FROM
        emp t1
    JOIN
        salgrade t2
    ON
        t1.sal BETWEEN t2.losal AND t2.hisal
) t3
GROUP BY
    deptno

-- 19.哪些人是领导

select
    *
from
    emp
where
    empno in
(
    select
        DISTINCT mgr
    from
        emp
    where
        mgr is not null
)

-- 20.求平均薪水最高的部门编号
SELECT
    *
from
(
    SELECT
        deptno,
        avg(sal) avg_sal
    FROM
        emp
    GROUP BY
        deptno
) t0
where avg_sal =
(
  SELECT
      max(avg_sal) max_avg_sal
  FROM
      (
          SELECT
              deptno,
              avg(sal) avg_sal
          FROM
              emp
          GROUP BY
              deptno
      ) t1
)

-- 使用having进行优化，就不需要多写一个子查询了

SELECT
    deptno,
    round(avg(sal), 6) avg_sal -- 使用四舍五入保留6位小数
FROM
    emp
GROUP BY
    deptno
HAVING avg_sal =
(
   SELECT
       max(avg_sal) max_avg_sal
   FROM
       (
           SELECT
               deptno,
               avg(sal) avg_sal
           FROM
               emp
           GROUP BY
               deptno
       ) t1
)

-- 21.求平均薪水最高的部门名称
-- 将20题结果再进行子查询，关联部门名称

-- 22.求比普通员工最高薪水还要高的经理人的名称

select
    *
from
    emp
where
    empno in
(
    select
        distinct mgr
    from
        emp
    where
        mgr is not null
)
and sal >
(
select
  max(sal)
from
  emp
where
      empno not in
      (
          select
              distinct mgr
          from
              emp
          where
              mgr is not null
      )
)

-- 23.工资5000及以上的职员，工资减少10% 工资在2000到4600之间的职员，工资增加15%

select
    ename,
    sal,
    case
        when sal >= 5000 then sal * (1-0.1)
        when sal BETWEEN 2000 and 4600 then sal * (1+0.15)
        else sal
    end as sal2
from
    emp;

-----

UPDATE emp
SET sal =
CASE
    WHEN sal >= 5000 THEN sal * 0.9
    WHEN sal >= 2000 AND sal < 4600 THEN salary * 1.15
    ELSE sal
END;


-- 创建视图

create view v_emp_dept_sal as
select
    ename,
    dname,
    grade,
    sal
from
    emp t1
join
    dept t2
on
    t1.deptno = t2.deptno
join
    salgrade t3
on
    t1.sal >= t3.losal and t1.sal <= t3.hisal


-- 创建视图
create view v_emp_dept_sal as
select
    ename,
    dname,
    grade,
    sal
from
    emp t1
join
    dept t2
on
    t1.deptno = t2.deptno
join
    salgrade t3
on
    t1.sal >= t3.losal and t1.sal <= t3.hisal

-- 从视图中查询
select * from v_emp_dept_sal where sal > 3000;


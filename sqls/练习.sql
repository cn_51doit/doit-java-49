

-- 创建一个数据库，指定字符集
create database doit49 charset utf8;

-- 使用doit49
use doit49;

-- 查看该数据库下的所有表
show tables;

-- 创建一个tb_user表，bigint类型相当于java中的long，varchar可变字符串
create table tb_user (id bigint, name varchar(20), age int, fv double);

show tables;

-- 查看表信息
desc tb_user;

-- 查看该表的建表语句
show create table tb_user;


-- 插入数据
insert into tb_user (id, name, age, fv) values (1000, '王心凌', 30, 99.99);
insert into tb_user values (1001, '柳岩', 32, 99.88); -- 当插入字段的顺序、个数、类型与表中字段一致，可以省略
insert into tb_user (id, name, fv) values (1002, '张天爱', 99.99);

-- 更新数据
-- 没有加where条件，所有的数据都被更新了，一定要小心
update tb_user set age = 18; 

-- 将id=1000的用户进行更新
update tb_user set age = 19, fv = 99.999 where id = 1000;

-- 将id > 1000 用户 年龄 + 10 岁
update tb_user set age = age + 10 where id > 1000

-- 查询年龄大于20岁的
select name, age, fv from tb_user where age > 20;

-- 删除数据
delete from tb_user where id = 1001;






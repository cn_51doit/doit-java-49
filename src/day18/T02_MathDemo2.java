package day18;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.Scanner;

/**
 * 统计一个文件中的数字最大值、最小值、合、Top3
 */
public class T02_MathDemo2 {

    public static void main(String[] args) throws FileNotFoundException {

        double sum = 0.0;
        double max = 0.0;
        double min = 0.0;

        //优先队列，可以排序，允许重复
        PriorityQueue<Double> priorityQueue = new PriorityQueue<>(4);

        Scanner scanner = new Scanner(new File("data/nums.txt"));
        boolean isFirst = true; //判断是否是第一次遍历出来的数据
        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            double num = Double.parseDouble(line);
            if (isFirst) {
                sum = num;
                max = num;
                min = num;
                isFirst = false;
            } else {
                sum += num;
                max = Math.max(max, num);
                min = Math.min(min, num);
            }
            priorityQueue.offer(num);
            if (priorityQueue.size() > 3) {
                priorityQueue.poll();
            }
        }

        //打印结果
        System.out.println("最大值：" + max + " , 最小值：" + min + " , 总和：" + sum);

        System.out.println(priorityQueue);

    }
}

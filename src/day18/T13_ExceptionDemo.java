package day18;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 *
 * 未检测异常 UnChecked Exception
 */
public class T13_ExceptionDemo {

    public static void main(String[] args)  {


        int num = Integer.parseInt("aaa"); //强制让你处理异常，但是为了程序的健壮，最好可能出现的异常进行处理


    }
}

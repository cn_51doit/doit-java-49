package day18;

public class T05_ExceptionDemo {

    public static void main(String[] args) {

        String line = "spark hive flink hadoop";
        //String line = null;

        String[] words = line.split(" ");

        System.out.println("~~~~~~~~~~~~~~~~~");

        String word = words[4];

        System.out.println("*****************");
        System.out.println(word);

    }
}

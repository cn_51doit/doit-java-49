package day18;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * 检测异常Checked Exception，必须try catch、或者 抛出异常
 * 未检测异常
 */
public class T12_ExceptionDemo {

    public static void main(String[] args) throws FileNotFoundException {

        File file = new File("data/abc.txt");

        Scanner scanner = new Scanner(file); //必须要处理的异常叫Checked Exception



    }
}

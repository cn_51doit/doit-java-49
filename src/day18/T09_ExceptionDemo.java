package day18;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 如果一段代码中有多个异常，可以根据异常的类型，进行catch，出现异常时，会进入到指定的catch
 */
public class T09_ExceptionDemo {

    public static void main(String[] args) {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        try {
            int nun = Integer.parseInt("a");
            Date date = sdf.parse("2024-10/10");
        } catch (NumberFormatException e) {
            System.out.println("数字转换异常");
            throw new RuntimeException(e);
        } catch (ParseException e) {
            System.out.println("日期转换异常");
            throw new RuntimeException(e);
        }


    }
}

package day18;

import day16.Zi;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * 统计一个文本中的偶数的数量、奇数的数量、字符串的数量
 */
public class T07_ExceptionDemo {

    public static void main(String[] args) throws FileNotFoundException {

        String reg1 = "[a-zA-Z]*";
        String reg2 = "[0-9]*";

        int strCount = 0;
        int numCount = 0;

        Scanner scanner = new Scanner(new File("data/exception.txt"));
        while (scanner.hasNext()) {

            String line = scanner.nextLine();
            String[] fields = line.split(" ");
            for (String str : fields) {

                if (str.matches(reg1)) {
                    //字符串
                    strCount++;
                }
                if(str.matches(reg2)) {
                    numCount++;
                }
            }


        }

        System.out.println("字符串的数量：" + strCount + " , 数字的数量：" + numCount);

    }
}

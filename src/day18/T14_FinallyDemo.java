package day18;

public class T14_FinallyDemo {

    public static int test(String str) {

        try {
            int num = Integer.parseInt(str);
            return num;
        } catch (NumberFormatException e) {
            //throw new RuntimeException(e);
            e.printStackTrace();
            return 0;
        } finally {
            System.out.println("finally被执行了！！！！！");
        }

    }

    public static int test2(String str) {

        try {
            int num = Integer.parseInt(str);
            return num;
        } finally {
            System.out.println("finally被执行了！！！！！");
        }

    }


    public static void main(String[] args) {


        int res = test2("aaa");
        System.out.println(res);

    }
}

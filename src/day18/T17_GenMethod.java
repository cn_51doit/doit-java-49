package day18;

/**
 * 泛型方法
 */
public class T17_GenMethod {

    public static Integer test(Integer i) {
        return i * 10;
    }

    /**
     * <T> 定义一个T类型的泛型
     *  test2前面的t代表返回值类型
     * test2中的参数代表参数的类型
     */
    public static <T> T test2(T t)  {
        return t;
    }
}

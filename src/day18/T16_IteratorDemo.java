package day18;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class T16_IteratorDemo {

    public static void main(String[] args) {

        //Arrays是个工具类，将数组转成List
        List<Integer> list = Arrays.asList(1, 2, 3, 4 ,5, 6, 7, 8, 9);

        //遍历数组 fori 增强for循环
//        for (int i = 0; i < list.size(); i++) {
//            Integer ele = list.get(i);
//            System.out.println(ele);
//        }

//        for (Integer ele : list) {
//            System.out.println(ele);
//        }

        //需要注意的是，迭代器是取数据的工具，而不是将集合中的数据都放到迭代器中
        Iterator<Integer> iterator = list.iterator();
        while (iterator.hasNext()) { //如果返回ture，说明，结合中有未取出来数据
            Integer ele = iterator.next();
            System.out.println(ele);
        }

    }
}

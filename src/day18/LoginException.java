package day18;

public class LoginException extends Exception {

    public LoginException() {
        super("登录异常!");
    }

    public LoginException(String msg) {
        super(msg);
    }
}

package day18;

import java.util.LinkedList;

/**
 * 增删快，查找慢
 */
public class T18_LinkedList {

    public static void main(String[] args) {

        LinkedList<String> list = new LinkedList<>();
        list.add("spark");
        list.add("hive");
        list.add("hadoop");
        list.add("hue");



        list.addFirst("aaa"); //添加到最前面
        list.addLast("www"); //添加到最后面

        for (String ele : list) {
            System.out.println(ele);
        }

        String first = list.getFirst(); //取出第一个元素
        System.out.println(first);

        String last = list.getLast(); //取出最后一个元素
        System.out.println(last);

        String first1 = list.removeFirst();//删除第一个元素
    }
}

package day18;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 如果一段代码中有多个异常，可以根据异常的类型，进行catch，出现异常时，会进入到指定的catch
 */
public class T10_ExceptionDemo {

    public static void main(String[] args) {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        try {
            int nun = Integer.parseInt("12");
            Date date = sdf.parse("2024-10-10");
            String str = null;
            String[] words = str.split(" ");
        } catch (NumberFormatException | ParseException e) {
            e.printStackTrace();
            System.out.println("进入到数字转换异常或日期格式异常");
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("进入到最大的异常了");
        }


    }
}

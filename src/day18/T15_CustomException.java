package day18;

import java.util.Random;

/**
 * 自定义异常
 */
public class T15_CustomException {

    public static void main(String[] args) {

        //执行登录功能
        try {
            login("tom", "123456");
        } catch (LoginException e) {
            //e.printStackTrace();
            //通知用户
            System.out.println(e.getMessage());
        }

    }

    public static void login(String name, String password) throws LoginException {

        Random random = new Random();
        int i = random.nextInt(10);
        if (i > 5) {
            throw new LoginException("用户名或密码错误");
        }

    }
}

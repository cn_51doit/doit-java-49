package day18;

import java.math.BigInteger;

public class T03_BigIntegerDemo {

    public static void main(String[] args) {

        BigInteger b1 = new BigInteger("123123123123");
        BigInteger b2 = new BigInteger("456456456456");

        //相加
        BigInteger r1 = b1.add(b2);
        System.out.println(r1);

        //相减
        BigInteger r2 = b1.subtract(b2);
        System.out.println(r2);

        //相乘
        BigInteger r3 = b1.multiply(b2);
        System.out.println(r3);

        //相除
        BigInteger r4 = b1.divide(b2);
        System.out.println(r4);


    }
}

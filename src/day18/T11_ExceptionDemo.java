package day18;

public class T11_ExceptionDemo {


    public static void m1(String str) {

        int num = 0;
        try {
            num = Integer.parseInt(str);
        } catch (NumberFormatException e) {
            //throw new RuntimeException(e);
        }

        System.out.println(num);

    }


    /**
     * 出现异常，为了通知方法的调用者，可以将异常抛出去
     * @param str
     * @throws NumberFormatException
     */
    public static void m2(String str) throws NumberFormatException {

        int num = Integer.parseInt(str);


        System.out.println(num);

    }

    public static void main(String[] args) {

        //m1("abc");

        try {
            m2("abc");
        } catch (NumberFormatException e) {
           //e.printStackTrace();
            throw e; //抛出异常
        }

        System.out.println(6666666);
    }
}

package day18;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * 统计一个文本中的偶数的数量、奇数的数量、字符串的数量
 */
public class T08_ExceptionDemo {

    public static void main(String[] args) throws FileNotFoundException {


        int strCount = 0;
        int evenCount = 0; //偶数
        int oddCount = 0;  //计算

        Scanner scanner = new Scanner(new File("data/exception.txt"));
        while (scanner.hasNext()) {

            String line = scanner.nextLine();
            String[] fields = line.split(" ");
            for (String str : fields) {

                try {
                    int num = Integer.parseInt(str);
                    if (num % 2 == 0) {
                        evenCount++;
                    } else {
                        oddCount++;
                    }
                } catch (NumberFormatException e) {
                    strCount++;
                }


            }


        }

        System.out.println("字符串的数量：" + strCount + " , 偶数的数量：" + evenCount + " ，奇数的数量：" + oddCount);

    }
}

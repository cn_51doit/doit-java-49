package day18;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * 忽略有问题的数据
 */
public class T14_ExceptionDemo {

    public static void main(String[] args) throws FileNotFoundException {

        Scanner scanner = new Scanner(new File("data/nums2.txt"));

        double sum = 0.0;

        while (scanner.hasNextLine()) {

            String line = scanner.nextLine();

            double money = 0;
            try {
                money = Double.parseDouble(line);
            } catch (NumberFormatException e) {
                //throw new RuntimeException(e);
                continue; //跳出本次循环，进行下一次循环
            }

            sum += money;

        }


        System.out.println(sum);
    }
}

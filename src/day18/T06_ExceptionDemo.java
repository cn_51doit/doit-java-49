package day18;

public class T06_ExceptionDemo {

    public static void main(String[] args) {

        String str = "abc";
        int num = 0;
        try {
            num = Integer.parseInt(str);
            System.out.println("没有出现异常");
        } catch (NumberFormatException e) {
            System.out.println("出现了异常信息！！！！");
            e.printStackTrace(); //打印错误的详细信息
        }

        System.out.println(num);

    }
}

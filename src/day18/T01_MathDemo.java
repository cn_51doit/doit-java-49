package day18;

/**
 * 数学运算相关的工具类
 */
public class T01_MathDemo {

    public static void main(String[] args) {

        int r1 = Math.min(5, 8);
        System.out.println(r1);

        double r2 = Math.max(8.8, 9.9);
        System.out.println(r2);

        //取绝对值
        //|-5.5| = 5.5
        double r3 = Math.abs(-5.5);
        System.out.println(r3);

        //向上取整
        double r4 = Math.ceil(5.9); //6
        System.out.println(r4);

        //向下取整
        double r5 = Math.floor(6.9); //6
        System.out.println(r5);

        //四舍五入(只保留整数)
        long r6 = Math.round(3.14); //3
        System.out.println(r6);

        //求平方
        double r7 = Math.pow(4, 2);
        System.out.println(r7);

        double r8 = Math.sqrt(16);
        System.out.println(r8);


    }
}

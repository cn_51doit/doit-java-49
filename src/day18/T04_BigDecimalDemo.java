package day18;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class T04_BigDecimalDemo {

    public static void main(String[] args) {

        BigDecimal big1 = new BigDecimal("0.05");
        BigDecimal big2 = new BigDecimal("0.06");
        //加法计算
        BigDecimal add = big1.add(big2);
        System.out.println("求和:"+add);
        //减法计算
        BigDecimal sub = big1.subtract(big2);
        System.out.println("求差:"+sub);
        //乘法计算
        BigDecimal mul = big1.multiply(big2);
        System.out.println("乘法:"+mul);
        //除法计算
        BigDecimal div = big1.divide(big2, 4, RoundingMode.HALF_UP);
        System.out.println(div);


        Double d = 6.6; //将基本类型转成了引用类型，自动装箱

        //Double aDouble = new Double(6.6);
        double r = Double.valueOf(5.5); //将引用转成了基本类型， 自动拆箱

    }
}

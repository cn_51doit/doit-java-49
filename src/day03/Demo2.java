package day03;

public class Demo2 {

    public static void main(String[] args) {

        String[] arr1 = new String[4];

        arr1[0] = "hadoop";
        //arr1[1] = "flink"; //会被覆盖，无意义
        arr1[1] = "spark";

        String s1 = arr1[1];
        if (s1 == null) {
            System.out.println("返回的s1为null");
        }

        //如果s1为null，会抛出空指针异常
//        if (s1.equals("spark")) {
//            System.out.println("s1的内容为spark");
//        }

        if("spark".equals(s1)) {
            System.out.println("s1的内容为spark");
        }





        //s1调用方法，但是s1是null，所以抛出了空指针异常
        //String res1 = s1.substring(2);



        System.out.println(s1);

        //System.out.println(res1);

    }
}

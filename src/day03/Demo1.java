package day03;



public class Demo1 {

    public static void main(String[] args) {

        //定义变量
        int i = 1;
        //int i = 10; //有错误i已经定义过了
        int j = 10;

        i = 10; //给i重新赋值

        String str1 = "abcedfg";
        String str2 = new String("ABCEDFG");

        String res = str1.substring(2);

        System.out.println(res);


    }
}

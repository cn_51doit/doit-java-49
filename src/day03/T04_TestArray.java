package day03;

public class T04_TestArray {

    public static void main(String[] args) {

        //存储文件中存在的单词
        String[] wordArr = new String[4];
        //存储对应下标单词的次数
        int[] countArr = new int[4];

        String line = "spark spark hive flink spark spark hive hadoop hive flink";

        //对字符串进行切割
        String[] words = line.split(" ");

        for (int i = 0; i < words.length; i++) {
            //取出来一个单词
            String word = words[i];
            //判断取出来的单词是否在wordArr中是否存在
            int index = ArrayUtils.indexOf(wordArr, word);
            if(index == -1) { //元素不在该数组中
                index = ArrayUtils.add(wordArr, word);
            }
            //根据第一个数组，取出对应下标的值，就是该单词对应的次数
            int historyCount = countArr[index];
            //累加后进行赋值
            countArr[index] = historyCount + 1;
        }

        for (int i = 0; i < wordArr.length; i++) {
            System.out.println(wordArr[i] + "的次数：" + countArr[i]);
        }







    }
}

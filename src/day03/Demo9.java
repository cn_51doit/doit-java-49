package day03;

public class Demo9 {

    public static int indexOf(String[] arr, String target) {
        int index = -1;
        for (int i = 0; i < arr.length; i++) {
            String ele = arr[i]; //取出的元素
            if (target.equals(ele)) {
                index = i;
                break; //跳出循环
            }
        }
        return index;
    }

    public static void main(String[] args) {

        String[] words = {"spark", "hive", null, "flink", "hadoop", "hue"};

        int index = indexOf(words, "flume");

        System.out.println(index);
        String word = words[index];
        System.out.println(word);
    }
}

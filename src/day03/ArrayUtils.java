package day03;


public class ArrayUtils {

    public static int indexOf(String[] arr, String word) {
        int index = -1;
        for (int i = 0; i < arr.length; i++) {
            String s = arr[i];
            //如果循环处理的元素与查找的一致
            if (word.equals(s)) {
                index = i;
                break; //跳出循环
            }
        }
        return index; //返回查找到的下标
    }

    public static int add(String[] arr, String element) {

        int index = -1;
        for (int i = 0; i < arr.length; i++) {
            String s = arr[i];
            if (s == null) { //判断字符串是否为null
                arr[i] = element; //将指定的元素添加的指定的位置
                index = i; //返回添加的元素在数组中的下标
                break; //跳出循环
            }
        }
        return index; //返回下标
    }

    public static void copy(String[] source, String[] dist) {
        for (int i = 0; i < source.length; i++) {
            dist[i] = source[i];
        }
    }

}

package day03;



public class Demo4 {

    public static void main(String[] args) {

        //调用其他类中public静态方法，使用类名.方法名()
        int res = Demo3.add(3, 6);

        System.out.println(res);

        String line = "123456789";

        String res2 = Demo3.substr(line, 2, 6);

        System.out.println(res2);

    }
}

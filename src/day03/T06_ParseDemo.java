package day03;

public class T06_ParseDemo {

    public static void main(String[] args) {

        String s1 = "10000";

        double d1 = Double.parseDouble(s1);

        System.out.println(d1);

        String s2 = "2024";

        int i2 = Integer.parseInt(s2);
        System.out.println(i2);

    }
}

package day03;

public class Demo3 {

    public static int add(int a, int b) {
        return a + b;
    }

    public static String substr(String line, int start, int end){
        return line.substring(start, end);
    }

    public static void main(String[] args) {

        int r = add(3, 5);

        System.out.println(r);

        String str = "ABCEDFGH";

        String r2 = substr(str, 2, 4);

        System.out.println(r2);

    }
}

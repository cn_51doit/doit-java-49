package day03;

public class Demo7 {

    public static void main(String[] args) {

        int[] arr = {10, 20, 30, 40, 50, 60, 70, 80, 90};


        for (int i = 0; i < arr.length; i++) {
            System.out.println(arr[i]);
        }

        int i = 0;
        while (i < arr.length) {
            System.out.println(arr[i]);
            i++;
            if(i == 5) {
                break; //跳出循环
            }
        }
    }
}

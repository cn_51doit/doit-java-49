package day03;

/**
 * 向指定的字符串数组中添加元素，从头开始添加，必须添加到null的位置，然后返回对应的下标
 */
public class T03_ArrayAdd {

    public static int add(String[] arr, String element) {
        int index = -1;
        for (int i = 0; i < arr.length; i++) {
            String s = arr[i];
            if (s == null) { //判断字符串是否为null
                arr[i] = element; //将指定的元素添加的指定的位置
                index = i; //返回添加的元素在数组中的下标
                break; //跳出循环
            }
        }
        return index; //返回下标
    }

    public static void main(String[] args) {

        String[] words = new String[4];
        //将数组和要添加的元素传入到add方法中
        int index = add(words, "spark");
        System.out.println(index);

        int index2 = add(words, "hive");
        System.out.println(index2);

        int index3 = add(words, "hive");
        System.out.println(index3);

        int index4 = add(words, "hadoop");
        System.out.println(index4);

        int index5 = add(words, "hue");
        System.out.println(index5);


    }
}

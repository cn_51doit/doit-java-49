package day03;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class T05_WordCountV2 {

    public static void main(String[] args) throws FileNotFoundException {
        int lineCount = 0;
        int wordCount = 0;
        //定义一个数组，用来保存以后出现的单词
        String[] wordArr = new String[8];
        int[] countArr = new int[8];
        //创建一个File对象
        File f = new File("data/words.txt");
        //创建一个读取数据的工具
        Scanner scanner = new Scanner(f);

        //循环读取文件中的内容
        while (scanner.hasNextLine()) {
            //读取数据
            String line = scanner.nextLine();
            //切分
            String[] words = line.split(" ");
            for(int i = 0; i < words.length; i++) {
                String word = words[i];
                //取出的单词不是error
                if(!word.equals("error")) {
                    //判断该单词是否在wordArr中存在
                    int index = ArrayUtils.indexOf(wordArr, word);
                    if (index == -1) { //该单词不在wordArr中
                        index = ArrayUtils.add(wordArr, word);
                    }
                    //到countArr中取出对应下标的数字，就是该单词的次数
                    int historyCount = countArr[index];
                    //historyCount++; //有错误
                    //countArr[index] = ++historyCount; //赋值 i++ 先赋值，然后++， ++i 先++，再赋值
                    countArr[index] = historyCount + 1;
                    //累加单词的数量
                    wordCount++;
                }
            }
            //累加函数
            lineCount++;
        }

        System.out.println("该文件中有" + lineCount +"行，单词的数量：" + wordCount);
        for (int i = 0; i < wordArr.length; i++) {
            String word = wordArr[i];
            if (word != null) {
                System.out.println(word +"单词出现的数量：" + countArr[i]);
            }
        }
    }
}

package day03;

public class Demo12 {


    public static void main(String[] args) {

        String[] words = new String[8];
        words[0] = "spark";
        words[1] = "hive";
        words[2] = "flink";
        words[3] = "hbase";

        int[] counts = new int[8];
        counts[0] = 38;
        counts[1] = 22;
        counts[2] = 55;
        counts[3] = 17;

        //flink单词再次出现
        //返回的是第一个数组的下标
        String s = "flume";
        int index = ArrayUtils.indexOf(words, s);
        //查找的元素不在words数组内
        if (index == -1) {
            //将该元素添加到数组中
            index = ArrayUtils.add(words, s);
        }
        //由于在第二个数组相同的下标位置保存了对应单词的次数
        int historyCount = counts[index];
        System.out.println(historyCount);
        historyCount++;
        //将累加后的数据在赋值到指定的位置
        counts[index] = historyCount;

        //取出累加后的数据
        System.out.println(counts[index]);

    }

}

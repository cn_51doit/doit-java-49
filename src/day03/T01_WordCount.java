package day03;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class T01_WordCount {

    public static void main(String[] args) throws FileNotFoundException {
        int lineCount = 0;
        int wordCount = 0;
        int sparkCount = 0;
        int hiveCount = 0;
        int flinkCount = 0;
        int hadoopCount = 0;
        int hueCount = 0;



        //创建一个File对象
        File f = new File("data/words.txt");
        //创建一个读取数据的工具
        Scanner scanner = new Scanner(f);

        //循环读取文件中的内容
        while (scanner.hasNextLine()) {
            //读取数据
            String line = scanner.nextLine();
            //切分
            String[] words = line.split(" ");
            for(int i = 0; i < words.length; i++) {
                String word = words[i];
                //取出的单词不是error
                if(!word.equals("error")) {
                    //判断是否是spark
                    if(word.equals("spark")) {
                        sparkCount++;
                    } else if (word.equals("hive")) {
                        hiveCount++;
                    } else if (word.equals("hadoop")) {
                        hadoopCount++;
                    } else if (word.equals("file")) {
                        flinkCount++;
                    } else if (word.equals("hue")) {
                        hueCount++;
                    }
                    //累加单词的数量
                    wordCount++;
                }
            }
            //累加函数
            lineCount++;
        }

        System.out.println("该文件中有" + lineCount +"行，单词的数量：" + wordCount + ", spark单词的数量:" + sparkCount + ", hive单词的数量：" + hiveCount);
    }
}

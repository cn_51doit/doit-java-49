package day03;

/**
 * 在Demo10中调用Demo9的indexOf方法
 */
public class Demo10 {

    public static void main(String[] args) {

        String[] words = {"aaa", "bbb", "ccc", "d", "e"};

        int index = Demo9.indexOf(words, "cc");
        System.out.println(index);
    }
}

package day03;

public class Demo8 {

    public static void main(String[] args) {

        int[] arr = {1,2,3,4,5,6,7,8,9};

        //将arr中为偶数的乘以100后，在放到原来对应的位置
        for (int i = 0; i < arr.length; i++) {
            int r = arr[i];
            if (r % 2 == 0) { // %模运算，其实就是相除后取余数
              arr[i] = r * 100;
            }
        }

        //System.out.println(arr);

        //将arr中的内容打印出来，显示在控制台，方便查看
        for (int i = 0; i < arr.length; i++) {
            System.out.println(arr[i]);
        }


    }
}

package day03;

/**
 * 调用Demo5的add方法
 */
public class Demo6 {

    public static void main(String[] args) {

        //Demo5中的add方法是非静态的方法（成员方法），必须先创建对象（实例）
        //然后通过对象名称调用非静态的方法
        Demo5 d = new Demo5();
        int res = d.add(5, 8);

        System.out.println(res);


    }
}

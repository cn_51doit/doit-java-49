package day03;

public class Demo11 {

    public static int append(String[] arr, String element) {
        int index = -1;
        for (int i = 0; i < arr.length; i++) {
            String ele = arr[i];
            if (ele == null) {
                arr[i] = element;
                index = i;
                break;
            }
        }
        return index;
    }

    public static void main(String[] args) {

        String[] words = new String[4];
        words[0] = "aaa";
        words[1] = "bbb";

        int index = append(words, "ccc");

        System.out.println(index);

    }
}

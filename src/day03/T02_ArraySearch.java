package day03;

/**
 * 从指定的数组中查找相应的元素，并且返回对应的下标，如果没有查找到，返回-1
 */
public class T02_ArraySearch {

    public static int indexOf(String[] arr, String word) {
        int index = -1;
        for (int i = 0; i < arr.length; i++) {
            String s = arr[i];
            //如果循环处理的元素与查找的一致
            if (s.equals(word)) {
                index = i;
                break; //跳出循环
            }
        }
        return index; //返回查找到的下标
    }

    public static void main(String[] args) {

        //定义一个数组
        String[] words = {"hive", "hadoop", "flink", "spark", "hue"};

        //查找指定的元素
        String searchWord = "Spark";

        int index = indexOf(words, searchWord);

        System.out.println(index);



    }
}

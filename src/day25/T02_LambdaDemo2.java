package day25;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;

public class T02_LambdaDemo2 {

    public static void main(String[] args) {

        ArrayList<Integer> list = new ArrayList<>(Arrays.asList(4, 9, 2, 1, 7, 5, 3, 8, 6));

        list.sort(new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                return -(o1 - o2);
            }
        });

        System.out.println(list);

        //list.sort((Integer o1, Integer o2) -> o2 - o1);
        //list.sort((o1, o2) -> o2 - o1); //如果方法有返回值，Lambda表达式就一行，return关键字可以省略
        list.sort((o1, o2) -> { //如果方法有返回值，Lambda表达式中的代码有很多行，一定要有return关键字
            //...很多代码
           return o2 - o1;
        });
        System.out.println(list);
    }
}

package day25;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Optional;
import java.util.function.Supplier;

public class T11_StreamMax {

    public static void main(String[] args) {

        //求List中的最大值，有以下两种方式，你觉得哪一种更好
        ArrayList<Integer> list = new ArrayList<>(Arrays.asList(4, 9, 2, 1, 7, 5, 3, 8, 6));

        //1.先降序排序，取出第一个元素就是最大的
        list.sort((a, b) -> b - a);
        Integer max = list.get(0);
        System.out.println(max);

        //2.将数据迭代出来，两两比较，将比较后大的值，攒起来，再跟下一个元素进行比较，一直循环下去，比到最后
        Integer acc = null;
        boolean isFirst = true;
        for (Integer e : list) {
            if(isFirst) {
                acc = e;
                isFirst = false;
            } else {
               acc = Math.max(acc, e);
            }
        }
        System.out.println(acc);

        //3.使用Stream编程
        Optional<Integer> opt = list.stream()
                //.filter(e -> e > 100)
                .max((a, b) -> a - b);
        //Integer res = opt.get();
        Integer res = opt.orElseGet(() -> -1);
        System.out.println(res);


    }
}

package day25;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class T10_StreamDemo4 {

    public static void main(String[] args) {

        //将集合中的偶数取出来乘以10
        ArrayList<Integer> list = new ArrayList<>(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10));

        ArrayList<Integer> newList = new ArrayList<>();
        for (Integer e : list) {
            //先应用过来逻辑
            if(e % 2 == 0) {
                //将符合条件的数据进行映射
                int r = e * 10;
                //将新的结果添加到新的List中
                newList.add(r);
            }
        }


        //Stream<Integer> stream1 = list.stream();
        //Stream<Integer> stream2 = stream1.filter(e -> e % 2 == 0);
        //Stream<Integer> stream3 = stream2.map(e -> e * 10);
        //List<Integer> newList = stream3.collect(Collectors.toList());

        List<Integer> nList = list.stream()
                .filter(e -> e % 2 == 0) //应用外部传入的过滤逻辑进行过滤
                .map(e -> e * 10) //在应用传入的转换逻辑进行映射
                .collect(Collectors.toList());


    }
}

package day25;

public class T01_LambdaDemo1 {

    public static void main(String[] args) {

        //第一种写法
        new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("run方法执行了");
            }
        }).start();

        //第二中写法

        //第二中写法
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                System.out.println("run方法执行了~~~~");
            }
        };
        new Thread(runnable).start();

        Runnable runnable2 = () -> System.out.println("run方法执行了~~~~");
        new Thread(runnable2).start();

        //第三种
        new Thread(new MyTask()).start();

        //第四种：Lambda表达式
        //第一种写法
        new Thread(
                () -> System.out.println("run方法执行了%%%%")
        ).start();

    }


    private static class MyTask implements Runnable {

        @Override
        public void run() {
            System.out.println("Run方法执行了#####");
        }
    }
}

package day25;

public interface FilterFunction {

    boolean apply(String in);
}

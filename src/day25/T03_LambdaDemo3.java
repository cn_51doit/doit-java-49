package day25;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Random;
import java.util.concurrent.*;

public class T03_LambdaDemo3 {

    public static void main(String[] args) throws ExecutionException, InterruptedException {

        ExecutorService threadPool = Executors.newFixedThreadPool(5);

//        Future<Integer> future = threadPool.submit(new Callable<Integer>() {
//            @Override
//            public Integer call() throws Exception {
//                Thread.sleep(3000);
//                return new Random().nextInt(10);
//            }
//        });

        Future<Integer> future = threadPool.submit(() -> {
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                //throw new RuntimeException(e);
                e.printStackTrace();
            }
            return new Random().nextInt(10);
        });

        Integer res = future.get();
        System.out.println(res);

    }
}

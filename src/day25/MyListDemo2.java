package day25;

import java.util.Arrays;
import java.util.List;

/**
 * 不使用Java8的Stream编程API和lambda表达式，实现类似StreamAPI的map、filter、flagMap、reduce等功能
 */
public class MyListDemo2 {

    public static void main(String[] args) {

        List<String> list = Arrays.asList(
                "spark hive flink hbase hadoop",
                "hive hive flink spark",
                "flink flink flink flink",
                "hadoop hadoop hive hbase",
                "hbase hadoop spark flink hive"
        );

        MyList myList = new MyList(list);

        List<String> nList = myList.flatMap(in -> Arrays.asList(in.split(" ")));

        for (String w : nList) {
            System.out.println(w);
        }

    }
}

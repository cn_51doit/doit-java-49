package day25.adv;

public interface MyFunction2<A, B, C> {

   C apply(A a, B b);
}

package day25.adv;

import java.util.ArrayList;

public class MyAdvList<T> extends ArrayList<T> {

    public <R> MyAdvList<R> map(MyFunction<T, R> function) {
        //1.定义一个新的List
        MyAdvList<R> nList = new MyAdvList<>();
        //2.循环老的List
        for (T t: this) {
            //3.应用外部传入的逻辑
            R r = function.apply(t);
            //4.将放到的几个添加到新的List中
            nList.add(r);
        }
        //5.返回新的List
        return nList;
    }

    public MyAdvList<T> filter(MyFunction<T, Boolean> func) {
        //1.定义新的List
        MyAdvList<T> nList = new MyAdvList<>();
        //2.循环老的List
        for (T t : this) {
            //3.应用外部的过滤逻辑
            Boolean flag = func.apply(t);
            if (flag) {
                //4.将符合条件的数据添加到新的List中
                nList.add(t);
            }
        }
        //5.返回新的List
        return nList;
    }

    public <R> MyAdvList<R> flatMap(MyFunction<T, Iterable<R>> func) {
        //1.定义一个新的List
        MyAdvList<R> nList = new MyAdvList<>();
        //2.循环老的List
        for (T t : this) {
            //3.应用外部传入的逻辑
            Iterable<R> it = func.apply(t);
            //4.将放到额结果进行循环
            for (R r : it) {
                //5.将循环的元素添加到新的List
                nList.add(r);
            }
        }
        //5.返回新的List
        return nList;
    }

    public T reduce(MyFunction2<T, T, T> func) {
        //1.定义一个累加的中间结果
        T acc = null;
        boolean isFirst = true;
        //2.循环List中的元素
        for (T t : this) {
            if (isFirst) {
                acc = t;
                isFirst = false;
            } else {
                //3.应用外部的聚合逻辑
                acc = func.apply(acc, t);
            }
        }
        //4.返回
        return acc;
    }

}

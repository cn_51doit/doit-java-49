package day25.adv;

import java.util.ArrayList;
import java.util.Arrays;

public class MyAdvListDemo {

    public static void main(String[] args) {

        MyAdvList<Integer> nums = new MyAdvList<>();
        nums.addAll(Arrays.asList(1,2,3,4,5,6,7,8,9));

        //ArrayList<Integer> nList = nums.map(e -> e * 10);

        MyAdvList<Integer> nList = nums.filter(e -> e % 2 == 0)
                .map(e -> e * 10);

        for (Integer e : nList) {
            System.out.println(e);
        }
    }
}

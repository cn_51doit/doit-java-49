package day25.adv;

public interface MyFunction<T, R> {

   R apply(T t);
}

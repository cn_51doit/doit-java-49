package day25.adv;

import java.util.Arrays;

public class MyAdvListDemo3 {

    public static void main(String[] args) {

        MyAdvList<Integer> nums = new MyAdvList<>();
        nums.addAll(Arrays.asList(1,2,3,4,5,6,7,8,9));

        Integer res = nums.filter(e -> e % 2 == 0)
                .map(e -> e * 10)
                .reduce((a, b) -> a + b);

        System.out.println(res);
    }
}

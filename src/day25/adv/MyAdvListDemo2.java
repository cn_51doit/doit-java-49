package day25.adv;

import java.util.Arrays;

public class MyAdvListDemo2 {

    public static void main(String[] args) {

        MyAdvList<String> lines = new MyAdvList<>();
        lines.add("spark hive flink hadoop");
        lines.add("hive flink flink hadoop");
        lines.add("hive hive hadoop flink hadoop");

        MyAdvList<String> nList = lines.flatMap(in -> Arrays.asList(in.split(" ")));

        for (String e : nList) {
            System.out.println(e);
        }

    }
}

package day25;



import day10.Person;

import java.util.ArrayList;
import java.util.Comparator;

public class T04_LambdaDemo4 {

    public static void main(String[] args) {

        ArrayList<Person> list = new ArrayList<>();
        list.add(new Person("柳岩", 33, 99.99));
        list.add(new Person("唐嫣", 18, 99.88));
        list.add(new Person("金莲", 38, 100));
        list.add(new Person("大郎", 48, 50));

        list.sort((p1, p2) -> Double.compare(p2.getFv(), p1.getFv()));








    }
}

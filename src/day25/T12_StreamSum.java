package day25;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Optional;
import java.util.function.BinaryOperator;

public class T12_StreamSum {

    public static void main(String[] args) {

        //求List中的最大值，有以下两种方式，你觉得哪一种更好
        ArrayList<Integer> list = new ArrayList<>(Arrays.asList(4, 9, 2, 1, 7, 5, 3, 8, 6));

//        Optional<Integer> reduced = list.stream().reduce(new BinaryOperator<Integer>() {
//            @Override
//            public Integer apply(Integer a, Integer b) {
//                return a + b;
//            }
//        });

        //Optional<Integer> reduced = list.stream().reduce((a, b) -> a + b);
        Optional<Integer> reduced = list.stream().reduce(Integer::sum);

        //求最小值
        //list.stream().reduce((a, b) -> Math.min(a, b));
        //list.stream().reduce(Math::min);

        Integer sum = reduced.orElse(0);

        System.out.println(sum);


    }
}

package day25;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class MyList{

    private List<String> list;

    public MyList(List<String> list) {
        this.list = list;
    }

    public List<String> map(MapFunction func) {
        //1.定义一个新的List
        ArrayList<String> nList = new ArrayList<>();
        //2.循环老的List
        for (String word : list) {
            //3.应用外部传入的运算逻辑
            String res = func.apply(word);
            //4.将返回的结果添加到新的List中
            nList.add(res);
        }
        //5.返回新的List
        return nList;
    }

    public List<String> filter(FilterFunction func) {
        //1.定义一个新的List
        ArrayList<String> nList = new ArrayList<>();
        //2.循环老的List
        for (String word : list) {
            //3.应为传入的过滤逻辑
            boolean flag = func.apply(word);
            if (flag) {
                //4.将返回true的数据添加到新的List中
                nList.add(word);
            }
        }
        //5.返回新的List
        return nList;
    }

    public List<String> flatMap(FlatMapFunction func) {
        //1.定义一个新的List
        ArrayList<String> nList = new ArrayList<>();
        //2.循环老的List
        for (String line : list) {
            //3.应用外部传入的逻辑
            Iterable<String> it = func.apply(line);
            //4.将返回的每一个数据取出来，添加到新的List
            for (String e : it) {
                //5.将返回的元素添加到新的List中
                nList.add(e);
            }
        }
        //5.返回新的List
        return nList;
    }
}

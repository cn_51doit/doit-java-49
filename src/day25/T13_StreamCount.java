package day25;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Optional;

public class T13_StreamCount {

    public static void main(String[] args) {

        //求Stream流中数据的个数
        ArrayList<Integer> list = new ArrayList<>(Arrays.asList(4, 9, 2, 1, 7, 5, 3, 8, 6));

        long count = list.stream().count();

        System.out.println(count);


    }
}

package day25;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 不使用Java8的Stream编程API和lambda表达式，实现类似StreamAPI的map、filter、flagMap、reduce等功能
 */
public class MyListDemo {

    public static void main(String[] args) {

        List<String> list = Arrays.asList("spark", "hive", "flink", "hadoop", "hbase");

        //List<String> nList = list.stream().map(String::toUpperCase).collect(Collectors.toList());

        //不使用Java8的Stream编程API和lambda表达式，将上面list中的每一个元素转成大写
        //定义一个MyList，将原来的List进行增强
        MyList myList = new MyList(list);

//        List<String> nList = myList.map(new MapFunction() {
//            @Override
//            public String apply(String in) {
//                return in.toUpperCase();
//            }
//        });

//        List<String> nList = myList.map(new MapFunction() {
//            @Override
//            public String apply(String in) {
//                return in + "2.0";
//            }
//        });

        List<String> nList = myList.map(e -> e.toUpperCase());

        //List<String> nList = myList.filter(in -> in.startsWith("h"));

        for (String word : nList) {
            System.out.println(word);
        }
    }
}

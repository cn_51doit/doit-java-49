package day25;

import java.util.Iterator;

public interface FlatMapFunction {

   Iterable<String> apply(String in);
}

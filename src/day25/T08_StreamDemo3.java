package day25;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class T08_StreamDemo3 {

    public static void main(String[] args) {

        //将集合中的偶数取出来乘以10
        ArrayList<Integer> list = new ArrayList<>(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10));

//        Stream<Integer> stream2 = list.stream().filter(new Predicate<Integer>() {
//            @Override
//            public boolean test(Integer in) {
//                return in % 2 == 0;
//            }
//        });

        Stream<Integer> stream2 = list.stream().filter(e -> e % 2 == 0);

        Stream<String> stream3 = stream2.map(new Function<Integer, String>() {
            @Override
            public String apply(Integer in) {
                return in.toString();
            }
        });

        List<String> newList = stream3.collect(Collectors.toList());



        //思路：先过滤，再映射
        //List<Integer> nList = list.stream().filter(e -> e % 2 == 0).map(a -> a * 10).collect(Collectors.toList());

        //System.out.println(nList);
    }
}

package day25;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class T14_StreamWordCount {

    public static void main(String[] args) throws IOException {

        File file = new File("data/words.txt");
        List<String> lineList = FileUtils.readLines(file, StandardCharsets.UTF_8);
        lineList.stream()
                .flatMap(line -> Arrays.stream(line.split(" ")))
                .collect(Collectors.groupingBy(e -> e))
                .forEach((key, value) -> System.out.println(key + ", " + value.size()));


    }
}

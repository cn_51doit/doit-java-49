package day25;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class T06_StreamDemo {

    public static void main(String[] args) {

        ArrayList<Integer> list = new ArrayList<>(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10));

        //需求，将原来List中的每一个元素乘以10，然后放到一个新的List中
        ArrayList<Integer> nList = new ArrayList<>();

        //循环老的List
        for (int i = 0; i < list.size(); i++) {
            Integer num = list.get(i); //取出老的List中的元素
            int newNum = num * 10; //应用运算逻辑，得到新的值
            nList.add(i, newNum); //将新的值添加到新的List中
        }

        System.out.println("老的List：" + list);
        System.out.println("新的List：" + nList);


        //Stream编程时JDK8退出的功能，可以将一个集合变成一个流（对集合进行流水线操作）

        //调用集合的stream()方法，会返回一个流对象，就可以对Stream流中的每一个元素依次进行流水线操作了
        Stream<Integer> stream = list.stream();

        //做映射，一个Stream调用map后，返回一个新的Stream
        Stream<Integer> stream2 = stream.map(new Function<Integer, Integer>() {
            @Override
            public Integer apply(Integer i) {
                return i * 10;
            }
        });

        List<Integer> newList = stream2.collect(Collectors.toList());

        System.out.println(newList);

    }
}

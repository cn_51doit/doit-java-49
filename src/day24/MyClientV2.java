package day24;

import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MyClientV2 {

    public static void main(String[] args) throws Exception {

        //解析传入该main方法的的参数
        //第一个为IP地址
        String host = args[0];
        //第二个为端口号
        int port = Integer.parseInt(args[1]);
        //创建客户端Socket，即向指定的IP和端口建立连接对象
        Socket socket = new Socket(host, port);

        //获取与服务端建立的连接对象，获取输出流，即向服务端发送计算逻辑
        OutputStream outputStream = socket.getOutputStream();
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream);

        //将要计算的文件放入到一个结合中
        List<String> files = Arrays.asList(
                "D:\\idea\\project\\data\\math\\1.txt",
                "D:\\idea\\project\\data\\math\\2.txt",
                "D:\\idea\\project\\data\\math\\3.txt"
        );

        //循环将多个Task发送到服务端
        for (String file : files) {
            //创建Task对象，将文件名称传入到Task的构造方法中
            Task task = new Task(file);
            //将Task序列化发送到服务端
            objectOutputStream.writeObject(task); //循环的向服务端发送多个Task对象
        }
        //如果以null结束，说明该发的Task已经完成了
        objectOutputStream.writeObject(null);

        //获取输入流，未来接收服务返回的数据的流
        InputStream inputStream = socket.getInputStream();
        ObjectInputStream objectInputStream = new ObjectInputStream(inputStream);

        Long acc = 0L;

        Object obj = null;
        //读取服务端返回的对象，如果是null，说明该读取的对象已经完成了
        while ((obj = objectInputStream.readObject()) != null) {
            Long res = (Long) obj; //将返回的结果转出指定的类型
            //System.out.println("服务端返回的结果：" + res);
            acc += res; //进行汇总累加
        }

        System.out.println("客户端累加的最终结果：" + acc);


    }
}

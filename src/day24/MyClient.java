package day24;

import java.io.*;
import java.net.Socket;

public class MyClient {

    public static void main(String[] args) throws Exception {

        String host = args[0];
        int port = Integer.parseInt(args[1]);

        Socket socket = new Socket(host, port);

        //向服务端发送计算逻辑
        OutputStream outputStream = socket.getOutputStream();
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream);

        //未来接收服务返回的数据的流
        InputStream inputStream = socket.getInputStream();
        ObjectInputStream objectInputStream = new ObjectInputStream(inputStream);


        Task task = new Task("D:\\idea\\project\\data\\math\\1.txt"); //创建一个对象（实例）
        //客户端将一个Java对象，通过网络发送到服务端，即将一个Java对象，转成二进制发送过去
        //必须将Task类实现Serializable
        //将Java对象转成二进制，叫做序列化，再通过网络发送给服务端
        objectOutputStream.writeObject(task);

        Long max = (Long) objectInputStream.readObject();

        System.out.println("返回给客户端的数据：" + max);


    }
}

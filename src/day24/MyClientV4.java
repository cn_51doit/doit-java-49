package day24;

import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class MyClientV4 {

    public static void main(String[] args) throws Exception {

        //为了简化，目前有两个服务端,地址分别是localhost:8899和localhost:9988
        //在localhost:8899这台机器上，要分配两个Task，用来计算1.txt和2.txt
        //在localhost:9988这台机器上，要分配两个Task，用来计算2.txt和4.txt
        HashMap<String, List<String>> serverToFileList = new HashMap<>();
        //第一个服务端对应要计算的文件
        serverToFileList.put("192.168.5.133:8888", Arrays.asList("C:\\dev\\IdeaProjects\\JavaBase\\res\\math\\3.txt", "C:\\dev\\IdeaProjects\\JavaBase\\res\\math\\4.txt"));
        //第二个服务端对应要计算的文件
        serverToFileList.put("192.168.5.135:9999", Arrays.asList("D:\\idea\\project\\data\\math\\1.txt", "D:\\idea\\project\\data\\math\\2.txt"));

        ExecutorService threadPool = Executors.newFixedThreadPool(4);

        LinkedList<Future<Long>> futures = new LinkedList<>();


        //使用for循环，读取map中的数据，分别与指定的服务端建立连接，然后发送计算任务
        for (Map.Entry<String, List<String>> entry : serverToFileList.entrySet()) {

            String address = entry.getKey();
            List<String> files = entry.getValue();
            String[] hostAndPort = address.split(":");
            String host = hostAndPort[0];
            int port = Integer.parseInt(hostAndPort[1]);

            //最好的解决方案：每跟一个服务端建立一个连接，就开启一个新的线程

            Future<Long> future = threadPool.submit(new ResultTask(host, port, files));

            //将每个server返回的future添加到LinkedList中
            futures.add(future);

        }


        Long acc = 0L;

        while (futures.size() > 0) {
            for (Future<Long> future : futures) {
                if(future.isDone()) {
                    Long res = future.get();
                    acc += res;
                    futures.remove(future);
                    break;
                }
            }
            Thread.sleep(200);
        }

        System.out.println("最终的结果：" + acc);

        threadPool.shutdown();
    }

    private static class ResultTask implements Callable<Long> {

        private String host;
        private int port;
        private List<String> files;

        public ResultTask(String host, int port, List<String> files) {
            this.host = host;
            this.port = port;
            this.files = files;
        }

        @Override
        public Long call() throws Exception {
            Socket socket = new Socket(host, port);
            OutputStream outputStream = socket.getOutputStream();
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream);

            for (String file : files) {
                Task task = new Task(file);
                objectOutputStream.writeObject(task); //将Task序列化，发送给服务端
            }
            objectOutputStream.writeObject(null);

            //获取服务端返回的结果
            InputStream inputStream = socket.getInputStream();
            ObjectInputStream objectInputStream = new ObjectInputStream(inputStream);

            Long acc = 0L;

            Object obj = null;
            //读取服务端返回的结果，该方法是阻塞的
            while ((obj = objectInputStream.readObject()) != null) {
                Long res = (Long) obj;
                acc += res;
            }
            return acc;
        }
    }
}

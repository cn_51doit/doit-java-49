package day24;

import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MyClientV3 {

    public static void main(String[] args) throws Exception {

        //为了简化，目前有两个服务端,地址分别是localhost:8899和localhost:9988
        //在localhost:8899这台机器上，要分配两个Task，用来计算1.txt和2.txt
        //在localhost:9988这台机器上，要分配两个Task，用来计算2.txt和4.txt
        HashMap<String, List<String>> serverToFileList = new HashMap<>();
        //第一个服务端对应要计算的文件
        serverToFileList.put("localhost:8899", Arrays.asList("data/math/1.txt", "data/math/2.txt"));
        //第二个服务端对应要计算的文件
        serverToFileList.put("localhost:9988", Arrays.asList("data/math/3.txt", "data/math/4.txt"));

        Long acc = 0L;
        //使用for循环，读取map中的数据，分别与指定的服务端建立连接，然后发送计算任务
        for (Map.Entry<String, List<String>> entry : serverToFileList.entrySet()) {

            String address = entry.getKey();
            List<String> files = entry.getValue();
            String[] hostAndPort = address.split(":");
            String host = hostAndPort[0];
            int port = Integer.parseInt(hostAndPort[1]);

            //最好的解决方案：每跟一个服务端建立一个连接，就开启一个新的线程

            Socket socket = new Socket(host, port);

            OutputStream outputStream = socket.getOutputStream();
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream);

            for (String file : files) {
                Task task = new Task(file);
                objectOutputStream.writeObject(task); //将Task序列化，发送给服务端
            }
            objectOutputStream.writeObject(null);

            //获取服务端返回的结果
            InputStream inputStream = socket.getInputStream();
            ObjectInputStream objectInputStream = new ObjectInputStream(inputStream);

            Object obj = null;
            //读取服务端返回的结果，该方法是阻塞的
            while ((obj = objectInputStream.readObject()) != null) {
                Long res = (Long) obj;
                acc += res;
            }
        }

        System.out.println("最后返回的结果：" + acc);

        //Thread.sleep(5000);

    }
}

package day24;

import java.io.File;
import java.io.Serializable;
import java.util.Random;
import java.util.Scanner;

public class Task implements Serializable {

    private String path;

    public Task(String path) {
        this.path = path;
    }

    public Long evaluate(String path) throws Exception {

        File file = new File(path);
        Scanner scanner = new Scanner(file);
        boolean isFirst = true;
        Long acc = null;
        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            long num = Long.parseLong(line);
            if (isFirst) {
                acc = num;
                isFirst = false;
            } else {
                //max = Math.max(max, num);
                acc = acc + num;
            }
        }
        scanner.close();
        //模拟程序执行需要一点时间，
        Thread.sleep(new Random().nextInt(500) + 2000);
        return acc;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}

package day24;

import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.LinkedList;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * 负责执行计算逻辑的服务端
 */
public class MyServerV3 {

    public static void main(String[] args) throws Exception {

        ExecutorService threadPool = Executors.newFixedThreadPool(4);

        LinkedList<Future<Long>> futures = new LinkedList<>();

        //接收main方法传入的参数，将参数转成int类型
        int port = Integer.parseInt(args[0]);
        //创建一个ServerSocket，传入指定的端口
        ServerSocket serverSocket = new ServerSocket(port);

        //等待客户端建立连接
        Socket socket = serverSocket.accept();

        //接收客户端发送过来的消息（封装着计算逻辑的消息）
        InputStream inputStream = socket.getInputStream();
        ObjectInputStream objectInputStream = new ObjectInputStream(inputStream);

        //获取输出流（未来服务端将数据发生给客户端的流）
        OutputStream outputStream = socket.getOutputStream();
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream);

        //循环接收客户端发送的Task对象，如果读取到null，Task发送完成
        Object obj = null;
        while ((obj = objectInputStream.readObject()) != null) {
            Task task = (Task) obj;
            //读取到一个Task，将Task提交到线程中进行运算
            Future<Long> future = threadPool.submit(new Callable<Long>() {
                @Override
                public Long call() throws Exception {
                    Long result = task.evaluate(task.getPath());
                    System.out.println("服务端计算"+ task.getPath() +"文件对应的结果：" + result);
                    return result;
                }
            });
            //将返回的future添加到LinkedList中
            futures.add(future);
        }

        System.out.println("---------------------");

        //循环等待，Futures中的任务都已经执行完成了
        while (futures.size() > 0) {
            for (Future<Long> future : futures) {
                if (future.isDone()) { //当前的Future已经返回结果了
                    Long res = future.get();
                    //服务端将计算好结果返回给客户端
                    System.out.println("写回客户端的数据：" + res);
                    objectOutputStream.writeObject(res);
                    //将使用后的future中，从LinkedList中移除
                    futures.remove(future);
                    break;
                }
            }
            Thread.sleep(200);
        }
        //服务端再写一个特殊的对象，就是null，说明服务端要发送的数据都已经发送完成了
        objectOutputStream.writeObject(null);


        //关闭服务端，释放资源
        threadPool.shutdown();
        objectOutputStream.close();
        outputStream.close();
        objectInputStream.close();
        inputStream.close();
        socket.close();
        serverSocket.close();
    }
}

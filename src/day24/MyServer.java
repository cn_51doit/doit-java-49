package day24;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * 负责执行计算逻辑的服务端
 */
public class MyServer {

    public static void main(String[] args) throws Exception {

        int port = Integer.parseInt(args[0]);
        ServerSocket serverSocket = new ServerSocket(port);

        Socket socket = serverSocket.accept();

        //接收客户端发送过来的消息（封装着计算逻辑的消息）
        InputStream inputStream = socket.getInputStream();
        ObjectInputStream objectInputStream = new ObjectInputStream(inputStream);

        //获取输出流（未来服务端将数据发生给客户端的流）
        OutputStream outputStream = socket.getOutputStream();
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream);


        //客户端发送给服务端的计算逻辑
        //将网络发送过来的二进制，转成Java对象，反序列化
        Task task = (Task) objectInputStream.readObject();

        //在服务端调用Task中的计算逻辑，然后返回结果
        Long max = task.evaluate(task.getPath());

        //System.out.println(max);

        //最后将计算好的结果返回给客户端
        objectOutputStream.writeObject(max);

        objectOutputStream.close();
        objectInputStream.close();
        socket.close();



    }
}

package day16;

/**
 * 创建内部类的实例（对象）前一定会创建外部的的实例（对象）
 *
 * 在内部类中，可以使用外部类的属性和方法
 */
public class Person {

    private boolean live = true;


    class Heart {
        public int count;

        public void getLive() {
            System.out.println(live);
        }
    }

    public static void main(String[] args) {

        //
        //Heart heart1 = new Person().new Heart();

        Person person = new Person();
        System.out.println(person.live);

        Heart heart = person.new Heart();

        int count = heart.count;
        heart.getLive();
        System.out.println(count);

    }



}

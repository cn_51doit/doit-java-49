package day16;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

/**
 * java中，方法传值和传引用（地址）
 */
public class Test03 {


    public static void changeValue(int a) {
        a = 10;
        System.out.println("方法内部a的值：" + a);
    }

    public static void changeValue(DataBean bean) { //调用方法会创建一个局部变量，将传入的变量赋值给局部变量
        bean.num = 888;
        bean.name = "多易";
    }

    public static void changeValue2(DataBean bean) { //0x8899 调用方法会创建一个局部变量，将传入的变量赋值给局部变量
        System.out.println("changeValue2方法中，没有赋值之前的bean的地址" + bean);
        bean = new DataBean(555, "doe"); //0x9900
        System.out.println("changeValue2方法中，赋值之后的bean的地址" + bean);
    }


    public static void changeValue(String[] arr, String ele) {
        //System.out.println(arr);
        arr[0] = ele;
    }

    public static void changeValue(HashMap<String, Integer> map) {
        map.put("AAA", 888);
        map.put("BBB", 999);
    }

    public static void addValue(ArrayList<String> list, String ele) {
        list.add(ele);
    }


    public static void main(String[] args) {

        int i = 100;
        System.out.println("int类型的i修改前的值：" + i);
        changeValue(i); //将i传入到changeValue方法中
        System.out.println("int类型的i修改后的值：" + i); //i还是100

        //自定义的类型是引用类型
        DataBean data = new DataBean(88, "doit");
        //DataBean data2 = data;
        System.out.println("引用类型修改前：" + data.name + "," + data.num);
        //changeValue(data);
        changeValue2(data);
        System.out.println("引用类型修改后："+ data.name + "," + data.num);

        //数组也是引用类型
        String[] words = {"spark", "hive", "flink", "hadoop"};
        changeValue(words, "多易大数据");
        System.out.println(Arrays.toString(words));

        //HashMap是引用类型
        HashMap<String, Integer> mp = new HashMap<>();
        mp.put("AAA", 8);
        mp.put("CCC", 9);
        changeValue(mp);
        System.out.println(mp);

        //ArrayList是引用类型
        ArrayList<String> wordsList = new ArrayList<>();
        wordsList.add("spark");
        wordsList.add("flink");
        Test03.addValue(wordsList, "kafka");
        System.out.println(wordsList);

    }


    private static class DataBean {

        public int num;
        public String name;

        public DataBean(int num, String name) {
            this.num = num;
            this.name = name;
        }
    }

}

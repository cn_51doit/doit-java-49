package day16;

import day15.GameBean;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

/**
 * 使用TreeSet实现游戏充值金额TopN的功能
 * 扩展知识：有界优先队列
 */
public class GameCountV3 {

    public static void main(String[] args) throws FileNotFoundException {
        //key 游戏id,分区id
        //value 金额
        HashMap<String, Double> gidAndZidToMoney = new HashMap<>();
        //key 游戏ID
        //value 装了多个分区id和Money的BeanBean TresSet
        HashMap<String, TreeSet<StaticInnerGameBean>> gidToZidAndMoneySet = new HashMap<>();

        File file = new File("data/game.txt");

        Scanner scanner = new Scanner(file);
        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            String[] fields = line.split(",");
            String gid = fields[0]; //游戏ID
            String zid = fields[1]; //分区ID
            double money = Double.parseDouble(fields[3]);
            //将gid和zid进行拼接
            String key = gid + "," + zid;
            Double accMoney = gidAndZidToMoney.getOrDefault(key, 0.0);
            accMoney += money;
            //将累加后的数据put到gidAndZidToMoney
            gidAndZidToMoney.put(key, accMoney);
        }

        //将累加后的数据，将结果方法TresSet中进行排序
        for (Map.Entry<String, Double> entry : gidAndZidToMoney.entrySet()) {
            String gidAndZid = entry.getKey();
            //切分
            String[] fields = gidAndZid.split(",");
            String gid = fields[0];
            String zid = fields[1];
            Double money = entry.getValue();
            //将数据添加到Set中
            StaticInnerGameBean bean = new StaticInnerGameBean(zid, money);
            TreeSet<StaticInnerGameBean> treeSet = gidToZidAndMoneySet.get(gid);
            if (treeSet == null) {
                treeSet = new TreeSet<>(new Comparator<StaticInnerGameBean>() {
                    @Override
                    public int compare(StaticInnerGameBean o1, StaticInnerGameBean o2) {
                        return Double.compare(o2.getMoney(), o1.getMoney());
                    }
                });
                //第一次创建的TreeSet put到gidToZidAndMoneySet
                gidToZidAndMoneySet.put(gid, treeSet);
            }
            //将封装好数据的GameBean添加到treeSet中，会自动排序
            treeSet.add(bean);
            //添加元素后，如果长度大于3，就移除到最后一个
            if (treeSet.size() > 3) {
                //由于是降序排序，取出最后一个就是最小的
                StaticInnerGameBean last = treeSet.last();
                //移除最小的
                treeSet.remove(last);
            }
        }

        //遍历结果
        for (Map.Entry<String, TreeSet<StaticInnerGameBean>> entry : gidToZidAndMoneySet.entrySet()) {
            String key = entry.getKey();
            TreeSet<StaticInnerGameBean> treeSet = entry.getValue();
            //将一个集合转成迭代器（用来迭代数据的工具）
            Iterator<StaticInnerGameBean> iterator = treeSet.iterator();
            int rank = 1;
            while (iterator.hasNext()) {
                StaticInnerGameBean next = iterator.next();
                System.out.println(key +"," + next.getZid() + "," + next.getMoney() + "," + rank);
                rank++;
            }

        }

    }


    private static class StaticInnerGameBean {

        private String zid;

        private double money;

        public StaticInnerGameBean() {}

        public StaticInnerGameBean(String zid, double money) {
            this.zid = zid;
            this.money = money;
        }

        public String getZid() {
            return zid;
        }

        public void setZid(String zid) {
            this.zid = zid;
        }

        public double getMoney() {
            return money;
        }

        public void setMoney(double money) {
            this.money = money;
        }

    }

}

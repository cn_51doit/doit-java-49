package day16;

/**
 * 如果一个类，仅在一个类中使用，并且还不想创建一个java文件，那么就可以使用静态内内部类，有点事减少java源文件的数量
 *
 * 在静态内部类中，可以方法外部类中静态的属性
 *
 */
public class People {

    private static int num = 888;

    private int age = 10;

    private String gender = "male";

    private void setAge(int age) {
        this.age = age;
    }



    //静态内部类(不需要创建外部类的实例（对象），就可以创建内部类的实例)
    public static class Dog{

        public String name;
        private int age;

        public void printNum() {
            System.out.println(num);
        }

    }


    public static void main(String[] args) {

        //People.Dog dog = new People.Dog();
        Dog dog = new Dog();
        System.out.println(dog.age); //创建静态内部类的实例，就可以访问内部类的属性和方法了


        //Dog2 dog2 = new Dog2();
        //System.out.println(dog2.getName());

    }

}



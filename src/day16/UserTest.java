package day16;

public class UserTest {

    public static void main(String[] args) {

        System.out.println("最开始静态变量的值为："+ User.num);

        User u1 = new User();
        System.out.println("u1 的 age的默认值：" + u1.age);
        //修改u1的age为10
        u1.age = 10;
        System.out.println("u1 的 age修改后的值：" + u1.age);

        //修改User类的num（静态变量）
        User.num = 888;

        System.out.println("修改后的静态的num ：" + User.num);

        User u2 = new User();

        System.out.println("u2age的默认值：" + u2.age);
        u2.age = 18;
        System.out.println("u2age的修改后的值：" + u2.age);

        System.out.println("修改后的静态的num ：" + User.num);




    }
}

package day16;

public class Fu {

    {
        System.out.println("父类的构造代码块执行了~");
    }

    static {
        System.out.println("父类的【静态】代码块执行了！");
    }

    public Fu() {
        System.out.println("父类的构造方法执行了@");
    }
}

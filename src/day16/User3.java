package day16;

/**
 * 构造代码块
 *
 * new 对象时，会按照先后顺序，初始化成员变量、和执行构造代码块，最后在执行构造方法中的内容
 */
public class User3 {

    public int age;
    //没有用static修饰的{}，叫做构造代码块
    {
        System.out.println("age:" + age);
        System.out.println("name:" + this.name); //null
        System.out.println("构造代码 1 执行了");
    }

    public User3(int age) {
        this.age = age; //
    }

    public String name = "tomcat";

    public static void main(String[] args) {
        System.out.println("main方法执行了！");

        User3 user3 = new User3(18);
        System.out.println(user3);
    }


    {
        System.out.println("在次打印name：" + name);
        System.out.println("构造代码 2 执行了");
    }
}

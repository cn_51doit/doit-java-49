package day16;

public class Son extends Father {

    public Son(int age) {
        this.age = age;
    }

    public static void main(String[] args) {

        Son son = new Son(10);

        int salary = son.salary;
        System.out.println(salary);
        System.out.println(son.age);

        //System.out.println(son.money);
    }
}

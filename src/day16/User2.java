package day16;

public class User2 {

    //new的时候会被初始化
    public int age = 0;

    //类加载是会初始化静态变量和静态代码块，会按照先后顺序进行执行
    public static int num = 888;

    //静态代码块
    static {
        //System.out.println(age);
        User2.sayHello();
        System.out.println(User2.NAME);
        System.out.println("静态代码块1中使用num的值" + num);
        System.out.println("静态代码块 1 执行了");
        num = 999;

    }


    public static void sayHello() {
        System.out.println("hello everybody");
    }



    public static String NAME = "用户";

    public static void main(String[] args) {
        System.out.println("main方法执行了~");
        User2 user2 = new User2();
        System.out.println("User的实例user2的age值为：" + user2.age);

        User2.sayHello();
    }


    //静态代码块在类加载时，会按照先后顺序执行
    //静态代码块
    static {
        System.out.println("静态代码块2中使用num的值" + num);
        System.out.println("静态代码块 2 执行了");
    }








}

package day16;

/**
 * 父类的【静态】代码块执行了！
 * 子类的【静态】代码块执行了
 * 父类的构造代码块执行了~
 * 父类的构造方法执行了@
 * 子类的构造代码块执行了
 * 子类的构造方法执行了~
 * day16.Zi@6bc7c054
 */
public class Test01 {

    public static void main(String[] args) {

        Zi zi = new Zi();
        System.out.println(zi);

    }
}

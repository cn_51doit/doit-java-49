package day16;

public class DefaultTest {

    public static void main(String[] args) {

        DefaultDemo defaultDemo = new DefaultDemo();
        //defaultDemo.sayHi(); // 方法是私有的
        defaultDemo.sayHello(); // 访问权限为default类型，只能在同一个包中使用

        Father father = new Father();
        father.printSalary();
    }
}

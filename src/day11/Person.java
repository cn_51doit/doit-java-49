package day11;

public class Person {

    public void eat() {
        System.out.println(this);
    }

    public void printName(String name) {

    }

    //int i, string s, Person p
    public void printPerson(Person p) {
        System.out.println("当前对象的地址" + this);
        System.out.println("传入对象的地址" + p);
    }

    public static Person getPerson() {
        //Person person = new Person();
        //return person;
        return new Person();
    }

    public static void main(String[] args) {

        //new Person().eat();
        //new Person().eat();

        //Person p = new Person();
        //p.eat();

        //p.printPerson(new Person());


        Person person = getPerson();
        System.out.println(person);

        //一大堆其他了逻辑
    }
}

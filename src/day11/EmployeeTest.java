package day11;

public class EmployeeTest {

    public static void main(String[] args) {

        //抽象类不能像以前那样的new了，抽象类不能直接实例化
        //Employee employee = new Employee(); //报错

        Teacher t = new Teacher();
        t.eat(); //调用父类的方法，这个方法是继承来的
        t.work(); //子类实现的方法

        t.name = "老赵";

    }
}

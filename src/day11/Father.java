package day11;

public class Father {

    int num = 5;

    {
        System.out.println("父类的构造代码块");
    }

    public Father() {
        System.out.println("父类空参的构造方法执行了");
    }

    public Father(int num) {
        this.num = num;
        System.out.println("父类有参的构造方法执行了");
    }


    protected void printNum() {
        System.out.println("num的值为：" + num);
    }

}

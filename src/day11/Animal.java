package day11;

/**
 * 定义一个类，加Animal（动物）
 *
 * private的属性和方法是不能被继承的
 */
public class Animal {

    String name;
    int age;
    double weight;

    public void breath() {
        System.out.println("呼吸空气");
    }

    public void run() {
        System.out.println("开始移动");
    }

}

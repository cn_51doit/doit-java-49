package day11;

public class TestAnimal {

    public static void main(String[] args) {

        Monkey monkey = new Monkey();
        monkey.name = "大马猴";
        monkey.age = 10;
        monkey.weight = 30.0;
//
//        Dog dog = new Dog();
//        dog.name = "黑子";
//        dog.age = 8;
//        dog.weight = 20;

        monkey.breath(); //父类继承的
        monkey.run(); //自己重写的


    }
}

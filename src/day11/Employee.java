package day11;

/**
 * 1.抽象方法必须定义在抽象类中
 * 2.抽象类中可以定义抽象方法，也可以定义非抽象的方（就是有方法体的具体方法）
 * 3.抽象类一定要被子类继承，抽象的方法一定要在子类中实现
 */
public abstract class Employee {


    public String name;
    private int age;

    //抽象的方法
    public abstract void work();

    //非抽象的方法
    public void eat() {
        System.out.println("吃饭");
    }
}

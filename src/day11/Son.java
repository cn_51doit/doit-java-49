package day11;

public class Son extends Father{

    int num = 8;

    //构造代码块，优先于构造方法执行
    {
        System.out.println("子类的构造代码块");
    }

    public Son() {
        super();
        System.out.println("子类空参的构造方法执行了");
    }

    public Son(int num) {
        //super(); 写不写都一样
        this.num = num;
        System.out.println("子类有参的构造方法执行了");
    }

    public void printNum() {
        //System.out.println(num);
        System.out.println("父类的num：" + super.num);
        System.out.println("son对象num: " + this.num);
    }

    public int getNum() {
        return num;
    }
}

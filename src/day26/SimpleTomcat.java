package day26;

import java.io.*;
import java.lang.reflect.Method;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class SimpleTomcat {

    //装URL和方法的映射
    /**
     * /user/add=day26.User.add
     * /user/del=day26.User.delete
     * /product/add=day26.Product.add
     * /product/update=day26.Product.update
     */
    private static HashMap<String, Method> urlToMethod = new HashMap<>();
    //装命名空间和对应实例的映射
    /**
     * user = User
     * product = Product
     */
    private static HashMap<String, Object> nameToServet = new HashMap<>();

    public static void main(String[] args) throws Exception {
        ServerSocket serverSocket = new ServerSocket(8080);
        System.out.println("Server started on port 8080");

        //加载配置文件
        Properties properties = new Properties();
        //读取文件中的配置到properties
        properties.load(new FileInputStream("conf/web.properties"));

        //循环，将配置文件中的url和方法的映射，添加到urlToMethod这个Map中
        for (Map.Entry<Object, Object> entry : properties.entrySet()) {
            //获取配置文件中的key
            String url = entry.getKey().toString();
            String nameSpace = url.split("/")[1];

            //获取配置文件中的value
            String value = entry.getValue().toString();

            //截取value中的类
            String clazzName = value.substring(0, value.lastIndexOf("."));
            //截取value中的方法名
            String methodName = value.substring(value.lastIndexOf(".") + 1);
            //通过反射，创建方法对象
            Class<?> clazz = Class.forName(clazzName);

            Object servlet = nameToServet.get(nameSpace);
            if (servlet == null) {
               servlet = clazz.newInstance();
               nameToServet.put(nameSpace, servlet);
            }

            //获取类的自己码
            Method method = clazz.getDeclaredMethod(methodName);
            //将请求的URL和Method进行映射
            urlToMethod.put(url, method);
        }


        while (true) {
            // 接受客户端的连接请求
            Socket socket = serverSocket.accept();

            System.out.println("Client connected");

            //定义一个方法，用于处理请求
            handleRequest(socket);

            // 关闭客户端连接
            socket.close();
        }
    }

    private static void handleRequest(Socket clientSocket) throws Exception {
        // 读取客户端发送的请求内容
        BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

        //获取请求参数
        String line = in.readLine();
        //System.out.println("请求的数据：" + line);
        /**
         * /user/add
         */
        String url = line.split(" ")[1];

        if (url.equals("/favicon.ico")) {
            return;
        }


        /**
         * /user/add 切分后返回的是 user
         */
        String nameSpace = url.split("/")[1];
        //找到对应的对象了（非静态方法的载体找到了）
        Object servlet = nameToServet.get(nameSpace);
        //根据请求的URL，即/user/add到map中找对应的方法对象
        Method method = urlToMethod.get(url);

        StringBuilder response = new StringBuilder();


        if (method != null) {
            response.append("HTTP/1.1 200 OK\n"); // 响应行
            response.append("Content-Type: text/json\n\n"); // 响应头

            //通过反射调用方法
            String res = (String) method.invoke(servlet); //将对应的载体，即所属对象传入到invoke方法中
            //返回客户端的结果
            response.append(res); // 响应体
        } else {
            response.append("HTTP/1.1 404 OK\n"); // 响应行
        }


        // 发送响应回客户端
        OutputStream out = clientSocket.getOutputStream();
        out.write(response.toString().getBytes());
        out.flush();
        out.close();


    }
}

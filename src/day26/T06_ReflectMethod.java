package day26;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

/**
 * 通过反射获取类的方法
 */
public class T06_ReflectMethod {

    public static void main(String[] args) throws Exception {

        //通过反射，获取类的字节码
        Class<?> clazz = Class.forName("day26.Student");

        //通过类的字节码，获取所有方法对象
        //Method[] methods = clazz.getMethods();

//        for (Method method : methods) {
//            System.out.println(method);
//        }

        //根据名称，获取到指定名称的方法对象
        Method method = clazz.getMethod("main", String[].class);
        //静态方法不属于任何对象
        method.invoke(null, new Object[]{new String[]{"aaa", "bbb"}});


        //获取非静态的方法，study，如果要调用分静态的方法，必须先创建对象，然后再调用方法
        Method method1 = clazz.getMethod("study");
        //Student s = null;
        //s.study(); //报空指针错误
        //先创建对象,非静态的方法必须有一个载体（必须归属到某个具体的对象/实例）
        //Student student = new Student("tom", 10);
        Object student = clazz.newInstance();
        method1.invoke(student); //传入的student对象，调用method1方法（Method method1是对study1方法的描述）

        //获取有参的方法
        Method method2 = clazz.getMethod("eat", String.class, double.class);
        method2.invoke(student, "abc", 5.5);

        //调用toString方法
        Method method3 = clazz.getMethod("toString");
        Object res = method3.invoke(student);
        System.out.println(res);

        Method method4 = clazz.getDeclaredMethod("sayHello");
        //暴力反射
        method4.setAccessible(true);
        method4.invoke(student);


    }
}

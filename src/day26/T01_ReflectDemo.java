package day26;

public class T01_ReflectDemo {

    public static void main(String[] args) throws Exception {

        //传统创建类的实例
        //1.加载类的字节码（放到方法去）
        //2.根据类的字节码，创建类的实例（对象），new出来的对象是放在堆内存中的
        //Student student = new Student();
        //Student student = new Student("tom", 18);
        //3.调用方法
        //student.study();

        //Student8 student8 = new Student8();

        /////////////////////////////////////
        //使用反射创建类的对象，并且调用对象的方法
        //1.加载类的字节码
        Class<?> clazz = Class.forName("day26.Student");//将全类名（包名.类名）
        //2.根据类的字节码，创建类的对象
        Student s1 = (Student) clazz.newInstance(); //调用空参构造方法，如果没有空参构造方法，反射就会报错
        Student s2 = (Student) clazz.newInstance();
        System.out.println(s1);
        System.out.println(s2);
        s1.study();
    }
}

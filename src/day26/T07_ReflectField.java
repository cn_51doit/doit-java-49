package day26;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

/**
 * 通过反射获取类的属性
 */
public class T07_ReflectField {

    private static Field field;

    public static void main(String[] args) throws Exception {

        //通过反射，获取类的字节码
        Class<?> clazz = Class.forName("day26.Student");

        //通过反射获取（属性）字段
//        Field[] fields = clazz.getFields(); //获取全部静态属性
//        for (Field field : fields) {
//            System.out.println(field);
//        }

        //Field field = clazz.getField("TTT");
        //Object o = field.get(null); // 静态的属性，不属于对象，是属于类的
        //System.out.println(o);

        //获取全部的变量（静态和成员的）
//        Field[] declaredFields = clazz.getDeclaredFields();
//        for (Field declaredField : declaredFields) {
//            System.out.println(declaredField);
//        }

        //gender是非静态的数据，必须归属与某个对象
        Field field1 = clazz.getDeclaredField("gender");

        //对象
        Student student = new Student("tom", 18, "male");

        //field1是对gender数学的描述，
        //
        //String gender = student.gender;
        //System.out.println(gender);
        Object res = field1.get(student);
        System.out.println(res);

        //获取私有的属性值
        Field field2 = clazz.getDeclaredField("name");
        //默认情况，反射无法获取私有的属性
        //Object res2 = field2.get(student); //报错
        //System.out.println(res2);

        //暴力反射
        field2.setAccessible(true); //设置即使是私有的也可以方法
        Object res3 = field2.get(student);
        System.out.println("修改前的值：" + res3);
        //修改属性值
        field2.set(student, "tomcat");
        Object res4 = field2.get(student);
        System.out.println("修改后的值" + res4);
    }
}


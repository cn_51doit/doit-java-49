package day26;

public class T03_ReflectDemo3 {

    public static void main(String[] args) throws Exception {

        //创建一个类的对象
        Student student = new Student();

        System.out.println(student);

        //更加该对象获取类的字节码
        Class<? extends Student> clazz = student.getClass();

        //根据类的字节码创建的对象
        Student student2 = clazz.newInstance(); //反射创建的对象

        System.out.println(student2);

    }
}

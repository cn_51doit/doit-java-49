package day26;

import java.lang.reflect.Constructor;

/**
 * 返回获取类的构造方法
 */
public class T04_ReflectConstructor {

    public static void main(String[] args) throws Exception {

        //通过反射，获取类的字节码
        Class<?> clazz = Class.forName("day26.Student");

        //获取空参的构造方法
        Constructor<?> constructor = clazz.getConstructor();

        //创建类的实例(对象)
        Object o = constructor.newInstance();

        System.out.println(o);
    }
}

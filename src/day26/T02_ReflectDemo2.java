package day26;

public class T02_ReflectDemo2 {

    public static void main(String[] args) throws Exception {

        //获取类的字节码
        Class<Student> clazz = Student.class;

        //创建类的实例
        Student student = clazz.newInstance();

        student.study();

    }
}

package day26;

public class Student {

    //静态属性
    public static String TTT = "ttt";
    //成员变量
    private String name;
    private int age;
    public String gender;

    //无参构造方法
    public Student(){
    }

    //有参构造方法
    public Student(String name,int age) {
        this.name = name;
        this.age = age;
    }

    public Student(String name, int age, String gender) {
        this.name = name;
        this.age = age;
        this.gender = gender;
    }

    //成员方法
    public void study(){
        System.out.println("学生在学习");
    }

    public void eat(String s,double d){
        System.out.println("带参数方法:"+s+"::"+d);
    }

    private void sayHello() {
        System.out.println("hello everyone");
    }

    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}' + "@" + hashCode();
    }

    public static void abc() {
        System.out.println("abc方法~~~~~");
    }

    public static void main(String[] args) {

        System.out.println("main方法~~~~~");
        System.out.println(args[0]);
        System.out.println(args[1]);
    }
}

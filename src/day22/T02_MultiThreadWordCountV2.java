package day22;

import org.apache.commons.io.FileUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.concurrent.*;

public class T02_MultiThreadWordCountV2 {

    public static void main(String[] args) throws ExecutionException, InterruptedException {


        HashMap<String, Integer> finalResult = new HashMap<>();

        //用来保存future的集合
        LinkedList<Future<HashMap<String, Integer>>> futures = new LinkedList<>();

        ExecutorService threadPool = Executors.newFixedThreadPool(4);

        Collection<File> files = FileUtils.listFiles(new File("data/word"), new String[]{"txt"}, false);

        for (File file : files) {

            Future<HashMap<String, Integer>> future = threadPool.submit(new WordCountTask(file));
            //Future的get方法是同步的，是阻塞的
            //HashMap<String, Integer> resultMap = future.get();
            futures.add(future);
        }


        while (futures.size() > 0) {

            //取出每一个future，如果每个计算任务对应的Task执行完成了，或返回一个HashMap
            for (Future<HashMap<String, Integer>> future : futures) {
                boolean done = future.isDone(); //线程执行完了，将将结果返回的，说明Future中有结果了
                if (done) {
                    HashMap<String, Integer> result = future.get(); //立即返回结果，因为future.isDone()为true
                    for (Map.Entry<String, Integer> entry : result.entrySet()) {
                        String word = entry.getKey();
                        Integer count = entry.getValue();
                        Integer acc = finalResult.getOrDefault(word, 0);
                        acc += count;
                        finalResult.put(word, acc); //累加后，添加到外面的最终结果
                    }
                    futures.remove(future);//移除已经计算完的Future
                    break; //终止循环
                }
            }
            Thread.sleep(100);
        }


        System.out.println("最终结果：" + finalResult);

        threadPool.shutdown();
    }


    public static class WordCountTask implements Callable<HashMap<String, Integer>> {

        private File file;

        private HashMap<String, Integer> wordToCount = new HashMap<String, Integer>();


        public WordCountTask(File file) {
            this.file = file;
        }

        @Override
        public HashMap<String, Integer> call() throws Exception {
            //用来保存每一个文件单词的数量的HashMap
            BufferedReader bufferedReader = null;

            try {
                bufferedReader = new BufferedReader(new FileReader(file));

                String line = null;
                //读取文件
                while ((line = bufferedReader.readLine()) != null) {
                    //每读取一行，然后进行切分
                    String[] words = line.split(" ");
                    for (String word : words) {
                        //从HashMap中查找指定单词的历史次数
                        Integer count = wordToCount.getOrDefault(word, 0);
                        wordToCount.put(word, ++count); //累加后再添加到HashMap中
                    }
                }
                bufferedReader.close(); //关闭IO流
                Thread.sleep(3000);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }

            return wordToCount;
        }
    }

}

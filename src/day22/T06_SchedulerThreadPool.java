package day22;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * 可调度的线程池，每个线程可以执行休息的是，每隔一段时间执行一次
 */
public class T06_SchedulerThreadPool {

    public static void main(String[] args) {


        ScheduledExecutorService scheduledThreadPool = Executors.newScheduledThreadPool(10);

        /**
         * 第一个参数：执行的逻辑
         * 第二个参数：延迟的时间
         * 第三个参数：执行的周期
         * 第四个参数：时间单位
         *
         * Runnable中的run方法只会执行一次
         */
        scheduledThreadPool.scheduleAtFixedRate(
                new Runnable() {
                    @Override
                    public void run() {
                        System.out.println(Thread.currentThread().getId() + " 定时器执行了，当前的时间：" + System.currentTimeMillis());
                        try {
                            Thread.sleep(20000); //假设任务逻辑复杂，耗时很长
                        } catch (InterruptedException e) {
                            throw new RuntimeException(e);
                        }
                    }
                },
                10,
                5,
                TimeUnit.SECONDS
        );

    }
}

package day22;

import java.util.List;

public class Consumer extends Thread {

    private List<String> list;

    public Consumer(String name, List<String> list) {
        super(name);
        this.list = list;
    }

    @Override
    public void run() {
        while (true) {
            synchronized (list) {
                if (list.size() == 0) {
                    try {
                        list.wait();
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                }
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
                System.out.println(super.getName() + "消费前的包子：" + list);
                list.remove(0);
                System.out.println(super.getName() + "消费后的包子：" + list);
                //集合中已经没有元素 则唤醒添加元素的线程 向集合中添加元素
                list.notify();
            }

        }

    }
}

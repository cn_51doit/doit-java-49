package day22;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 可缓存的线程池，默认创建60个线程对象，放在池子里面，如果提交的任务超过了60个，再额外创建指定的线程对象
 * 如果任务执行完成，超出的现象会被释放，但是一直保持60个活跃的
 */
public class T04_CachedThreadPool {

    public static void main(String[] args) {


        int processors = Runtime.getRuntime().availableProcessors();
        System.out.println("我的CPU的线程数量：" + processors);

        ExecutorService threadPool = Executors.newCachedThreadPool();

        for (int i = 0; i < 80; i++) {
            threadPool.execute(new Runnable() {
                @Override
                public void run() {
                    try {
                        Thread.sleep(3000);
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                    System.out.println(Thread.currentThread().getId() + " 执行完成");
                }
            });
        }

        System.out.println("任务已经提交到了线程池");

    }
}

package day22;

import java.util.LinkedList;

public class T10_MyBlockingQueue {

    public static void main(String[] args) {

        LinkedList<String> list = new LinkedList<>();
        Producer producer = new Producer("生产者", list);
        Consumer consumer1 = new Consumer("消费者1", list);
        //Consumer consumer2 = new Consumer("消费者2", list);
        producer.start();
        consumer1.start();
        //consumer2.start();


    }
}

package day22;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * scheduleWithFixedDelay ：如果run方法中的逻辑，执行时间大于指的周期时间，下一次执行的是，run方法执行的是+周期时间
 */
public class T07_SchedulerThreadPool {

    public static void main(String[] args) {


        ScheduledExecutorService scheduledThreadPool = Executors.newScheduledThreadPool(1);

        /**
         * 第一个参数：执行的逻辑
         * 第二个参数：延迟的时间
         * 第三个参数：执行的周期
         * 第四个参数：时间单位
         *
         * Runnable中的run方法只会执行一次
         */
        scheduledThreadPool.scheduleWithFixedDelay(

                new Runnable() {
                    @Override
                    public void run() {
                        System.out.println(Thread.currentThread().getId() + " 定时器执行了，当前的时间：" + System.currentTimeMillis());
                        try {
                            Thread.sleep(20000);
                        } catch (InterruptedException e) {
                            throw new RuntimeException(e);
                        }
                    }
                },
                10,
                5,
                TimeUnit.SECONDS
        );

    }
}

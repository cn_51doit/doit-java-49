package day22;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * 只有一个线程的线程池
 */
public class T03_SingleThreadPool {

    public static void main(String[] args) {

        ExecutorService threadPool = Executors.newSingleThreadExecutor();
        //ExecutorService threadPool = Executors.newFixedThreadPool(1);

        for (int i = 0; i < 10; i++) {

            threadPool.execute(new Runnable() {
                @Override
                public void run() {
                    try {
                        Thread.sleep(3000);
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                    System.out.println(Thread.currentThread().getId() + " 执行完成");
                }
            });
        }

        System.out.println("任务已经提交到了线程池");

    }
}

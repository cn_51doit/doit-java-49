package day22;

import javax.xml.crypto.Data;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * 可调度的线程池，每个线程可以执行休息的是，每隔一段时间执行一次
 */
public class T05_SchedulerThreadPool {

    public static void main(String[] args) {


//        ExecutorService threadPool = Executors.newSingleThreadExecutor();
//
//        threadPool.execute(new Runnable() {
//            @Override
//            public void run() {
//
//                while (true) {
//
//                    System.out.println("当前时间：" + System.currentTimeMillis());
//
//                    try {
//                        Thread.sleep(10000);
//                    } catch (InterruptedException e) {
//                        //throw new RuntimeException(e);
//                    }
//                }
//            }
//        });


        ScheduledExecutorService scheduledThreadPool = Executors.newScheduledThreadPool(1);

        /**
         * 第一个参数：执行的逻辑
         * 第二个参数：延迟的时间
         * 第三个参数：时间单位
         *
         * Runnable中的run方法只会执行一次
         */

        scheduledThreadPool.schedule(
                new Runnable() {
                    @Override
                    public void run() {
                        System.out.println("定时器执行了，当前的时间：" + System.currentTimeMillis());
                    }
                },
                10,
                TimeUnit.SECONDS
        );

    }
}

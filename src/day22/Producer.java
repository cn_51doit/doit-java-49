package day22;

import java.util.List;

public class Producer extends Thread {

    private List<String> list;

    public Producer(String name, List<String> list) {
        super(name);
        this.list = list;
    }

    @Override
    public void run() {
        while (true) {

            synchronized (list) {
                if (list.size() > 0) { //还有剩余的包子
                    try {
                        list.wait();
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                }
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
                list.add("包子");
                System.out.println(super.getName() + "生成后的包子：" +list);
                //集合中已经有元素了 唤醒获取元素的线程
                list.notify();
            }



        }
    }
}

package day22;

import java.util.concurrent.*;

public class T11_BlockingQueue {

    public static void main(String[] args) throws ExecutionException, InterruptedException {

        //BlockingQueue<Integer> blockingQueue = new ArrayBlockingQueue<>(5);
        //BlockingQueue<Integer>  blockingQueue = new LinkedBlockingQueue<>(10);
        BlockingQueue<Integer>  blockingQueue = new SynchronousQueue<>();

        //生成的线程
        new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 20; i++) {
                    try {
                        blockingQueue.put(i);
                        System.out.println("存入数据-->" + i);

                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();

        //消费的线程
        new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {

                    try {
                        Thread.sleep(1000);
                        System.out.println("取出数据-->" + blockingQueue.take());

                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

            }
        }).start();
    }

}

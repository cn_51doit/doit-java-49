package day22;

public class T08_WaitDemo {

    public static void main(String[] args) {

        Object obj = new Object();

        //String str = "abc";
        //str.wait();


        new Thread(new Runnable() {
            @Override
            public void run() {

                System.out.println("run方法执行了~~~");

                //加锁，将obj对象锁住
                synchronized (obj) {
                    try {
                        System.out.println("线程处于等待状态！！！");
                        obj.wait();

                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                }

                System.out.println("线程执行结束####");

            }
        }).start();





    }
}

package day22;

public class T09_NotifyDemo {

    public static void main(String[] args) throws InterruptedException {

        Object obj = new Object();

        new Thread(new Runnable() {
            @Override
            public void run() {

                System.out.println(Thread.currentThread().getId() +  " run方法执行了~~~");

                //加锁，将obj对象锁住
                synchronized (obj) {
                    try {
                        System.out.println(Thread.currentThread().getId() + " 线程处于等待状态！！！");
                        obj.wait(); //将线程切换成无限等待状态，并且让出锁
                        System.out.println(Thread.currentThread().getId() + " 线程被唤醒，处于运行状态！！！");
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                }
                System.out.println(Thread.currentThread().getId() + " 线程执行结束####");
            }
        }).start();

        //主线程睡眠5秒
        Thread.sleep(5000);

        new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println(Thread.currentThread().getId() +  " run方法执行了@@@@");
                //加锁，将obj对象锁住
                synchronized (obj) {
                    try {
                        System.out.println(Thread.currentThread().getId() + " 线程处于调用了通知方法");
                        obj.notify(); //调用完notify方法，如果下面没有调用wait方法，继续向下执行
                        System.out.println("￥￥￥￥￥￥￥￥￥￥￥￥￥");
                    } catch (Exception e) {
                        throw new RuntimeException(e);
                    }
                }
                System.out.println(Thread.currentThread().getId() + " 线程执行结束####");
            }
        }).start();


    }
}

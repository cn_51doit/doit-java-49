package day22;

import org.apache.commons.io.FileUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Collection;
import java.util.HashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class T01_MultiThreadWordCount {

    public static void main(String[] args) {

        ExecutorService threadPool = Executors.newFixedThreadPool(4);

        Collection<File> files = FileUtils.listFiles(new File("data/word"), new String[]{"txt"}, false);

        for (File file : files) {

            threadPool.execute(new Runnable() {

                @Override
                public void run() {
                    //用来保存每一个文件单词的数量的HashMap
                    HashMap<String, Integer> wordToCount = new HashMap<String, Integer>();
                    //读取文件内容
                    BufferedReader bufferedReader = null;
                    try {
                        bufferedReader = new BufferedReader(new FileReader(file));

                        String line = null;
                        //读取文件
                        while ((line = bufferedReader.readLine()) != null) {
                            //每读取一行，然后进行切分
                            String[] words = line.split(" ");
                            for (String word : words) {
                                //从HashMap中查找指定单词的历史次数
                                Integer count = wordToCount.getOrDefault(word, 0);
                                wordToCount.put(word, ++count); //累加后再添加到HashMap中
                            }
                        }
                        bufferedReader.close(); //关闭IO流

                        Thread.sleep(3000);
                        System.out.println(file.getName() + "各个单词的数量：" + wordToCount);

                    } catch (Exception e) {
                        throw new RuntimeException(e);
                    }
                }
            });

        }

        threadPool.shutdown();
    }
}

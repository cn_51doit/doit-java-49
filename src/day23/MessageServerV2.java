package day23;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class MessageServerV2 {

    public static void main(String[] args) throws Exception {

        ExecutorService threadPool = Executors.newCachedThreadPool();

        ServerSocket serverSocket = new ServerSocket(8888);

        //可以接受多个客户端发送的建立连接的请求
        while (true) {
            Socket socket = serverSocket.accept();
            threadPool.execute(new Worker(socket));
        }


    }

    private static class Worker implements Runnable {

        private Socket socket;

        public Worker(Socket socket) {
            this.socket = socket;
        }

        @Override
        public void run() {
            boolean flag = true;
            try {
                InputStream inputStream = socket.getInputStream();
                OutputStream outputStream = socket.getOutputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream));

                do {
                    String line = bufferedReader.readLine();
                    System.out.println("=> " + line);
                    //将数据写回到客户端
                    bufferedWriter.write("<= " + line);
                    bufferedWriter.newLine();
                    bufferedWriter.flush();
                    if (line.equalsIgnoreCase("bye")) {
                        flag = false;
                    }
                } while (flag);

                bufferedReader.close();
                bufferedWriter.close();
                inputStream.close();
                outputStream.close();
                socket.close();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }


        }
    }

}

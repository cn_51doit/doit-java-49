package day23;

import java.io.*;
import java.net.Socket;
import java.util.Scanner;

public class MessageClient {

    public static void main(String[] args) throws IOException {

        Socket socket = new Socket("192.168.6.5", 8888);

        //将输出流进行包装增强
        OutputStream outputStream = socket.getOutputStream();
        BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream));

        //获取输入流（服务端发送的数据）
        InputStream inputStream = socket.getInputStream();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

        //从命令行中接收数据
        Scanner scanner = new Scanner(System.in);

        boolean flag = true;

        while (flag) {
            String line = scanner.nextLine(); //接收到一行数据后
            if (line.equalsIgnoreCase("exit")) {
                flag = false;
                break;
            }
            bufferedWriter.write(line); //将数据写出去
            bufferedWriter.newLine();   //再写一个\n，代表以后结束
            bufferedWriter.flush(); //将缓存的数据刷新
            //结束服务端返回的数据
            String responseLine = bufferedReader.readLine();
            System.out.println("服务端返回的数据：" + responseLine);
        }

        bufferedReader.close();
        bufferedWriter.close();
        socket.close();


    }
}

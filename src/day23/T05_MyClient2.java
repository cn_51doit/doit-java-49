package day23;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

public class T05_MyClient2 {

    public static void main(String[] args) throws IOException {

        //localhost代表本地的地址，对应的ip地址为127.0.0.1
        Socket client = new Socket("localhost", 8888);

        //客户端向服务端发送数据
        OutputStream out = client.getOutputStream();

        out.write("hello".getBytes());

        //获取服务端返回的数据流
        InputStream input = client.getInputStream();

        byte[] bytes = new byte[1024];
        int len = input.read(bytes);
        String backMsg = new String(bytes, 0, len);

        System.out.println(backMsg);

    }
}

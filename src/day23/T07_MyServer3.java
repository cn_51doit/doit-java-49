package day23;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * 可以一直接收客户端发送的消息，一次接收一行数据，如果输入bye，服务端跟客户端端口链接
 */
public class T07_MyServer3 {

    public static void main(String[] args) throws IOException {

        //创建一个ServerSocket（服务端套接字）
        ServerSocket serverSocket = new ServerSocket(9999);

        //等待客户端的链接
        Socket socket = serverSocket.accept();

        //获取客户端的输入流
        InputStream inputStream = socket.getInputStream();

        //获取输出流
        OutputStream outputStream = socket.getOutputStream();

        BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream));


        //将字节流，转换成字符流
        InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
        //将输入的数据进行包装增强，增强的目的是为了可以一次读取一行内容
        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

        //继续运行的标记
        boolean flag = true;

        //先指定do中的代码内容，即do中的代码一定会执行一次
        do {
            String line = bufferedReader.readLine(); //如果数据以\n结束，才会返回一行
            System.out.println("server收到的消息：" + line);
            if (line.equalsIgnoreCase("bye")) {
                flag = false;
            }
            //将数据再发送回客户端
            bufferedWriter.write("<= " + line);
            bufferedWriter.newLine();
            bufferedWriter.flush();
        } while (flag); //如果while中的条件为true，继续执行do中的内容
        //如果while中的条件为false，那么就不执行do中的代码了

        bufferedReader.close();
        inputStream.close();
        socket.close();
        serverSocket.close();


    }
}

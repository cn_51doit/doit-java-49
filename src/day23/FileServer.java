package day23;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.UUID;

public class FileServer {

    public static void main(String[] args) throws IOException {

        ServerSocket serverSocket = new ServerSocket(7777);

        while (true) {

            Socket socket = serverSocket.accept();

            new Thread(new Runnable() {
                @Override
                public void run() {
                    //获取客户端发送过来的数据
                    InputStream inputStream = null;
                    FileOutputStream fileOutputStream = null;
                    OutputStream outputStream = null;
                    try {
                        inputStream = socket.getInputStream();
                        outputStream = socket.getOutputStream();

                        fileOutputStream = new FileOutputStream(new File("pic" + File.separator + UUID.randomUUID().toString() + ".png"));
                        byte[] buffer = new byte[1024 * 8];
                        int len = 0;
                        while ((len = inputStream.read(buffer)) != -1) {
                            fileOutputStream.write(buffer, 0, len);
                        }
                        outputStream.write("上传成功".getBytes());

                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    } finally {
                        try {
                            fileOutputStream.close();
                            inputStream.close();
                            outputStream.close();
                            socket.close();
                        } catch (IOException e) {
                            throw new RuntimeException(e);
                        }
                    }
                }
            }).start();
        }

    }
}

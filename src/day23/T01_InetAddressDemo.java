package day23;

import java.net.InetAddress;
import java.net.UnknownHostException;

public class T01_InetAddressDemo {

    public static void main(String[] args) throws UnknownHostException {

        InetAddress localHost = InetAddress.getLocalHost();
        String hostName = localHost.getHostName();
        System.out.println("本机的HostName: " + hostName);
        String hostAddress = localHost.getHostAddress();
        System.out.println("本地的IP地址：" + hostAddress);

        InetAddress remote = InetAddress.getByName("www.baidu.com");
        String remoteHostName = remote.getHostName();
        String remoteAddress = remote.getHostAddress();
        System.out.println("远端的HostName: " + remoteHostName);
        System.out.println("远端的IP地址：" + remoteAddress);



    }
}

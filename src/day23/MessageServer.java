package day23;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class MessageServer {

    public static void main(String[] args) throws Exception {


        ServerSocket serverSocket = new ServerSocket(8888);

        //可以接受多个客户端发送的建立连接的请求
        while (true) {
        //for (; ; ) { //死循环
            //来一个客户端，获取一个客户端连接的Socket
            Socket socket = serverSocket.accept();
            //一个客户端交给一个线程进行处理
            new Thread(new Runnable() {
                @Override
                public void run() {

                    try {
                        InputStream inputStream = socket.getInputStream();
                        OutputStream outputStream = socket.getOutputStream();
                        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                        BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream));

                        boolean flag = true;
                        do {
                            String line = bufferedReader.readLine();
                            System.out.println("=> " + line);
                            //将数据写回到客户端
                            bufferedWriter.write("<= " + line);
                            bufferedWriter.newLine();
                            bufferedWriter.flush();
                            if (line.equalsIgnoreCase("bye")) {
                                flag = false;
                            }
                        } while (flag);

                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }


                }
            }).start();

        }


    }
}

package day23;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Arrays;

/**
 * 以前访问的京东、淘宝、网易的网站，后台也会很多服务器，运行着服务程序
 * tomcat、JBoss 使用Java编写的服务端程序
 * nginx 使用 c语言编写的服务端程序
 */
public class WebServer {


    public static void main(String[] args) throws IOException {
        ServerSocket server = new ServerSocket(8888);
        while (true) {
            Socket socket = server.accept();
            new Thread(new Web(socket)).start();
        }
    }


    private static class Web implements Runnable{
        private Socket socket;

        public Web(Socket socket){
            this.socket=socket;
        }

        public void run() {
            try{
                //转换流,读取浏览器请求第一行
                BufferedReader readWb = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                String request = readWb.readLine();
                //取出请求资源的路径
                String[] strArr = request.split(" ");
                String path = strArr[1].substring(1);
                System.out.println(path);
                //将请求的文件，读取文件中的内容，然后使用IO流，返回给浏览器
                FileInputStream fis = new FileInputStream("html/"+ path);
                System.out.println(fis);
                byte[] bytes= new byte[1024]; //字节数组
                int len = 0 ;

                //向浏览器 回写数据
                OutputStream out = socket.getOutputStream();
                out.write("HTTP/1.1 200 OK\r\n".getBytes());
                out.write("Content-Type:text/html\r\n".getBytes());
                out.write("\r\n".getBytes());
                while((len = fis.read(bytes))!=-1){
                    out.write(bytes,0,len);
                }
                fis.close();
                out.close();
                readWb.close();
                socket.close();
            }catch(Exception ex){
                ex.printStackTrace();
            }
        }
    }


}

package day23;

import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;

public class T03_MyClient1 {

    public static void main(String[] args) throws IOException {

        //localhost代表本地的地址，对应的ip地址为127.0.0.1
        Socket client = new Socket("localhost", 8888);

        //客户端向服务端发送数据
        OutputStream out = client.getOutputStream();

        out.write("hello".getBytes());

        out.close();

        client.close();
    }
}

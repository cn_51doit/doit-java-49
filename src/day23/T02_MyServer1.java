package day23;

import java.io.IOException;
import java.io.InputStream;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * 使用TCP协议实现的一个简单的服务端
 */
public class T02_MyServer1 {

    public static void main(String[] args) throws IOException {

        //创建一个服务端套接字，要指定端口号，端口号的范围[1, 65535], 操作系统会预留一些端口，1到1024
        ServerSocket serverSocket = new ServerSocket(8888);

        //获取客户端的socket，相当于拿到了客户端的链接
        Socket socket = serverSocket.accept(); //接收

        //根据客户端的链接，就可以拿到客户端发送过来的数据
        InputStream input = socket.getInputStream();

        byte[] buff = new byte[1024];
        // 4.2 据读取到字节数组中.
        int len = input.read(buff);
        // 4.3 解析数组,打印字符串信息
        String msg = new String(buff, 0, len);
        System.out.println(msg);
        //5.关闭资源.
        input.close();
        serverSocket.close();


    }


}

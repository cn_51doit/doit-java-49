package day21;

public class T12_ReentrantLockDemo {

    public static void main(String[] args) throws InterruptedException {

        //要抢购的商品，总共有5件
        Product product = new Product();

        for (int i = 0; i < 30; i++) {
            if (i % 3 == 0) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            product.purchase();
                        } catch (Exception e) {
                            //throw new RuntimeException(e);
                        }
                    }
                }).start();
            } else {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            product.view();
                        } catch (Exception e) {
                            //throw new RuntimeException(e);
                        }
                    }
                }).start();
            }



        }

        Thread.sleep(2000);

        System.out.println("最后剩余商品剩余的数量：" + product.getAmount());



    }

}

package day21;

public class MyRunnable implements Runnable {

    @Override
    public void run() {
        for (int i = 0; i < 1000; i++) {
            System.out.println(Thread.currentThread().getId() +  " MyRunnable类中的run方法执行了，当前I的值：" + i);
        }
    }
}

package day21;

public class T02_RunnableDemo1 {

    public static void main(String[] args) {

        System.out.println("主线程开始执行！！！！！");

        MyRunnable mr = new MyRunnable();
        Thread thread = new Thread(mr);
        thread.start();

        for (int i = 0; i < 1000; i++) {
            System.out.println(Thread.currentThread().getId() +  " main方法执行了，当前I的值：" + i);
        }

        System.out.println("main方法执行结束@@@@@");
    }
}

package day21;

public class T03_RunnableDemo2 {

    public static void main(String[] args) {

        System.out.println("主线程开始执行！！！！！");


        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 1000; i++) {
                    System.out.println("Runnable实现类中的run方法执行了，当前的I：" + i);
                }
            }
        });
        thread.start();

        for (int i = 0; i < 1000; i++) {
            System.out.println(Thread.currentThread().getId() +  " main方法执行了，当前I的值：" + i);
        }

        System.out.println("main方法执行结束@@@@@");
    }
}

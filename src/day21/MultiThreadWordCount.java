package day21;

import org.apache.commons.io.FileUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.concurrent.*;

public class MultiThreadWordCount {

    public static void main(String[] args) throws ExecutionException, InterruptedException {

        //用来保存最终结果的HashMap
        HashMap<String, Integer> finalResultMap = new HashMap<String, Integer>();

        //LinkedList增删效率更高
        LinkedList<Future<HashMap<String, Integer>>> futures = new LinkedList<>();

        //创建线程池
        ExecutorService threadPool = Executors.newFixedThreadPool(4);

        //列出所有符合条件的文件
        Collection<File> files = FileUtils.listFiles(new File("data/word"), new String[]{"txt"}, false);

        //循环文件
        for (File file : files) {

            //每一个文件开启一个线程，提交到线程池中
            Future<HashMap<String, Integer>> future = threadPool.submit(new Callable<HashMap<String, Integer>>() {

                //用来保存每一个文件单词的数量的HashMap
                HashMap<String, Integer> wordToCount = new HashMap<String, Integer>();

                @Override
                public HashMap<String, Integer> call() throws Exception {
                    //读取文件内容
                    BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
                    String line = null;
                    while ((line = bufferedReader.readLine()) != null) {
                        String[] words = line.split(" ");
                        for (String word : words) {
                            Integer count = wordToCount.getOrDefault(word, 0);
                            wordToCount.put(word, ++count);
                        }
                    }
                    bufferedReader.close();
                    //返回结果
                    return wordToCount;
                }
            });

            //将每个线程返回的future添加到LinkedList中
            futures.add(future);

        }

        while (futures.size() > 0) {

            for (Future<HashMap<String, Integer>> future : futures) {
                //如果future中的结果已经返回，isDone方法返回true，否则返回false
                if (future.isDone()) {
                    HashMap<String, Integer> result = future.get();
                    //将future从LinkedList中移除
                    futures.remove(future);
                    //遍历每个文件返回的结果
                    for (Map.Entry<String, Integer> entry : result.entrySet()) {
                        String word = entry.getKey();
                        Integer count = entry.getValue();
                        //将每个文件每个单词的次数保存到finalResultMap中
                        Integer acc = finalResultMap.getOrDefault(word, 0);
                        acc += count; //进行累加
                        finalResultMap.put(word, acc);
                    }
                    break;
                }
            }
            Thread.sleep(100);
        }

        System.out.println(finalResultMap);

        threadPool.shutdown();
    }
}

package day21;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.util.Collection;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class T14_MultiFileNumCount {

    public static void main(String[] args) {

        ExecutorService threadPool = Executors.newFixedThreadPool(6);

        File dir = new File("data/math");
        Collection<File> files = FileUtils.listFiles(dir, new String[]{"txt"}, false);

        for (File file : files) {
            if (file.length() > 0) {
                //System.out.println(file.getName());
                threadPool.execute(new Runnable() {
                    @Override
                    public void run() {
                        System.out.println("开始对" + file.getName() + "进行统计");
                    }
                });
            }
        }


    }
}

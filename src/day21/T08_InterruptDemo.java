package day21;

public class T08_InterruptDemo {

    public static void main(String[] args) {

        Thread t1 = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                    //throw new RuntimeException(e);
                }
                for (int i = 0; i < 1000; i++) {
                    System.out.println("子线程的for循环， I " + i);
                }
            }
        });

        t1.start();

        boolean interrupted = t1.isInterrupted();
        System.out.println(interrupted);

        t1.interrupt();

        boolean interrupted2 = t1.isInterrupted();
        System.out.println(interrupted2);


    }
}

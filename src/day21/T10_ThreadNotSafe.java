package day21;

public class T10_ThreadNotSafe {

    public static void main(String[] args) throws InterruptedException {

        //要抢购的商品，总共有5件
        Product product = new Product();


        for (int i = 0; i < 10; i++) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Thread.sleep(200);
                        product.amount -= 1;
                    } catch (Exception e) {
                        //throw new RuntimeException(e);
                    }
                }
            }).start();

        }

        Thread.sleep(2000);

        System.out.println("最后剩余商品剩余的数量：" + product.getAmount());



    }

}

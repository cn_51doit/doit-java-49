package day21;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 固定大小的线程池
 */
public class T13_FixedThreadPool {

    public static void main(String[] args) {

        //创建一个固定大小的线程池，大小为6
        ExecutorService threadPool = Executors.newFixedThreadPool(6);
        //使用线程池后，就不用再new Thread了

        System.out.println("开始提交任务到线程池~~~~");

        //循环的提交任务
        for (int i = 1; i <= 20; i++) {
            //execute是执行的意思，
            threadPool.execute(new Runnable() {
                @Override
                public void run() {
                    try {
                        System.out.println(Thread.currentThread().getId() + " 这个线程开始执行了");
                        Thread.sleep(8000);
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                }
            });
        }

        System.out.println("任务已经全部提交到线程池了！！！！");



    }
}

package day21;

public class T07_JoinDemo {

    public static void main(String[] args) throws Exception {

        Thread t1 = new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 1000; i++) {
                    System.out.println("t1   "+i);
                }
            }
        });

        Thread t2 = new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 1000; i++) {
                    System.out.println("t2   "+i);
                }
            }
        });
        t1.start();
        //注意 此时当前线程是main线程
        //main线程进入等待,让t1线程执行. 由于此时main等待 所以t2还没有执行
        //t1执行完毕后 main和t2抢夺cpu资源 执行
        //t1.join();

        t2.start();
        //如果t1.join放在这里 那么main进入到等待 t2已经开启了
        //t1 t2互相抢夺资源
        //main一定是在 t1执行完毕后 才会执行
        //那么就会有两种情况  t1执行完毕  t2没执行完 main和 t2进行抢夺
        //t1执行完毕  t2也执行完毕了  main自己执行
        t1.join();

        for (int i = 0; i < 1000; i++) {
            System.out.println("main "+i);
        }








    }
}

package day21;

public class T05_ThreadPriorityDemo {

    public static void main(String[] args) {

        Thread t1 = new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 10000; i++) {
                    System.out.println("t1 "+i);
                }
            }
        });

        Thread t2 = new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 10000; i++) {
                    System.out.println("t2 "+i);
                }
            }
        });

        //如果不设置优先级 谁先执行完是随机的
        //但是即使设置了优先级 也没办法绝对保证谁先执行 谁后执行
        t1.setPriority(Thread.MIN_PRIORITY);//优先级设置为最低
        t2.setPriority(Thread.MAX_PRIORITY);//优先级设置为最高


        t1.start();
        t2.start();




    }
}

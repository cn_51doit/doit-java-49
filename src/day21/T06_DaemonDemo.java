package day21;

public class T06_DaemonDemo {

    public static void main(String[] args) throws InterruptedException {


        System.out.println("666");

        //Thread.sleep(10000);

        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 10000; i++) {
                    System.out.println(i);
                }
            }
        });
        thread.setDaemon(true);
        thread.start();

    }
}

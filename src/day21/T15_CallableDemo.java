package day21;

import java.util.Random;
import java.util.concurrent.*;

/**
 * Callable和Runnable一样，也可以提交到线程池中执行
 *
 * Runnable的run方法没有返回值
 * Callable的call方法有返回值
 */
public class T15_CallableDemo {

    public static void main(String[] args) throws ExecutionException, InterruptedException, TimeoutException {

        Random random = new Random();
        //创建一个固定大小的线程池，大小为6
        ExecutorService threadPool = Executors.newFixedThreadPool(6);
        //使用线程池后，就不用再new Thread了

        System.out.println("开始提交任务到线程池~~~~");

        //循环的提交任务

        //execute是执行的意思，
        Future<Integer> future = threadPool.submit(new Callable<Integer>() {
            @Override
            public Integer call() throws Exception {
                System.out.println("子线程" + Thread.currentThread().getId() + "开始执行运算逻辑~~~");
                Thread.sleep(12000);
                int r = random.nextInt(10);
                System.out.println("call方法已经return了" + r);
                return r;
            }
        });

        System.out.println("任务已经已经到线程池中了");

        //从Future中取值，如果有，立即返回，如果没有，阻塞等待

        System.out.println("开始从Future中取值");
        //Integer i = future.get(); //future中没有值，方法会阻塞在这里

        Integer i = null; //Future中有值，立即取出对应的值，没有最多等待指定的是，超时会抛出异常
        try {
            i = future.get(8000, TimeUnit.MILLISECONDS);
            System.out.println("从Future中已经取到了值");
        } catch (Exception e) {
            //throw new RuntimeException(e);
            System.out.println("超时了，没有从Future中取到值~");
        }


        System.out.println(i);





    }
}

package day21;

public class T01_ThreadDemo1 {

    public static void main(String[] args) {

        System.out.println("主线程开始执行~~~~~");

        MyThread myThread = new MyThread();
        //myThread.setName("我的子线程");
        //myThread.run(); 调用线程的run方法，根本就没有开启一个新的线程

        myThread.start(); //开启一个新的线程，新线程开启后，会自动调用run方法

        for (int i = 0; i < 1000; i++) {
            System.out.println(Thread.currentThread().getName() + " 主线程开始执行的循环：" + i + " ，主线程ID" + Thread.currentThread().getId());
        }


        System.out.println("主线程执行完毕~~~~~");
    }
}

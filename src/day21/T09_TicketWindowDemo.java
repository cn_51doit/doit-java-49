package day21;

public class T09_TicketWindowDemo {

    public static void main(String[] args) {

        TicketSystem ticketSystem = new TicketSystem();

        //创建3个线程对象，将Ticket传入到线程对象中
        Thread t1 = new Thread(ticketSystem, "窗口1");
        Thread t2 = new Thread(ticketSystem, "窗口2");
        Thread t3 = new Thread(ticketSystem, "窗口3");

        t1.start();
        t2.start();
        t3.start();

    }
}

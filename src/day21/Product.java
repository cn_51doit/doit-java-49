package day21;

import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class Product {

    //普通锁，读跟写都互斥
    private ReentrantLock lock = new ReentrantLock();

    //读写锁，读和读不互斥，读跟写互斥，写跟写互斥
    private ReentrantReadWriteLock readWriteLock = new ReentrantReadWriteLock();

    int amount = 5;

    public synchronized void buy() throws Exception {
        if (amount > 0) {
            Thread.sleep(200);
            amount -= 1;
            System.out.println(Thread.currentThread().getId() + " 购买到了商品，剩余的商品数量：" + amount);
        } else {
            System.out.println(Thread.currentThread().getId() + "没有购买到商品");
        }

    }

    public void buy2() throws Exception{
        //执行核心代码前加锁
        lock.lock();
        if (amount > 0) {
            Thread.sleep(200); //出货的操作
            amount -= 1;
            System.out.println(Thread.currentThread().getId() + " 购买到了商品，剩余的商品数量：" + amount);
        } else {
            System.out.println(Thread.currentThread().getId() + "没有购买到商品");
        }
        //核心代码执行后释放锁
        lock.unlock();
    }


    //浏览剩余商品的数量
    public void view() {
        //开启读锁
        readWriteLock.readLock().lock();
        try {
            //执行查询逻辑
            System.out.println("view方法：当前剩余的商品数量：" + amount);
        } finally {
            //释放读锁
            readWriteLock.readLock().unlock();
        }
    }

    public void purchase() throws InterruptedException {
        try {
            readWriteLock.writeLock().lock();
            if (amount > 0) {
                System.out.println(Thread.currentThread().getId() + "正在准备出货...");
                Thread.sleep(500); //出货的操作
                amount -= 1;
                System.out.println(Thread.currentThread().getId() + " 购买到了商品，剩余的商品数量：" + amount);
            } else {
                System.out.println(Thread.currentThread().getId() + "没有购买到商品");
            }
        } finally {
            readWriteLock.writeLock().unlock();
        }
    }


    public int getAmount() {
        return amount;
    }
}

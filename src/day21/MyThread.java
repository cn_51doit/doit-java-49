package day21;

public class MyThread extends Thread {

    @Override
    public void run() {
        for (int i = 0; i < 1000; i++) {
            System.out.println(Thread.currentThread().getName() + " MyThread的run方法执行了，正在执行for循环 "+ i + " 子线程ID" + Thread.currentThread().getId());
        }
    }
}

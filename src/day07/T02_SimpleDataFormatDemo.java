package day07;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Java用于格式化日期的类
 */
public class T02_SimpleDataFormatDemo {

    public static void main(String[] args) throws ParseException {
        
        //2024-03-29 16:48:08 字符串 转成 1711702088000

        String str = "2024-03-29 16:48:08";
        
        //先new对象，再调用方法
        //new SimpleDateFormat 要传入日期的格式
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        
        //调用方法
        Date date = sdf.parse(str);

        //获取long类型的时间戳
        long time = date.getTime();
        System.out.println(time);
    }
    
}

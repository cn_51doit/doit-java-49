package day07;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Java用于格式化日期的类
 * 将字符串转成Date类型，Date类型中包含long类型的时间
 */
public class T03_SimpleDataFormatDemo2 {

    public static void main(String[] args) throws ParseException {
        
        //2024/03/29 16:48:08字符串 转成 1711702088000

        String str = "2024/03/29 16:48:08";
        
        //先new对象，再调用方法
        //new SimpleDateFormat 要传入日期的格式
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        
        //调用parse方法，将字符串转成Date类型
        Date date = sdf.parse(str);

        //获取lang类型的时间戳
        long time = date.getTime();
        System.out.println(time);
    }
    
}

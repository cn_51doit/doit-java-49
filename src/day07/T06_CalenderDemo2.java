package day07;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * 
 */
public class T06_CalenderDemo2 {

    public static void main(String[] args) throws ParseException {

       
        //先将日期字符串转成Date类型
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        
        //创建日历工具类，调用静态getInstance方法
        Calendar calendar = Calendar.getInstance();
        
        //将当前的时间加12小时
        long currentTime = System.currentTimeMillis();
        calendar.setTime(new Date(currentTime));
        
        //使用calendar进行处理，在原来的基础上加12个小时
        calendar.add(Calendar.HOUR, 12);

        //获取到calendar中处理后的Date
        Date date2 = calendar.getTime();
        
        //将date2转成字符
        String res = sdf.format(date2);

        System.out.println(res);


    }
}

package day07;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * lang类型的时间戳，转成字符串
 */
public class T03_SimpleDataFormatDemo3 {

    public static void main(String[] args) throws ParseException {
        
        //1711702088000 转成 "2024-03-29 16:48:08"

        long time = 1711702088000L; //

        //先new对象，再调用方法
        //new SimpleDateFormat 要传入日期的格式
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        
        //调用format方法，将Date转成字符串（注意，时间要封装到Date中）
        String str = sdf.format(new Date(time));

        //获取lang类型的时间戳
        System.out.println(str);
    }
    
}

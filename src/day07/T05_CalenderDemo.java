package day07;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Java的日历工具类，可以对日期进行加减等操作
 */
public class T05_CalenderDemo {

    public static void main(String[] args) throws ParseException {

        String dt = "2023-03-01";
        //先将日期字符串转成Date类型
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date date = sdf.parse(dt);
        
        //创建日历工具类，调用静态getInstance方法
        Calendar calendar = Calendar.getInstance();
        
        //将要处理的Date设置到calendar
        calendar.setTime(date);
        
        //使用calendar进行处理，在原来的基础上减一天
        calendar.add(Calendar.DATE, -1);
        //在原来的基础上加一个月
        //calendar.add(Calendar.MONTH, 1);
        //在原来的基础上加一年
        //calendar.add(Calendar.YEAR, 1);
        
        
        //获取到calendar中处理后的Date
        Date date2 = calendar.getTime();
        
        //将date2转成字符
        String res = sdf.format(date2);

        System.out.println(res);


    }
}

package day07;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class T01_ProvinceCityIncome {

    public static void main(String[] args) throws FileNotFoundException {
        //provinceCityAndMoney的地址值：HashMap@497
        HashMap<String, HashMap<String, Double>> provinceCityAndMoney = new HashMap<>();

        File file = new File("data/city.txt");
        Scanner scanner = new Scanner(file);
        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            String[] fields = line.split(",");
            String province = fields[0];
            String city = fields[1];
            double money = Double.parseDouble(fields[2]);
            //根据省份名称取出保存该省份下所有城市和对应金额的信息
            HashMap<String, Double> cityToMoney = provinceCityAndMoney.get(province);
            if(cityToMoney == null) { //该省份第一次出现
                cityToMoney = new HashMap<>(); //山东省的Map地址值：HashMap@752 ， 辽宁省的Map地址值：HashMap@954
                //将新new 出来的这个HashMap添加到provinceCityAndMoney中
                provinceCityAndMoney.put(province, cityToMoney);
            }
            //根据城市名称，获取该城市的金额
            Double accMoney = cityToMoney.getOrDefault(city, 0.0);
            accMoney += money;
            //将累加后的数据，添加到cityToMoney中
            cityToMoney.put(city, accMoney);
        }
        
        //输出结果, 即循环provinceCityAndMoney，并且在循环里面的cityToMoney
        for (Map.Entry<String, HashMap<String, Double>> entry : provinceCityAndMoney.entrySet()) {
            String province = entry.getKey(); // 取出一个省份
            //一个省份下所有城市和金额的对应关系
            HashMap<String, Double> cityToMoney = entry.getValue();
            //一个省份下，所有城市对应的金额
            Collection<Double> allMoney = cityToMoney.values();
            double sumMoney = 0.0;
            for (Double m : allMoney) {
                sumMoney += m;
            }

            for (Map.Entry<String, Double> entry2 : cityToMoney.entrySet()) {
                String city = entry2.getKey(); //城市名称
                Double money = entry2.getValue(); //城市对应的金额
                System.out.println(province + "," + sumMoney + "," + city + "," + money);
            }
        }
        scanner.close();
    }
}

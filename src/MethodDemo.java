public class MethodDemo {

    public static String concat(String sp, String s1, String s2) {
        //String res = s1 + sp + s2; //字符串相加就是字符串拼接
        //return res;
        return s1 + sp + s2;
    }


    public static void concat2(String sp, String s1, String s2) {
        String res = s1 + sp + s2;
        System.out.println(res);
    }

    private double add(double a, int b) {
        return a + b;
    }

    private void sayHello() {
        System.out.println("hello everybody");
    }

    public static void main(String[] args) {



        //调用静态方法公式：类名.方法名()

        //调用成员方法：先new对象，对象名.方法名()



        //String r = MethodDemo.concat("-", "aaa", "bbb");
        //System.out.println(r);

        //MethodDemo.concat2("_", "spark", "2.0");
        //concat2("_", "spark", "2.0"); //静态方法整合在本类中使用，类名和点可以省略


        //非静态方法也叫成员方法
        //使用new关键字创建这个类的对象（实例）
        MethodDemo demo = new MethodDemo();
        //double res = demo.add(2.2, 3);
        //System.out.println(res);

        demo.sayHello();
    }

}


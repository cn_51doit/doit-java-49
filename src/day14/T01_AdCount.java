package day14;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Scanner;

/**
 * 通过用一个广告、同一种事件的次数（不去重，即PV）和人数（去重即UV）
 */
public class T01_AdCount {

    public static void main(String[] args) throws FileNotFoundException {

        File file = new File("data/ad.txt");
        Scanner scanner = new Scanner(file);

        HashMap<String, HashSet<String>> aidAndEidToUidSet = new HashMap<>();

        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            String[] fields = line.split(",");
            String uid = fields[0]; //用户ID
            String eid = fields[1]; //事件ID
            String aid = fields[2]; //广告ID
            String key = aid + "," + eid;
            HashSet<String> uidSet = aidAndEidToUidSet.get(key);
            if (uidSet == null) {
                uidSet = new HashSet<>();
                //将新创建的HashSet保存到aidAndEidToUidSet这个map中
                aidAndEidToUidSet.put(key, uidSet);
            }
            uidSet.add(uid); //将用户ID添加到uidSet中
        }

        //遍历aidAndEidToUidSet
        for (Map.Entry<String, HashSet<String>> entry : aidAndEidToUidSet.entrySet()) {
            String aidAndEid = entry.getKey();
            HashSet<String> uidSet = entry.getValue();

            System.out.println(aidAndEid + "," + uidSet.size());
        }


    }
}

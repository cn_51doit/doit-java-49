package day14;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Scanner;

/**
 * 通过用一个广告、同一种事件的次数（不去重，即PV）和人数（去重即UV）
 */
public class T02_AdCount {

    public static void main(String[] args) throws FileNotFoundException {

        File file = new File("data/ad.txt");
        Scanner scanner = new Scanner(file);

        HashMap<String, Integer> aidAndEidToCount = new HashMap<>();

        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            String[] fields = line.split(",");
            String uid = fields[0]; //用户ID
            String eid = fields[1]; //事件ID
            String aid = fields[2]; //广告ID
            String key = aid + "," + eid;
            Integer count = aidAndEidToCount.getOrDefault(key, 0);
            //count++;
            aidAndEidToCount.put(key, ++count);
        }

        //遍历aidAndEidToUidSet
        for (Map.Entry<String, Integer> entry : aidAndEidToCount.entrySet()) {
            String aidAndEid = entry.getKey();
            int count = entry.getValue();

            System.out.println(aidAndEid + "," + count);
        }


    }
}

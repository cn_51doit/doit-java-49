package day14;

/**
 * 演示空指针异常
 * 使用一个变量，如果这个引用类型的变量是null，那么调用这个变量的方法或属性都会包空指针异常
 */
public class NullPointerTest {

    public static void main(String[] args) {

        String line = "spark hive flink";
        //String line = null;

        //String[] fields = line.split(" ");
        String[] fields = null;

        System.out.println(fields.length);


        //String word = "spark";
        //String word = null; //报错
        String word = "null";
        //将word单词转成大写
        String upperCase = word.toUpperCase();

        System.out.println(upperCase);

    }
}

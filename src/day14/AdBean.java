package day14;

import java.util.HashSet;

/**
 * 封装数据的Java类
 * bean
 */
public class AdBean {

    private HashSet<String> uidSet = new HashSet<>();
    //private HashSet<String> uidSet;
    private int count;

    public HashSet<String> getUidSet() {
        return uidSet;
    }

    public void setUidSet(HashSet<String> uidSet) {
        this.uidSet = uidSet;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}

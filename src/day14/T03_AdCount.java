package day14;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * 通过用一个广告、同一种事件的次数（不去重，即PV）和人数（去重即UV）
 */
public class T03_AdCount {

    public static void main(String[] args) throws FileNotFoundException {

        File file = new File("data/ad.txt");
        Scanner scanner = new Scanner(file);

        //"a01,view" -> AdBean(HashSet(u01, u02, u03), 4)
        //"a01,click" -> AdBean(HashSet(u01, u03, u04), 5)
        HashMap<String, AdBean> aidAndEidToAdBean = new HashMap<>();

        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            String[] fields = line.split(",");
            String uid = fields[0]; //用户ID
            String eid = fields[1]; //事件ID
            String aid = fields[2]; //广告ID
            String key = aid + "," + eid;
            AdBean adBean = aidAndEidToAdBean.get(key);
            if (adBean == null) {
                adBean = new AdBean();
                aidAndEidToAdBean.put(key, adBean);
            }
            adBean.getUidSet().add(uid); //将用户id添加到HashSet
            adBean.setCount(adBean.getCount() + 1); //将原来的次数取出来相加后再赋值
        }

        //循环
        for (Map.Entry<String, AdBean> entry : aidAndEidToAdBean.entrySet()) {
            String aidAndEid = entry.getKey();
            AdBean adBean = entry.getValue();
            System.out.println(aidAndEid + "," + adBean.getUidSet().size() + "," + adBean.getCount());
        }
    }
}

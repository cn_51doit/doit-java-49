package day02;

public class T07_ArrayDemo4 {

    public static void main(String[] args) {

        double[] arr = new double[]{1.1, 2.2, 3.1, 4.5, 6.6};

        double d1 = arr[0];

        System.out.println(d1);

        arr[0] = 100.1;

        System.out.println(arr[0]);
    }
}

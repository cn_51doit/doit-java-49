package day02;

public class User {

    public static void sayHei() {
        System.out.println("hei");
    }

    private static void sayHeHe() {
        System.out.println("hehe");
    }


    /**
     * 该方法是一个非静态的方法，即方法中没有用static关键字修饰
     * 非静态的方法必须创建一个具体的对象（实例），先创建对象，仔调用方法
     * @param name
     */
    public void printName(String name) {
        System.out.println("你的名字是：" + name);
    }

    public static void main(String[] args) {

        sayHeHe();

        int i = 10;
        //前面的User代表变量的类型
        //u 代表变量名称
        //new User()创建一个User对象（实例），通过=赋值给u
        User u = new User();

        u.printName("张三");

    }
}

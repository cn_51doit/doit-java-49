package day02;

public class T14_ForDemo3 {

    public static void main(String[] args) {

        int[] arr = new int[8];

        for(int i = 0; i < arr.length; i++) {
            arr[i] = (i + 1) * 10;
        }


        for(int i = 0; i < arr.length; i++) {
            System.out.println("数组第"+ i + "位置的值为：" + arr[i]);
        }


    }
}

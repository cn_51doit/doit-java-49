package day02;

public class T13_ForDemo2 {

    public static void main(String[] args) {

        //取出字符串中的每一个字符
        String str = "abcdefghijk";

        for(int i = 0; i < str.length(); i++) {
            System.out.println(str.charAt(i));
        }

    }
}

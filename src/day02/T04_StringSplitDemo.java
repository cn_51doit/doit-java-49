package day02;

/**
 * 演示字符串的split方法
 */
public class T04_StringSplitDemo {

    public static void main(String[] args) {

        String s = "spark,hive,flink,hadoop,spark";

        //按照指定的分隔符切分字符串
        String[] words = s.split(",");

        System.out.println(words.length);

    }
}

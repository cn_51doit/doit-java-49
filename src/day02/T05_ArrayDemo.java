package day02;

/**
 * 数组：存放多个相同数据类型元素且有序的集合
 */
public class T05_ArrayDemo {

    public static void main(String[] args) {

        String a = "abc";

        //定义数组的方式
        //创建一个长度为8的数组
        String[] arr = new String[8]; //8代表数组的长度

        System.out.println("数组的长度为：" + arr.length);

        //数组的特点：固定长度，里面存储的内容可以修改
        String s = arr[0]; //取出对应位置的值

        //String b = arr[1];

        System.out.println(s);

        //给下标为0的位置进行赋值
        arr[0] = "spark";

        System.out.println(arr[0]);

        arr[1] = "hive";
        arr[1] = "hadoop"; //后面赋值的数据会覆盖前面的数据

        System.out.println(arr[1]);


    }
}

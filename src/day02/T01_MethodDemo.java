package day02;

/**
 * 开发程序归根结底就是掉方法
 * 方法的定义：程序中最小的执行单元，就是将多行代码进行进行了封装，完成相应的运算逻辑
 */
public class T01_MethodDemo {


    /**
     * public 公有的方法
     * static 静态的方法
     * double 返回值类型（方法名前面的类型）
     * add 方法名
     * () 括号里面装的：参数列表，描述了参数的类型和个数，a、b是给传入方法的参数取的名字，也叫做方法的形参
     * {} 大括号里面的内容叫做方法体
     * 如果方法有返回必须在后面加上return
     * @param a
     * @param b
     * @return
     */
    public static double add(double a, double b) {
        //double c = a + b;
        //return c;
        return a + b;
    }


    /**
     * void 该方法没有返回值，不需要返回
     * () 方法的参数列表，空参的参数列表
     */
    private static void sayHello() {
        System.out.println("hello everyone");

    }


    public static void sayHi(String name) {
        System.out.println("hi " + name);
    }

    public static void main(String[] args) {
        //直接调用
        //double r = T01_MethodDemo.add(3.5, 4.1);

        //方法的调用：方法名(参数1,参数2)
        double r = add(3.5, 4.1);
        System.out.println(r);

        //方法的效应：方法名()
        sayHello();

        sayHi("tom");
    }

}

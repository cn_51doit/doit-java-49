package day02;

/**
 * java中的循环： for循环和while循环
 */
public class T11_ForDemo {

    public static void main(String[] args) {

        String[] arr = {"aaa", "bbb", "ccc", "ddd", "eee", "fff", "ggg"};

        System.out.println("数组的长度：" + arr.length);

//        System.out.println(arr[0]);
//        System.out.println(arr[1]);
//        System.out.println(arr[2]);
//        System.out.println(arr[3]);
//        System.out.println(arr[4]);
//        System.out.println(arr[5]);
//        System.out.println(arr[6]);

        for(int i = 0; i < arr.length; i++) {
            // 循环体
            System.out.println(arr[i]);
        }



    }
}

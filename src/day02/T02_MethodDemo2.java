package day02;

public class T02_MethodDemo2 {

    public static void main(String[] args) {

        //静态的方法，不在同一个类中，类名.方法名
        User.sayHei();

        //私有的方法，在其他类中就无法调用了（访问权限控制）
        //User.sayHeHe()

        User u = new User();

        u.printName("wangwu");

    }
}

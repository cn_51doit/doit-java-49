package day02;

/**
 * 类名叫Teacher
 */

public class Teacher {

    //静态变量，用static关键字修饰了
    public static String NAME = "人民教师";

    //下面是非静态的，成员变量
    public String name;
    public int age;

    public void sayHi() {
        System.out.println("hi~~~~~" + name);
    }

    /**
     * 括号前面的叫方法名
     * 方法名的前面是返回值类型（java程序只能有一个返回值类型）
     * 方法名后面的括号叫参数列表，描述了参数的个数和类型, a 和 b 叫形参
     * 方法有返回值，一定用有return关键字将结果返回
     */
    public static int add(int a, int b) {
        int c = a + b; //运算逻辑
        return c; //方法有返回值一定要用return
    }

    /**
     * void 无返回值的
     */
    public static void printName(String name) {
        System.out.println("打印的名称为：" + name);
    }


    /**
     * 程序执行的入口
     */
    public static void main(String[] args) {
        //调用Teacher的add方法，一个等号代表赋值
        int bbb = Teacher.add(10, 30); //add 加法
        System.out.println(bbb);

        //Teacher.printName("张三");
        printName("张三");

        // 静态变量
        System.out.println(Teacher.NAME);

        //非静态的方法，即成员方法，先创建该类的对象（实例）
        Teacher t1 = new Teacher();

        t1.name = "老王";
        t1.age = 18;
        t1.sayHi(); //调用t1的sayHi
        System.out.println(Teacher.NAME + " t1对象中的: name : " + t1.name + ", age:" + t1.age);


        Teacher t2 = new Teacher();
        t2.name = "老赵";
        t2.age = 35;
        t2.sayHi(); //调用t2的sayHi
        System.out.println(Teacher.NAME + " t2对象中的: name : " + t2.name + ", age:" + t2.age);







    }
}

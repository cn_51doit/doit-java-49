package day02;

/**
 * 演示java的运算符
 */
public class T09_OperatorsDemo {

    public static void main(String[] args) {

        int i = 10;
        int j = i + 1;
        System.out.println(j);

        //将运算后的结果给i赋值，以前的值就被覆盖了
        //i = i + 1;
        //i += 1;
        i++;

        System.out.println("赋值后的i：" + i);


    }
}

package day02;

import java.util.Arrays;

public class T03_StringMethod {

    public static void main(String[] args) {

        //字符串（多个字符连在一起叫字符串）
        String str = "abcdecfg";
        //String s2 = new String("abcdefgh");


        //返回字符串中你要查找的字符或字符串第一次出现的索引（下标）
        //int i = str.indexOf("c");
        //打印的结果2
        //System.out.println(i);

        //返回字符串中指定下标位置的字符
        //char c1 = str.charAt(2);
        //打印的结果c
        //System.out.println(c1);

        //从指定的下标开始，一直截取到最后返回
        //String s2 = str.substring(2);
        //打印的结果：cdecfg
        //System.out.println(s2);

        //冲指定的下标开始截取字符串，到指定的下标，是一个前闭后开的[2, 8)
        //String s3 = str.substring(2, 8);
        //System.out.println(str.length());
        //String s3 = str.substring(2, str.length());

        //System.out.println(s3);





        String s = new String("abcdefghijk");

        int index = s.indexOf("d");
        System.out.println(index);

        //abcdefghijk
        //012345
        String s2 = s.substring(0, 5);//截取字符串
        System.out.println(s2);


        String line = "spark hive flink hadoop";

        String[] words = line.split(" ");

        String w = words[1];
        System.out.println(w);

        String w2 = words[2];
        String w3 = w2.toUpperCase();
        System.out.println(w3);

        words[2] = w3;

        System.out.println(words);
        System.out.println(Arrays.toString(words));
    }
}

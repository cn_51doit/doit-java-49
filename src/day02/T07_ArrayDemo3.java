package day02;

public class T07_ArrayDemo3 {

    public static void main(String[] args) {

        //double[] arr = new double[5];
        //System.out.println(arr[1]);

        double[] arr = {1.1, 2.0, 4.5, 6.6, 1};

        System.out.println("数组的长度为：" + arr.length);

        //取值
        double d1 = arr[4];
        System.out.println(d1);

        //赋值
        arr[4] = 9.9;

        System.out.println(arr[4]);
    }
}

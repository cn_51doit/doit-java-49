package day02;


import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * 统计文件中有多少行，多少个单词，并且过滤掉error这个单纯
 */
public class T15_ScannerDemo {

    public static void main(String[] args) throws Exception {

        //int i = 10;
        //User u = new User();

        //File是java操作文件对应的类
        //File file = new File("C:\\Users\\star\\IdeaProjects\\doit-java\\data\\words.txt");
        File file = new File("data/words.txt");
        //想要使用Scanner这个工具类，帮我完成读取指定位置文件的内容
        Scanner sc = new Scanner(file);

        //用于累加函数的变量
        int lineCount = 0;
        //用于累计单词的计算器
        int wordCount = 0;
        //判断是否有新的一行
        while (sc.hasNextLine()) {
            //调用nextLine方法就可以读取一行数据了
            String line = sc.nextLine();
            //对一行字符串进行切分
            String[] words = line.split(" ");
            //wordCount += words.length;
            //如果字符串中有error这个单词，那么就不计数，所以必须对数组进行循环
            for (int i = 0; i < words.length; i++) {
                String word = words[i]; //取出指定位置的单词
                if (!"error".equals(word)) {
                    //判断不是error单词后
                    wordCount++;
                }
            }

            lineCount++; //每循环一行，行数+1
        }

        //关闭IO
        sc.close();

        System.out.println("文件的行数：" + lineCount + " 文件中单词的个数：" + wordCount);


    }
}

package day02;

public class T16_TestMethod {

    public static double add(double a, double b) {
        return a + b;
    }


    public static void main(String[] args) {

        double m = add(2.1, 3.4);
        System.out.println(m);

        String s = "ABCDEFGHIJK";
        String r = s.substring(2);
        System.out.println(r);

        String line = "spark hive flink hadoop hive spark";
        //String[] words = line.split(" ");
        String[] words = line.split(" ");
        System.out.println(words.length);


    }
}

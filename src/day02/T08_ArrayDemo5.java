package day02;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class T08_ArrayDemo5 {

    public static void main(String[] args) throws FileNotFoundException {

        //定义数组的方式
        //User[] users = new User[5];

        //第一种
        String[] arr1 = new String[5];

        //第二种
        String[] arr2 = new String[]{"aaa", "bbb", "ccc"};


        //第三种
        String[] arr3 = {"aaa", "bbb", "ccc", "6.6"};
        System.out.println(arr3.length);

        //取值
        String s1 = arr2[1];

        //赋值
        arr2[2] = "CCCCCC";

        System.out.println(arr2[2]);

    }
}

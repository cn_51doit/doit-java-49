package day02;

public class T06_ArrayDemo2 {

    public static void main(String[] args) {
        //定义一个int类型的变量
        int i = 10;

        //定义一个int类型的数组
        int[] arr = new int[6];

        //打印长度
        System.out.println("数组的长度为：" + arr.length);

        //取值
        int m = arr[1];
        System.out.println(m);

        //赋值
        arr[0] = 10;
        arr[1] = 20;
        arr[2] = 30;
        arr[3] = 40;
        arr[4] = 50;
        arr[5] = 60;

        //取值
        System.out.println("下标为3对应的数据为："+ arr[3]);




    }
}

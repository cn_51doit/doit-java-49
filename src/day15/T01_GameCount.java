package day15;

import java.io.*;
import java.util.*;

/**
 * 使用ArrayList实现游戏充值金额TopN的功能
 */
public class T01_GameCount {

    public static void main(String[] args) throws IOException {
        //key 游戏id,分区id
        //value 金额
        HashMap<String, Double> gidAndZidToMoney = new HashMap<>();
        //key 游戏ID
        //value 装了多个分区id和Money拼接字符串的List
        HashMap<String, ArrayList<String>> gidToZidAndMoneyList = new HashMap<>();
        File file = new File("data/game.txt");

        FileReader reader = new FileReader(file);
        //创建一个增强的包装类BufferedReader
        BufferedReader bufferedReader = new BufferedReader(reader);

        //如果没有新的内容了，会返回null
        String line = null;
        while ((line = bufferedReader.readLine()) != null) {
            String[] fields = line.split(",");
            String gid = fields[0]; //游戏ID
            String zid = fields[1]; //分区ID
            double money = Double.parseDouble(fields[3]);
            //将gid和zid进行拼接
            String key = gid + "," + zid;
            Double accMoney = gidAndZidToMoney.getOrDefault(key, 0.0);
            accMoney += money;
            //将累加后的数据put到gidAndZidToMoney
            gidAndZidToMoney.put(key, accMoney);
        }
        //将累加后的数据，进行放到ArrayList中进行排序
        for (Map.Entry<String, Double> entry : gidAndZidToMoney.entrySet()) {
            String gidAndZid = entry.getKey();
            //切分
            String[] fields = gidAndZid.split(",");
            String gid = fields[0];
            String zid = fields[1];
            Double money = entry.getValue();
            //将数据添加到ArrayList中
            ArrayList<String> list = gidToZidAndMoneyList.get(gid);
            if (list == null) {
                list = new ArrayList<>();
                //将新创建的list添加到gidToZidAndMoneyList中
                gidToZidAndMoneyList.put(gid, list);
            }
            list.add(zid + "," + money);
        }
        for (Map.Entry<String, ArrayList<String>> entry : gidToZidAndMoneyList.entrySet()) {
            String gid = entry.getKey();
            ArrayList<String> list = entry.getValue();
            list.sort(new Comparator<String>() {
                @Override
                public int compare(String o1, String o2) {
                    double m1 = Double.parseDouble(o1.split(",")[1]);
                    double m2 = Double.parseDouble(o2.split(",")[1]);
                    return -Double.compare(m1, m2);
                }
            });
            //输出结果
            for (int index = 0; index < Math.min(3, list.size()); index++) {
                String zidAndMoney = list.get(index);
                System.out.println(gid + "," + zidAndMoney + "," + (index + 1));
            }
        }
    }
}

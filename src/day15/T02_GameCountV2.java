package day15;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

/**
 * 使用TreeSet实现游戏充值金额TopN的功能
 * 扩展知识：有界优先队列
 */
public class T02_GameCountV2 {

    public static void main(String[] args) throws FileNotFoundException {
        //key 游戏id,分区id
        //value 金额
        HashMap<String, Double> gidAndZidToMoney = new HashMap<>();
        //key 游戏ID
        //value 装了多个分区id和Money的BeanBean TresSet
        HashMap<String, TreeSet<GameBean>> gidToZidAndMoneySet = new HashMap<>();

        File file = new File("data/game.txt");

        Scanner scanner = new Scanner(file);
        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            String[] fields = line.split(",");
            String gid = fields[0]; //游戏ID
            String zid = fields[1]; //分区ID
            double money = Double.parseDouble(fields[3]);
            //将gid和zid进行拼接
            String key = gid + "," + zid;
            Double accMoney = gidAndZidToMoney.getOrDefault(key, 0.0);
            accMoney += money;
            //将累加后的数据put到gidAndZidToMoney
            gidAndZidToMoney.put(key, accMoney);
        }

        //将累加后的数据，将结果方法TresSet中进行排序
        for (Map.Entry<String, Double> entry : gidAndZidToMoney.entrySet()) {
            String gidAndZid = entry.getKey();
            //切分
            String[] fields = gidAndZid.split(",");
            String gid = fields[0];
            String zid = fields[1];
            Double money = entry.getValue();
            //将数据添加到Set中
            GameBean bean = new GameBean(zid, money);
            TreeSet<GameBean> treeSet = gidToZidAndMoneySet.get(gid);
            if (treeSet == null) {
                treeSet = new TreeSet<>(new Comparator<GameBean>() {
                    @Override
                    public int compare(GameBean o1, GameBean o2) {
                        return Double.compare(o2.getMoney(), o1.getMoney());
                    }
                });
                //第一次创建的TreeSet put到gidToZidAndMoneySet
                gidToZidAndMoneySet.put(gid, treeSet);
            }
            //将封装好数据的GameBean添加到treeSet中，会自动排序
            treeSet.add(bean);
            //添加元素后，如果长度大于3，就移除到最后一个
            if (treeSet.size() > 3) {
                //由于是降序排序，取出最后一个就是最小的
                GameBean last = treeSet.last();
                //移除最小的
                treeSet.remove(last);
            }
        }

        //遍历结果
        for (Map.Entry<String, TreeSet<GameBean>> entry : gidToZidAndMoneySet.entrySet()) {
            String key = entry.getKey();
            TreeSet<GameBean> treeSet = entry.getValue();
            //将一个集合转成迭代器（用来迭代数据的工具）
            Iterator<GameBean> iterator = treeSet.iterator();
            int rank = 1;
            while (iterator.hasNext()) {
                GameBean next = iterator.next();
                System.out.println(key +"," + next.getZid() + "," + next.getMoney() + "," + rank);
                rank++;
            }

        }

    }
}

package day15;


public class GameBean {

    private String zid;

    private double money;

    public GameBean() {}

    public GameBean(String zid, double money) {
        this.zid = zid;
        this.money = money;
    }

    public String getZid() {
        return zid;
    }

    public void setZid(String zid) {
        this.zid = zid;
    }

    public double getMoney() {
        return money;
    }

    public void setMoney(double money) {
        this.money = money;
    }


}

package day19;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class T08_FileOutputStreamDemo {

    public static void main(String[] args) throws IOException {

        String data = "Aahello"; //在内存中
        //将字符串转成字节数组
        byte[] bytes = data.getBytes();
//        for (byte aByte : bytes) {
//            System.out.println(aByte);
//        }

        //创建一个文件
        File file = new File("/Users/star/Desktop/data.txt");
        FileOutputStream fos = new FileOutputStream(file);
        fos.write(bytes); //将数据写入到文件中
        fos.close(); //关闭流

    }
}

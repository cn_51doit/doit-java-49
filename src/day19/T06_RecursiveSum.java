package day19;

/**
 * 计算等差数列的和
 */
public class T06_RecursiveSum {

    //1 + 2 + 3 + 4 + 5
    public static int getSum(int i) {
        if (i == 1) {
            return 1;
        }
        return i + getSum(i - 1);
    }

    //斐波那契数列
    public static void main(String[] args) {

        int sum = getSum(5);
        System.out.println(sum);

    }
}

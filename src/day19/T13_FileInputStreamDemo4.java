package day19;


import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;

public class T13_FileInputStreamDemo4 {

    public static void main(String[] args) throws IOException {

        FileInputStream fis = new FileInputStream("/Users/star/Desktop/data.txt");

        //int b = fis.read(); 一次只读取一个字节，读取后，会将读取到的字节返回

        byte[] buff = new byte[10];

        //定义一个int类型的变量叫len
        int len = 0;
        while ((len = fis.read(buff)) != -1) {
            String line = new String(buff, 0, len);
            System.out.print(line);
        }



    }
}

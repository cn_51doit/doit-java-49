package day19;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class T16_BufferedReader {

    public static void main(String[] args) throws IOException {

        //装饰模式（包装模式）
        //FileReader目标类
        FileReader reader = new FileReader("/Users/star/Desktop/test2");

        //带缓存的Reader，内部有一个字符数组，长度默认为8192
        //new BufferedReader，将原来的FileReader作为参数传入到BufferedReader的构造方法中了
        //BufferedReader装饰类或增强类
        BufferedReader bufferedReader = new BufferedReader(reader);

        //如果没有新的内容了，会返回null
        String line = null;
        while ((line = bufferedReader.readLine()) != null) {
            System.out.println(line);
        }

        bufferedReader.close();
        reader.close();

    }
}

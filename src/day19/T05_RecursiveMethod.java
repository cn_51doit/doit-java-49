package day19;

/**
 * 递归方法的演示
 */
public class T05_RecursiveMethod {

    static int i = 0;
    public static void m1() {
        i += 1;
        System.out.println("m1方法被调用了~");
        if (i < 10) {
            m1();
        }

    }


    public static void main(String[] args) {
        m1();
    }
}

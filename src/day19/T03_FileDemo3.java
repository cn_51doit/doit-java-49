package day19;

import java.io.File;
import java.io.IOException;

public class T03_FileDemo3 {

    public static void main(String[] args) throws IOException {

//        File file = new File("/Users/star/Documents/dev/doit49/doit-java/data/math");
//        System.out.println("该file是文件类型的：" + file.isFile());
//        System.out.println("该file是目录类型的：" + file.isDirectory());
//        System.out.println("该file是否存在："+ file.exists());

        //创建一个新文件
        File file = new File("/Users/star/Documents/dev/doit49/doit-java/data/math/");
//        if (!file.exists()) { //如果不存在
//            file.createNewFile();
//        }

        //删除一个文件
//        if (file.exists()) {
//            boolean delete = file.delete();
//            System.out.println("是否删除成功：" + delete);
//        }

        //在给文件的同一级目录创建一个新的目录
        //boolean mkdir = file.mkdir(); //如果改目录已经存在，就返回false
        //System.out.println(mkdir);

        File[] files = file.listFiles();

        //获取指定目录的文件，只要文件不要目录，并且文件中必须有内容
        for (File f : files) {
            if (f.isFile() && f.length() > 0) {
                String name = f.getName();
                System.out.println(name);
            }
        }


    }
}

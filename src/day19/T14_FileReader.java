package day19;


import java.io.File;
import java.io.FileReader;
import java.io.IOException;

/**
 * 字符流
 */
public class T14_FileReader {

    public static void main(String[] args) throws IOException {

        FileReader reader = new FileReader(new File("/Users/star/Desktop/data.txt"));

        char[] buff = new char[100];

//        int len = reader.read(buff);
//        String str = new String(buff);
//        System.out.println("读取出来的内容：" + str);
//        System.out.println("内容的长度：" + len);

        int len = 0;
        while ((len = reader.read(buff)) != -1) {
            String data = new String(buff, 0, len);
            String[] lines = data.split("\n");
            for (String line : lines) {
                System.out.println(line);
            }
            //System.out.print(line);
        }



    }
}

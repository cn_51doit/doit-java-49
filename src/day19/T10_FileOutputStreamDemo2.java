package day19;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class T10_FileOutputStreamDemo2 {

    public static void main(String[] args) throws IOException {

        String line = "spark hive flink hadoop hbase";
        File file = new File("/Users/star/Desktop/data.txt");
        FileOutputStream fos = new FileOutputStream(file, true); //可以追加内容

        String[] words = line.split(" ");
        for (String word : words) {
            byte[] bytes = word.getBytes();
            fos.write(bytes); //将字节数组写入到文件中
            fos.write(10); //换行
        }

        fos.close();

    }
}

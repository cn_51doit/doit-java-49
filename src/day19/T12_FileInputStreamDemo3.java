package day19;


import java.io.FileInputStream;
import java.io.IOException;

public class T12_FileInputStreamDemo3 {

    public static void main(String[] args) throws IOException {

        FileInputStream fis = new FileInputStream("/Users/star/Desktop/data.txt");

        //int b = fis.read(); //一次只读取一个字节，读取后，会将读取到的字节返回
        //System.out.println((char)b);

        byte[] bytes = new byte[10];
        //调用read方法，传入字节数组，会将读取的多个字节，放到字节数组中
        //该方法会将读取的长度返回
        int len = fis.read(bytes);

        String str = new String(bytes, 0, len);
        System.out.println("读出来的内容："+ str);
        System.out.println("内容的长度："+ len);

    }
}

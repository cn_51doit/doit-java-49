package day19;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;

public class T09_FileInputStreamDemo {

    public static void main(String[] args) throws IOException {

        //File file = new File("/Users/star/Desktop/data.txt");
        //FileInputStream fis = new FileInputStream(file);

        FileInputStream fis = new FileInputStream("/Users/star/Desktop/data.txt");

        //int b = fis.read(); //读取一个字节
        //System.out.println(b);

        byte[] bytes = new byte[10];
        //将文件中读取到的字节，放到byte数组中
        fis.read(bytes);
        //System.out.println(Arrays.toString(bytes));
        //将字节数组转成zfc
        String line = new String(bytes);
        System.out.println(line);


        fis.close(); //关闭流


    }
}

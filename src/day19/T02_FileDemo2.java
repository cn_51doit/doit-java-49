package day19;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;

public class T02_FileDemo2 {

    public static void main(String[] args) throws IOException {

        //File file = new File("/Users/star/Documents/dev/doit49/doit-java/data/math", "1.txt");

        File parentFile = new File("/Users/star/Documents/dev/doit49/doit-java/data/math");
        File file = new File(parentFile, "4.txt");

        //Scanner scanner = new Scanner(file);
//        while (scanner.hasNextLine()) {
//            String line = scanner.next();
//            System.out.println(line);
//        }

        //返回决定路径
        String absolutePath = file.getAbsolutePath();
        System.out.println(absolutePath);

        //堆内存中的地址是不一样的
        //File absoluteFile = file.getAbsoluteFile();
        //System.out.println(absoluteFile == file);

        //File canonicalFile = file.getCanonicalFile();
        //System.out.println(canonicalFile);

        //类似getAbsolutePath，或根据当前系统的分割符，返回相应的路径
        String path = file.getPath();
        System.out.println(path);

        //获取当前文件的名称，不包含路径
        String name = file.getName();
        System.out.println(name);

        //获取当前文件的父路径
        String parent = file.getParent();
        System.out.println(parent);

        //返回父目录的对象
        File parentFile1 = file.getParentFile();
        System.out.println(parentFile1);


        long length = file.length();
        System.out.println(length);


    }
}

package day19;

import java.io.File;

public class T01_FileDemo {

    public static void main(String[] args) {

        //文件路径分隔符, windows是\  Linux、Unix是/
        String separator = File.separator;
        System.out.println(separator);

        String path = File.separator + "ddd" + File.separator + "a.txt";

        System.out.println(path);

        //环境变量分隔符,windows是;  Linux、Unix是:
        String pathSeparator = File.pathSeparator;
        System.out.println(pathSeparator);




    }
}

package day19;

import java.io.File;
import java.io.FilenameFilter;

/**
 * 递归打印目录中的文件
 */
public class T07_RecursivePrintFile {

    static int level = 0;

    public static void printFile(File dir) {

        ///Users/star/Desktop/test01
        String path = dir.getPath();
        path = path.substring(path.lastIndexOf(File.separator) + 1);

        File[] files = dir.listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                return !name.startsWith("."); //Mac上有隐藏文件，隐藏文件以.开始
            }
        });

        String tabStr = "";
        for (int i = 0; i < level; i++) {
            tabStr += "\t";
        }

        for (File file : files) {
            if (file.isFile()) {
                System.out.println(tabStr + File.separator + path + File.separator + file.getName());
            } else {
                level += 1;
                printFile(file); //递归到目录中
            }
        }


    }


    public static void main(String[] args) {

        //要打印的根目录
        File file = new File("/Users/star/Desktop/test01");
        printFile(file);
    }
}

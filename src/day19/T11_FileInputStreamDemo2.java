package day19;

import java.io.FileInputStream;
import java.io.IOException;

public class T11_FileInputStreamDemo2 {

    public static void main(String[] args) throws IOException {

        FileInputStream fis = new FileInputStream("/Users/star/Desktop/data.txt");

        //一次读取一个字节，如果读取到文件的末尾，返回-1
        //int b = fis.read();
        //System.out.println(b);

        int b = 0; //外面定义一个int类型的变量b
        //将输入流fis读取一个字节，赋值给b，然后用括号括起来，对p进行逻辑判断，判断是否等于-1，如果不是-1，说明还有新的内容
        while ((b = fis.read()) != -1) { //如果返回-1，说明文件已经读取到末尾了
            System.out.println((char)b);
        }


    }
}

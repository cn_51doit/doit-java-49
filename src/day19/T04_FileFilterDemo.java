package day19;

import java.io.File;
import java.io.FileFilter;

public class T04_FileFilterDemo {

    public static void main(String[] args) {

        File file = new File("/Users/star/Desktop/test01");
        if (file.isDirectory()) {
            File[] files = file.listFiles(new FileFilter() {
                @Override
                public boolean accept(File file) {
                    //System.out.println("列出文件时传入的文件"+ file.getName());
                    if (file.isFile()) { //不要目录只要文件
                        String name = file.getName();
                        if (name.endsWith(".txt")) {
                            return true;
                        }
                    }
                    return false; //accept方法返回true，就会放到数组中
                }
            });
            //取出该知道目录下的全部文件和文件夹
            for (File f : files) {
                System.out.println("取出来的文件：" + f.getName());
            }
        }



    }
}

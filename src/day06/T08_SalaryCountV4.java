package day06;

import day04.DeptInfo;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class T08_SalaryCountV4 {

    public static void main(String[] args) throws FileNotFoundException {

        HashMap<String, DeptInfo> nameToInfo = new HashMap<String, DeptInfo>();
        File file = new File("data/emp.txt");
        Scanner scanner = new Scanner(file);

        while (scanner.hasNextLine()) {
            //判断入职日期消息2024年的
            String[] fields = scanner.nextLine().split(",");
            int year = Integer.parseInt(fields[2].split("-")[0]);
            if (year < 2024) {
                String deptName = fields[1];
                double salary = Double.parseDouble(fields[3]) + Double.parseDouble(fields[4]);

                DeptInfo info = nameToInfo.get(deptName);
                if (info == null) { //该部门第一次出现
                    info = new DeptInfo();
                    nameToInfo.put(deptName, info);
                }
                info.sumSal = info.sumSal + salary;
                info.maxSal = Math.max(info.maxSal, salary);
                info.minSal = Math.min(info.minSal != 0.0 ? info.minSal : salary, salary);
                info.amount = info.amount + 1;
                //要将累加后的数据添加到map中
                //nameToInfo.put(deptName, info);
            }
        }

        //循环map
        Set<Map.Entry<String, DeptInfo>> entries = nameToInfo.entrySet();
        for (Map.Entry<String, DeptInfo> entry : entries) {
            String deptName = entry.getKey();
            DeptInfo deptInfo = entry.getValue();
            System.out.println("部门名称：" + deptName + " 总薪水：" + deptInfo.sumSal + " , 最大薪水：" + deptInfo.maxSal + " , 最小薪水：" + deptInfo.minSal + " , 平均薪水：" + deptInfo.sumSal / deptInfo.amount);
        }

        scanner.close();


    }
}

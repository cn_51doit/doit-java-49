package day06;

import java.util.ArrayList;
import java.util.Collections;

/**
 * 演示List的排序
 */
public class T03_ArrayListSort {

    public static void main(String[] args) {

        ArrayList<Integer> nList = new ArrayList<>();

        nList.add(7);
        nList.add(3);
        nList.add(9);
        nList.add(5);
        nList.add(1);
        nList.add(8);
        nList.add(6);
        nList.add(4);
        nList.add(2);


        for (int i = 0; i < nList.size(); i++) {
            System.out.println("当前List下标为" + i  + " 对应的元素为：" + nList.get(i));
        }

        //对List进行排序
        Collections.sort(nList); //对当前List中的元素按照默认的规则(按照Integer的升序)进行排序

        //排序后，数据还在原来的List中，只是顺序发生了变化
        //TimSort(目前基于内存最快的排序算法)
        for (int i = 0; i < nList.size(); i++) {
            System.out.println("排序后List下标为" + i  + " 对应的元素为：" + nList.get(i));
        }

    }
}

package day06;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class T05_SortDemo {

    public static void main(String[] args) throws FileNotFoundException {

        File file = new File("data/access.txt");
        Scanner scanner = new Scanner(file);

        ArrayList<String> dtList = new ArrayList<>();

        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            String[] fields = line.split(",");
            dtList.add(fields[1]);
        }

        Collections.sort(dtList);

        for (int i = 0; i < dtList.size(); i++) {
            System.out.println(dtList.get(i));
        }


    }
}

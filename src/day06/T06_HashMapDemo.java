package day06;

import java.util.HashMap;

public class T06_HashMapDemo {

    public static void main(String[] args) {

        //创建一个HashMap，需要指定key和value，key和value是一一对应的

        //通过泛型，指定要存储的key和value的类型
        HashMap<String, Integer> wordToCountMap = new HashMap<>();

        //向map中添加数据
        wordToCountMap.put("spark", 8);
        wordToCountMap.put("hive", 10);
        wordToCountMap.put("hadoop", 13);
        wordToCountMap.put("hbase", 5);
        wordToCountMap.put("scala", 7);

        //根据key取数据
        Integer count = wordToCountMap.get("hadoop");

        System.out.println(count);

        //再次将hadoop put到 该map中,即将key相同的数据put到map中，后面的value会覆盖前面的value
        wordToCountMap.put("hadoop", 140);

        Integer count2 = wordToCountMap.get("hadoop");
        System.out.println(count2);

        //取doris这个key，返回的count3 为 null
        Integer count3 = wordToCountMap.get("doris");
        System.out.println(count3);

        //getOrDefault取doris这个key，返回的count4 为 0
        //Integer count4 = wordToCountMap.getOrDefault("doris", 0);
        Integer count4 = wordToCountMap.getOrDefault("hadoop", 0);
        System.out.println(count4);

    }
}

package day06;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Map的Key和Value可以是各种类型的，基本类型的包装类型，也可以是自定义的类型，也可以是集合类型
 */
public class T13_MapDemo1 {

    public static void main(String[] args) {

        HashMap<Integer, ArrayList<String>> numToListMap = new HashMap<>();

        //一线城市
        ArrayList<String> lv1Cities = new ArrayList<>();
        lv1Cities.add("北京");
        lv1Cities.add("上海");
        lv1Cities.add("广州");
        lv1Cities.add("深圳");

        //将一线城市对应的List添加到Map中
        numToListMap.put(1, lv1Cities);
        
        //二线城市
        ArrayList<String> lv2Cities = new ArrayList<>();
        lv2Cities.add("天津");
        lv2Cities.add("沈阳");
        lv2Cities.add("武汉");
        lv2Cities.add("杭州");

        //将二线城市对应的List添加到Map中
        numToListMap.put(2, lv2Cities);
        
        //三线城市
        ArrayList<String> lv3Cities = new ArrayList<>();
        lv3Cities.add("廊坊");
        lv3Cities.add("锦州");
        lv3Cities.add("芜湖");

        //将三线城市对应的List添加到Map中
        numToListMap.put(3, lv3Cities);
        
        
        //取出所有的2线城市
        ArrayList<String> cities = numToListMap.get(2);
        
        for (String city : cities) {
            System.out.println(city);
        }

        System.out.println("--------------");

//        Set<Map.Entry<Integer, ArrayList<String>>> entries = numToListMap.entrySet();
//        for (Map.Entry<Integer, ArrayList<String>> entry : entries) {
//               
//        }

        for (Map.Entry<Integer, ArrayList<String>> entry : numToListMap.entrySet()) {
            Integer num = entry.getKey();
            ArrayList<String> cityList = entry.getValue();
            System.out.println(num + "线城市有：");
            for (String city : cityList) {
                System.out.println(city);
            }
        }
        
    }
}

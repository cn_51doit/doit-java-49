package day06;

import java.util.HashSet;

/**
 * HashSet的特点：无序、不重复
 */
public class T11_HashSetDemo {

    public static void main(String[] args) {

        //Set集合
        HashSet<String> words = new HashSet<>();
        
        words.add("spark");
        words.add("hive");
        words.add("hive");
        words.add("spark");
        words.add("flink");
        words.add("hbase");
        words.add("hadoop");

        System.out.println("Set的size: " + words.size());
        
        for (String word : words) {
            System.out.println(word);
        }
        
        


    }
}

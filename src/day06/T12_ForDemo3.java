package day06;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * 使用增强for循环，循环map
 */
public class T12_ForDemo3 {

    public static void main(String[] args) {

        Map<String, Integer> wordCount = new HashMap<>();
        
        wordCount.put("spark", 10);
        wordCount.put("hive", 20);
        wordCount.put("hbase", 30);
        wordCount.put("flink", 40);
        wordCount.put("flume", 50);

        //循环所有的key
        Set<String> keys = wordCount.keySet();

        for (String key : keys) {
            System.out.println(key);
        }

        System.out.println("---------------");
        
        //循环所有的value
        Collection<Integer> values = wordCount.values();
        for (Integer count : values) {
            System.out.println(count);
        }

        System.out.println("---------------");
        
        //同时循环key和value
        Set<Map.Entry<String, Integer>> entries = wordCount.entrySet();

        //entry代表一对key和value
        for (Map.Entry<String, Integer> entry : entries) {
            String key = entry.getKey();
            Integer value = entry.getValue();
            System.out.println(key + " -> " + value);
        }
    }
}

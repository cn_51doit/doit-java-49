package day06;

import java.util.ArrayList;

/**
 * 增强的for循环
 * 可以循环数组、List、Set、Map、Iterator
 */
public class T10_ForDemo2 {

    public static void main(String[] args) {

        ArrayList<String> sList = new ArrayList<>();
        
        sList.add("spark");
        sList.add("hive");
        sList.add("flink");
        sList.add("hadoop");
        sList.add("hbase");
        
        //循环ArrayList
        for (String word : sList) {
            System.out.println(word);
        }

//        int i = 0;
//        for (String w : sList) {
//            String word = sList.get(i);
//            System.out.println(word);
//            i++;
//        }
        
    }
}

package day06;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Map的Key和Value可以是各种类型的，基本类型的包装类型，也可以是自定义的类型，也可以是集合类型
 * 省份 -> {城市 -> 成交金额}
 */
public class T14_MapDemo2 {

    public static void main(String[] args) {


        //山东省 -> {济南市 -> 3000, 烟台市 -> 2000}
        //河北省 -> {廊坊市 -> 1000, 邢台市 -> 5000}
        //辽宁省 -> {沈阳市 -> 2000, 本溪市 -> 3000}
        HashMap<String, HashMap<String, Double>> provinceToCityAndMoney = new HashMap<>();


        //山东省的数据
        HashMap<String, Double> sdCityAndMoney = new HashMap<>();
        sdCityAndMoney.put("济南市", 3000.0);
        sdCityAndMoney.put("烟台市", 2000.0);
        sdCityAndMoney.put("威海市", 4000.0);

        provinceToCityAndMoney.put("山东省", sdCityAndMoney);
        
        //河北省的数据
        HashMap<String, Double> hbCityAndMoney = new HashMap<>();
        hbCityAndMoney.put("廊坊市", 1000.0);
        hbCityAndMoney.put("邢台市", 5000.0);

        provinceToCityAndMoney.put("河北省", hbCityAndMoney);
        
        //辽宁省的数据
        HashMap<String, Double> lnCityAndMoney = new HashMap<>();
        lnCityAndMoney.put("沈阳市", 2000.0);
        lnCityAndMoney.put("本溪市", 3000.0);
        
        provinceToCityAndMoney.put("辽宁省", lnCityAndMoney);
        
        //取出河北省的数据
        HashMap<String, Double> hbCities = provinceToCityAndMoney.get("河北省");

        for (Map.Entry<String, Double> entry : hbCities.entrySet()) {
            System.out.println(entry.getKey() + " , " + entry.getValue());
        }

        System.out.println("----------");

        for (Map.Entry<String, HashMap<String, Double>> entry : provinceToCityAndMoney.entrySet()) {
            String province = entry.getKey();
            System.out.println(province);
            HashMap<String, Double> cityToMoney = entry.getValue();
            for (Map.Entry<String, Double> entry2 : cityToMoney.entrySet()) {
                String city = entry2.getKey();
                Double money = entry2.getValue();
                System.out.println(city + " : " + money);
            }
        }
        
        

    }
}

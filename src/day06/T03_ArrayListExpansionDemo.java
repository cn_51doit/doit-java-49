package day06;

import java.util.ArrayList;

/**
 * 演示 ArrayList自动扩容的效果
 */
public class T03_ArrayListExpansionDemo {

    public static void main(String[] args) {

        //new的时候，可以在构造方法可以指定该List的初始长度，如果不指定，默认是10
        ArrayList<String> strList = new ArrayList<String>();

        strList.add("A"); //0
        strList.add("B");
        strList.add("C");
        strList.add("D");
        strList.add("E");
        strList.add("F"); //5
        strList.add("G");
        strList.add("H");
        strList.add("I");
        strList.add("J"); //9
        strList.add("H"); //10

        System.out.println("当前的size：" + strList.size());

        for (int i = 0; i < strList.size(); i++) {
            System.out.println("当前List的下标：" + i + ", 当前下标的元素：" + strList.get(i));
        }


    }
}

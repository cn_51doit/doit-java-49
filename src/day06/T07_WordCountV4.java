package day06;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;
import java.util.Set;

public class T07_WordCountV4 {

    public static void main(String[] args) throws FileNotFoundException {

        long startTime = System.currentTimeMillis(); //获取当前的时间
        System.out.println(startTime);
        //int lineCount = 0; //文件的总函数
        //int wordCount = 0; //文件中除了error以外单词的数量
        HashMap<String, Integer> wordToCount = new HashMap<>();

        //创建一个文件对象
        File file = new File("data/words-big.txt");
        //创建读完文件的工具类
        Scanner scanner = new Scanner(file);
        //开始一行一行的读取数据
        while (scanner.hasNextLine()) { //判断文件中是否有未读取的数据，如果返回true，继续读取
            //lineCount++; //统计文件中的行数
            String line = scanner.nextLine(); //读出来一行内容，使用字符串接收
            String[] words = line.split(" "); //将一行内容对应的字符串进行切分，返回多个单词，放到数组中
            for (int i = 0; i < words.length; i++) {
                String word = words[i]; //取出一个单词
                if (!"error".equals(word)) {
                    //wordCount++;
                    //统计每个单词出现的次数
                    Integer count = wordToCount.get(word);
                    if (count == null) { //该单词第一次出现
                        count = 0;
                    }
                    //count++;
                    wordToCount.put(word, ++count); //将该单词累加后的次数，在put到map中
                }
            }
        }

        //HashMap底层是无序的
        //Set是无序的，去重的
        Set<String> keys = wordToCount.keySet();

        //增强for循环，循环keys，每循环一次，取出来一个元素，赋值给局部变量key
        for(String key: keys) {
            System.out.println(key + " 的次数：" + wordToCount.get(key));
        }

        long endTime = System.currentTimeMillis();

        //耗时的时间
        System.out.println("耗时：" + (endTime - startTime));

    }
}

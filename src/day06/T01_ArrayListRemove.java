package day06;

import java.util.ArrayList;

/**
 * 演示 ArrayList的remove方法
 * <p>
 * 根据下标查找快，增加或删除慢（尤其不是在最后面的位置）
 */
public class T01_ArrayListRemove {

    public static void main(String[] args) {

        //new的时候，可以在构造方法可以指定该List的初始长度，如果不指定，默认是10
        ArrayList<String> strList = new ArrayList<String>(8);

        strList.add("A");
        strList.add("B");
        strList.add("C");
        strList.add("D");
        strList.add("E");

        for (int i = 0; i < strList.size(); i++) {
            System.out.println("当前List的下标：" + i + ", 当前下标的元素：" + strList.get(i));
        }

        //删除一个元素
        String ele = strList.remove(2); //根据下标删除
        System.out.println("删除的元素： " + ele);

        for (int i = 0; i < strList.size(); i++) {
            System.out.println("删除后的List的下标：" + i + ", 删除后List下标的元素：" + strList.get(i));
        }
    }
}

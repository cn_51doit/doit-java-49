package day06;

/**
 * 增强的for循环
 * 可以循环数组、List、Set、Map、Iterator
 */
public class T09_ForDemo1 {

    public static void main(String[] args) {
        
        String[] words = {"spark", "hive", "flink", "hadoop", "hive"};
        
        //for(变量类型 变量名称 : 要循环的数组或集合) {}
        for(String w : words) {
            System.out.println(w);
        }
        
        int[] nums = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        
        for(int n : nums) {
            System.out.println(n);
        }
        
    }
}

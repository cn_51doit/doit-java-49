package day06;

import java.util.ArrayList;
import java.util.Collections;

/**
 * 演示List的排序
 */
public class T04_ArrayListSort2 {

    public static void main(String[] args) {

        ArrayList<String> sList = new ArrayList<>();

        sList.add("spark");
        sList.add("100");
        sList.add("hadoop");
        sList.add("flink");
        sList.add("hive");
        sList.add("java");
        sList.add("20");
        sList.add("11");
        sList.add("1");
        sList.add("55");
        sList.add("scala");
        sList.add("hbase");
        sList.add("golang");
        sList.add("php");

        for (int i = 0; i < sList.size(); i++) {
            System.out.println("当前List下标为" + i  + " 对应的元素为：" + sList.get(i));
        }

        //对List进行排序
        Collections.sort(sList); //对当前List中的元素按照默认的规则(按照Integer的升序)进行排序

        //排序后，数据还在原来的List中，只是顺序发生了变化
        //TimSort(目前基于内存最快的排序算法)
        for (int i = 0; i < sList.size(); i++) {
            System.out.println("排序后List下标为" + i  + " 对应的元素为：" + sList.get(i));
        }

    }
}

public class HelloWorld {

    /**
     * main方法
     */
    //main方法
    //byte、short、int、long、double、float、boolean、String
    public static void main(String[] args) {
        System.out.println("hello world");


        double d1 = 3.5;
        double d2 = 4.1;
        double d3 =  d1 + d2;


        System.out.println(d3);

        //定义两个int类型的变量
        int a = 1;
        int b = 2;
        int c = a + b;
        System.out.println(c);

        String s1 = "1";
        String s2 = "2";
        System.out.println(s1 + s2);
    }
}

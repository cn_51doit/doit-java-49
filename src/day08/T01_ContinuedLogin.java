package day08;

import java.io.File;
import java.io.FileNotFoundException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * 统计用户连续登录3天及以上的有哪些用户，输出每个用户的详情信息
 */
public class T01_ContinuedLogin {

    public static void main(String[] args) throws FileNotFoundException, ParseException {

        //guid01 -> ArrayList("2018-03-01", "2018-02-28", "2018-03-04", "2018-03-02", "2018-03-05", "2018-03-06", "2018-03-07")
        //guid02 -> ArrayList("2018-03-01", "2018-03-02", "2018-03-03", "2018-03-06")
        HashMap<String, ArrayList<String>> idToDates = new HashMap<>();

        //guid01_2018-02-28 -> ArrayList("2018-02-28", "2018-03-21", "2018-03-02")
        //guid01_2018-03-01 -> ArrayList("2018-03-04", "2018-03-05", "2018-03-06", "2018-03-07")
        //guid02_2018-03-01 -> ArrayList("2018-03-01", "2018-03-02", "2018-03-03")
        HashMap<String, ArrayList<String>> uidDtDifToDates = new HashMap<>();


        File file = new File("/Users/star/Documents/dev/doit49/doit-java/data/access.txt");
        Scanner scanner = new Scanner(file);

        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            String[] fields = line.split(",");
            String uid = fields[0];
            String dt = fields[1];
            //根据用户ID取出该用户的登录日期，ArrayList<String>有可能为空
            ArrayList<String> dates = idToDates.get(uid);
            if (dates == null) {
                dates = new ArrayList<>();
                //将该用户id的List放到idToDates的map中
                idToDates.put(uid, dates);
            }
            //如果改日期不再对应的List中，就添加进去
            if (!dates.contains(dt)) {
                dates.add(dt);
            }
        }

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar calendar = Calendar.getInstance();

        //将idToDates中的数据取出来，进行测试
        for (Map.Entry<String, ArrayList<String>> entry : idToDates.entrySet()) {
            String uid = entry.getKey();
            ArrayList<String> dates = entry.getValue();
            //将dates中的日期进行排序
            Collections.sort(dates);
            //System.out.println(uid + " -> " + dates);
            for (int index = 0; index < dates.size(); index++) {
                String dt = dates.get(index); //"2018-02-28"
                //System.out.println(uid + "," + dt + "," + (index+1));
                //将字符串格式转成日期格式
                Date date = sdf.parse(dt);
                //将当前的日期设置到calender中;
                calendar.setTime(date);
                //在当前的时间基础上，减去行号
                calendar.add(Calendar.DATE, -index);
                //获取相减后的日期差值
                Date dateDif = calendar.getTime();
                //将dateDif转成字符串
                String dtDifStr = sdf.format(dateDif);
                //System.out.println(uid + "," + dt + "," + index + "," + dtDifStr);
                String uidAndDtDif = uid + "_" + dtDifStr;
                ArrayList<String> loginDates = uidDtDifToDates.get(uidAndDtDif);
                if (loginDates == null) {
                    loginDates = new ArrayList<>();
                    //放到uidDtDifToDates map中
                    uidDtDifToDates.put(uidAndDtDif, loginDates);
                }
                loginDates.add(dt);
            }
        }

//        //对第二个map进行遍历
        for (Map.Entry<String, ArrayList<String>> entry : uidDtDifToDates.entrySet()) {
            String uidAndDtDif = entry.getKey();
            ArrayList<String> dates = entry.getValue();
            //System.out.println(uidAndDtDif + " -> " + dates);
            //用户ID
            String uid = uidAndDtDif.split("_")[0];
            //dates的size就是连续登录的天数
            int counts = dates.size();
            //连续登录的起始日期
            String startDt = dates.get(0);
            //连续登录的结束日期
            String endDt = dates.get(dates.size() - 1);

            if (counts >= 3) {
                System.out.println(uid + "," + counts + "," + startDt + "," + endDt);
            }
        }


        scanner.close();

    }
}

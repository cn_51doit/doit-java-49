package day10;

public class People {

    //成员变量
    private String name; //姓名
    private double height; //身高
    private int age; //年龄

    public void setHeight(double h) {
        height = h; //将传入的h对应的值，赋值给成员变量height
    }

    private void printName() {
        System.out.println("我的名字叫：" + name);
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
       this.age = age; //this关键字，代表当前对象的
    }

    public static void main(String[] args) {

        People p1 = new People();
        p1.name = "赵昆";
        p1.height = 182;
        p1.age = 28;

        System.out.println(p1 + " , name: " + p1.name + ", age: " + p1.age + ", height: " + p1.height);

        p1.printName();

        String n = p1.name;
        System.out.println(n);


    }


}

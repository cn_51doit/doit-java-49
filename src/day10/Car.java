package day10;

public class Car {

    String name = "su7"; //成员变量，在该类中可以使用

    public void drive() {
        int speed; //局部变量，仅在该方法中可以使用
        //System.out.println("当前车速：" + speed);
        System.out.println("6666");
    }

    public void stop() {
        //System.out.println(speed); //报错，取不到
        System.out.println(name);
    }

    public static void main(String[] args) {

        Car c1 = new Car();
        c1.drive();

    }
}

package day10;

//window idea 生成getter和setter方法的快捷键 alt + insert

/**
 * 如果给一个类添加的有参的构造方法，最好在添加一个空参的构造方法
 */
public class Person {

    private String name;

    private int age = 10;

    private double fv;

    private boolean married; //婚否

    //空参构造方法
    public Person() {
        System.out.println("无参构造方法被调用了");
    }

    //有参的构造方法（构造器）
    public Person(String name, int age) {
        this.name = name; //给name赋值
        this.age = age;
    }


    //如果一个构造方法在内部调用了其他构造方法，一定要在第一行调用
    public Person(String name, int age, double fv) {
        this(name, age);
        this.fv = fv;
    }

    public Person(String name, int age, double fv, boolean married) {
        this.name = name;
        this.age = age;
        this.fv = fv;
        this.married = married;
    }

    public String getName() {
        return name;
    }

    public double getFv() {
        return fv;
    }

    public int getAge() {
        return age;
    }

    public boolean isMarried() {
        return married; //boolean类型的getter方法，使用is开头
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setFv(double fv) {
        this.fv = fv;
    }

    public void setMarried(boolean married) {
        this.married = married; //set
    }


    public static void main(String[] args) {

        //构造方法，就是在new对象时，直接传参，给该对象的成员变量赋值
//        Person p1 = new Person();
//        p1.age = 18;
//        p1.setName("tom");
//        p1.setFv(99.99);
//        p1.setMarried(true);
//        System.out.println(p1);


        Person p2 = new Person("tom", 18);
        p2.setFv(99.99);
        p2.setMarried(true);
        System.out.println(p2.name + ", " + p2.age);


        Person p3 = new Person("kitty", 16, 999.99, false);
        System.out.println(p3.name);

    }
}

package day10;


public class Student {

    //成员变量
    String name; //姓名
    int age; //年龄
    Double height; //身高

    //成员方法
    //学习的方法
    public void study() {
        System.out.println("好好学习，天天向上");
    }

    //吃饭的方法
    public void eat() {
        System.out.println("学习饿了要吃饭");
    }

    //静态方法
    public static void main(String[] args) {

        Student s1 = new Student();
        System.out.println(s1);
        //调用成员方法
        s1.study();

        //给成员变量赋值
        s1.name = "tom";
        s1.age = 18;
        s1.height = 188.8;

        //取值
        String n = s1.name;
        System.out.println(n);


    }
}

package day10;

public class PeopleTest {

    public static void main(String[] args) {

        //私有的属性和方法，即使在同一个包中，但是不同类中，也无法访问
        People p2 = new People();
        //p2.name = "杨天"; //报错
        // p2.printName(); //报错
    }
}

package day10;

public class Phone {

    //成员变量
    String name;
    String color;
    int price;

    //成员方法
    public void sendMessage() {
        System.out.println("群发短信");
    }

    public static void main(String[] args) {

        Phone p = new Phone();
        p.name = "锤子";
        p.color = "棕色";
        p.price = 2999;

        p.sendMessage();

        Phone p2 = new Phone();
        p2.name = "苹果";
        p2.color = "金色";
        p2.price = 5888;

        p2.sendMessage();

        //再定义一个Phone类型的变量
//        Phone p3 = p;
//        p3.price = 2777;

        System.out.println("p的地址"+ p);
        System.out.println("p2的地址"+ p2);
        //System.out.println("p3的地址"+ p3);

        //Phone p2 = new Phone(); //报错，因为变量重复定义了

        p2 = new Phone();
        System.out.println("p2的地址"+ p2);
        System.out.println(p2.name);


    }

}

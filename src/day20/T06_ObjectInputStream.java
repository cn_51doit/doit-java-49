package day20;

import java.io.*;

public class T06_ObjectInputStream {

    public static void main(String[] args) throws Exception {

        File file = new File("/Users/star/Desktop/obj");

        FileInputStream fileInputStream = new FileInputStream(file);

        //读取指定文件中的二进制，将二进制转成对象
        ObjectInputStream ois = new ObjectInputStream(fileInputStream);

        Object obj = ois.readObject();

        Girl g = (Girl) obj;

        System.out.println(g);

        ois.close();
        fileInputStream.close();

    }
}

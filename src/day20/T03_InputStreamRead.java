package day20;

import java.io.*;

public class T03_InputStreamRead {

    public static void main(String[] args) throws Exception {

        FileInputStream fis = new FileInputStream("/Users/star/Desktop/test4");
        InputStreamReader in = new InputStreamReader(fis, "UTF-8");
        BufferedReader bufferedReader = new BufferedReader(in);

        String line = null;
        while ((line = bufferedReader.readLine()) != null) {
            System.out.println(line);
        }
        bufferedReader.close();
        in.close();
        fis.close();
    }
}

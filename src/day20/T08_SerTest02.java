package day20;


import java.io.*;
import java.util.ArrayList;

public class T08_SerTest02 {

    public static void main(String[] args) throws Exception {

        // 创建 学生对象
        Student student = new Student("老王", 18);
        Student student2 = new Student("老张", 19);
        Student student3 = new Student("老李", 20);

        ArrayList<Student> arrayList = new ArrayList<>();
        arrayList.add(student);
        arrayList.add(student2);
        arrayList.add(student3);


        File file = new File("/Users/star/Desktop/obj5");
        if (!file.exists()) {
            file.createNewFile();
        }
        FileOutputStream fos = new FileOutputStream(file);
        ObjectOutputStream oos = new ObjectOutputStream(fos);

        //将Java对象编码成二进制写入到文件中，这个过程就是序列化
        oos.writeObject(arrayList);
        oos.flush();
        oos.close();

        ////////////////////////////////////////
        //读取指定文件中的二进制，将二进制数据转成Java对象，这个过程叫做反序列化
        FileInputStream fis = new FileInputStream(file);
        ObjectInputStream ois = new ObjectInputStream(fis);

        Object obj = ois.readObject();

        ArrayList<Student> list = (ArrayList<Student>) obj;

        for (Student s : list) {
            System.out.println(s);
        }

        ois.close();
        fis.close();

    }
}

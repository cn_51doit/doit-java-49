package day20;

import java.io.*;

/**
 * 序列化测试，测试序列化和反序列后数据是否一致，对象的地址是否相同
 */
public class T07_SerTest {

    public static void main(String[] args) throws Exception {

        //判断文件是否存在，不存在就创建
        File file = new File("/Users/star/Desktop/obj3");
        if (!file.exists()) {
            file.createNewFile();
        }
        FileOutputStream fos = new FileOutputStream(file);
        //对象输出流
        ObjectOutputStream oos = new ObjectOutputStream(fos);

        //new 出来两个 Java对象
        Girl g1 = new Girl("lucy", 19, 99.9999);
        Girl g2 = new Girl("kitty", 20, 99.8888);
        //将java对象，以二进制的形式，写入到文件中
        oos.writeObject(g1);
        oos.writeObject(g2);

        oos.flush();
        oos.close();

        /////////////////////////////////////

        //从指定的文件中读取数据
        FileInputStream fileInputStream = new FileInputStream(file);
        //读取指定文件中的二进制，将二进制转成对象
        ObjectInputStream ois = new ObjectInputStream(fileInputStream);

        Object obj = null;

        try {
            while ((obj =ois.readObject()) != null) {
                Girl g = (Girl) obj;
                System.out.println(g);
            }
        } catch (EOFException e) {
            //
        } finally {
            ois.close();
            fileInputStream.close();
        }



    }
}

package day20;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class T02_BufferedWriter {

    public static void main(String[] args) throws IOException {

        //希望一个单词写一行
        String line = "spark hive flink hadoop hive";

        FileWriter fileWriter = new FileWriter("/Users/star/Desktop/test2");
        BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);

        String[] words = line.split(" ");

        for (String word : words) {
            bufferedWriter.write(word);
            bufferedWriter.newLine(); //开启新的行
        }

        bufferedWriter.close();
        fileWriter.close();

    }
}

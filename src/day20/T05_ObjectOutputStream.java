package day20;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

public class T05_ObjectOutputStream {

    public static void main(String[] args) throws Exception {

        File file = new File("/Users/star/Desktop/obj");
        if (!file.exists()) {
            file.createNewFile();
        }

        FileOutputStream fos = new FileOutputStream(file);

        //对象输出流
        ObjectOutputStream oos = new ObjectOutputStream(fos);

        //Java对象
        Girl g1 = new Girl("lucy", 18, 99.99);
        //将java对象，以二进制的形式，写入到文件中
        oos.writeObject(g1);

        oos.flush();
        oos.close();

    }
}

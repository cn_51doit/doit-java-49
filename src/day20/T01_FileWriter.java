package day20;

import java.io.FileWriter;
import java.io.IOException;

public class T01_FileWriter {

    public static void main(String[] args) {

        FileWriter fileWriter = null;
        try {
            fileWriter = new FileWriter("/Users/star/Desktop/test", true); //true未追加
            fileWriter.write("hello everyone\n");
            fileWriter.write("hello everybody\n");
        } catch (IOException e) {
            //throw new RuntimeException(e);
            //处理异常
            e.printStackTrace();
        } finally {
            try {
                fileWriter.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }




    }
}

package day20;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;

import java.io.File;
import java.io.IOException;
import java.util.Collection;

public class T11_FileUtilsDemo2 {

    public static void main(String[] args) throws IOException {

        //File srcfile = new File("data/access.txt");
        //File destFile = new File("/Users/star/Desktop/acc.txt");
        //FileUtils.copyFile(srcfile, destFile);

        //用来保存文件偏移量的文件
        //File offsetFile = new File("/Users/star/Desktop/offset.txt");
        //读取原来文件中的数值
        //String numStr = FileUtils.readFileToString(offsetFile, "UTF-8");
        //将数值取出来加100后再写回到原来的文件中
        //FileUtils.writeStringToFile(offsetFile, (Integer.parseInt(numStr) + 100) + "", "UTF-8");


        //递归查找指定目录下的txt文件
        File file = new File("/Users/star/Desktop/aaa");
        Collection<File> files = FileUtils.listFiles(file, new String[]{"txt"}, true);
        for (File f : files) {
            System.out.println(f.getName());
        }

    }
}

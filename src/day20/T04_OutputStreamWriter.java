package day20;

import java.io.*;
import java.nio.charset.StandardCharsets;

public class T04_OutputStreamWriter {

    public static void main(String[] args) throws Exception {

        File file = new File("/Users/star/Desktop/test5.txt");
        if (!file.exists()) {
            file.createNewFile();//创建一个新的文件
        }

        FileOutputStream fos = new FileOutputStream(file, true);
        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fos, "GBK");
        BufferedWriter bufferedWriter = new BufferedWriter(outputStreamWriter);

        String line = "多 易 教 育";

        String[] words = line.split(" ");
        for (String word : words) {
            bufferedWriter.write(word);
            bufferedWriter.newLine();
        }

        bufferedWriter.close();
        outputStreamWriter.close();
        fos.close();


    }
}

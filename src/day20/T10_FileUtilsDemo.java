package day20;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class T10_FileUtilsDemo {

    public static void main(String[] args) throws IOException {

        File file = new File("data/access.txt");

        //List<String> list = FileUtils.readLines(file, "UTF-8");

        LineIterator iterator = FileUtils.lineIterator(file);

        while (iterator.hasNext()) {

            String line = iterator.next();
            System.out.println(line);

        }







    }
}

package day20;

import java.io.Serializable;

/**
 * 在java中，如果要将对象写入到文件中，或将对象在网络传输，并且以后还想恢复给对象，必须实现Serializable（标记性的接口，不需要实现任何方法）
 */
public class Girl implements Serializable {

    private String name;
    private int age;
    private double fv;

    public Girl() {}

    public Girl(String name, int age, double fv) {
        this.name = name;
        this.age = age;
        this.fv = fv;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public double getFv() {
        return fv;
    }

    public void setFv(double fv) {
        this.fv = fv;
    }

    @Override
    public String toString() {
        return "Girl{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", fv=" + fv +
                '}';
    }
}

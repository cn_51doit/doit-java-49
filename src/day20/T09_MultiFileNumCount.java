package day20;


import java.io.*;
import java.util.Arrays;
import java.util.Comparator;
import java.util.PriorityQueue;

/**
 * 在多个文件中，实现计算最大值、最小值、求和、TopN
 */
public class T09_MultiFileNumCount {


    /**
     * @param file 传入一个要计算的文件对象
     * @return 返回一个ResultBean
     */
    public static ResultBean calculate(File file) throws IOException {

        PriorityQueue<Long> priorityQueue = new PriorityQueue<>(4);
        //可以一行一行读取数据的工具
        BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
        String line = null;
        boolean isFirst = true;
        long min = 0;
        long max = 0;
        long sum = 0;
        while ((line = bufferedReader.readLine()) != null) {
            long num = Long.parseLong(line);
            if (isFirst) {
                min = num;
                max = num;
                sum = num;
                isFirst = false;
            } else {
                min = Math.min(min, num);
                max = Math.max(max, num);
                sum += num;
            }
            priorityQueue.offer(num);
            //如果长度大于3移除最小的
            if (priorityQueue.size() > 3) {
                priorityQueue.poll(); //移除优先级最高的，由于是升序，就是将最小的移除掉了
            }
        }
        Long[] arr = priorityQueue.toArray(new Long[0]);
        return new ResultBean(file.getName(), max, min, sum, arr);
    }


    public static void main(String[] args) {

        //1.将指定目录下的文件都列出来
        File file = new File("data/math");
        //2.并指定过滤器条件
        File[] files = file.listFiles(new FileFilter() {
            @Override
            public boolean accept(File file) {
                return file.isFile() && file.length() > 0 && file.getName().endsWith(".txt");
            }
        });


        boolean isFirst = true;
        long min = 0;
        long max = 0;
        long sum = 0;

        PriorityQueue<Long> priorityQueue = new PriorityQueue<>();
        for (File f : files) {
            //System.out.println(f.getName());
            //调用一个方法：输入一个File对象，返回该文件中最大值、最小值、和、TopN
            try {
                ResultBean bean = calculate(f);
                if (isFirst) {
                    min = bean.min;
                    max = bean.max;
                    sum = bean.sum;
                    priorityQueue.addAll(Arrays.asList(bean.topN));
                    isFirst = false;
                } else {
                    min = Math.min(min, bean.min);
                    max = Math.max(max, bean.max);
                    sum += bean.sum;
                    for (int i = 0; i < bean.topN.length; i++) {
                        priorityQueue.offer(bean.topN[i]);
                        if (priorityQueue.size() > 3) {
                            priorityQueue.poll(); //移除最小的
                        }
                    }
                }
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }

        Long[] res = priorityQueue.toArray(new Long[0]);
        Arrays.sort(res, new Comparator<Long>(){
            @Override
            public int compare(Long o1, Long o2) {
                return Long.compare(o2, o1);
            }
        });
        System.out.println("最大值：" + max + " , 最小值：" + min + " , 和：" + sum + " ,topN: " + Arrays.toString(res));


    }


    private static class ResultBean {

        String name;
        long max;
        long min;
        long sum;
        private Long[] topN;

        public ResultBean() {
        }

        public ResultBean(String name, long max, long min, long sum, Long[] topN) {
            this.name = name;
            this.max = max;
            this.min = min;
            this.sum = sum;
            this.topN = topN;
        }

        @Override
        public String toString() {
            return "ResultBean{" +
                    "name='" + name + '\'' +
                    ", max=" + max +
                    ", min=" + min +
                    ", sum=" + sum +
                    ", topN=" + Arrays.toString(topN) +
                    '}';
        }
    }

}

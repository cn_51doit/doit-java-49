public class BreakDemo {



    public static void main(String[] args) {

        //没有查找到，返回-1
        int[] arr = {10, 20, 30, 40, 50, 60};

        //break跟循环结合起来使用

        for (int i = 0; i < 5; i++) {
            if (i == 2) {
                break; //终断， 跳出循环，for不再循环了
            }
            System.out.println(i);
        }



    }
}

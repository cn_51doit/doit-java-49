package day04;

public class T02_TestDeptInfo {

    public static void main(String[] args) {


        //创建一个对象（创建一个实例）
        DeptInfo d1 = new DeptInfo();
        //DeptInfo.AAA = 100;
        d1.sumSal = 30000; //总薪水
        d1.maxSal = 12000; //最大薪水
        d1.minSal = 10000; //最小薪水
        d1.amount = 3;     //部门人数

        System.out.println("d1这个对象：总薪水："+ d1.sumSal + " ,最大薪水：" + d1.maxSal + ", 最小薪水：" + d1.minSal + ", 部门人数：" + d1.amount);

        DeptInfo d2 = new DeptInfo();
        //DeptInfo.AAA = 666;
        d2.sumSal = 50000; //总薪水
        d2.maxSal = 30000; //最大薪水
        d2.minSal = 10000; //最小薪水
        d2.amount = 4;     //部门人数

        System.out.println("d2这个对象：总薪水："+ d2.sumSal + " ,最大薪水：" + d2.maxSal + ", 最小薪水：" + d2.minSal + ", 部门人数：" + d2.amount);



    }
}

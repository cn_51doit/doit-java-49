package day04;

public class T03_TestPerson {

    public static void main(String[] args) {

        Person p1 = new Person();
        p1.name = "涛哥";
        p1.age = 40;
        p1.height = 175;
        p1.fv = 99.99;



        Person p2 = new Person();
        p2.name = "行哥";
        p2.age = 35;
        p2.height = 178;
        p2.fv = 99.8;

        Person p3 = new Person();
        p3.name = "星哥";
        p3.age = 18;
        p3.height = 180;
        p3.fv = 9999.99;


        System.out.println(p1 + ", 姓名：" + p1.name + ", 颜值：" + p1.fv + "， 年龄：" + p1.age + " 公司总资产：" + Person.money);

        Person.money = 8888888;
        System.out.println(p2 + ", 姓名：" + p2.name + ", 颜值：" + p2.fv + "， 年龄：" + p2.age + " 公司总资产：" + Person.money);
        System.out.println(p3 + ", 姓名：" + p3.name + ", 颜值：" + p3.fv + "， 年龄：" + p3.age + " 公司总资产：" + Person.money);

        p3.fv = 8888.88;

        System.out.println(p1 + ", 姓名：" + p1.name + ", 颜值：" + p1.fv + "， 年龄：" + p1.age );
        System.out.println(p2 + ", 姓名：" + p2.name + ", 颜值：" + p2.fv + "， 年龄：" + p2.age );
        System.out.println(p3 + ", 姓名：" + p3.name + ", 颜值：" + p3.fv + "， 年龄：" + p3.age );


    }
}

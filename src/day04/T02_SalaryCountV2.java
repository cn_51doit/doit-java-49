package day04;

import day03.ArrayUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class T02_SalaryCountV2 {

    public static void main(String[] args) throws FileNotFoundException {
        //保存部门名称的数组
        String[] deptArr = new String[4];
        //保存部门信息的包装类
        DeptInfo[] infoArr = new DeptInfo[4];

        //读取数据
        Scanner scanner = new Scanner(new File("./data/emp.txt"));
        while (scanner.hasNextLine()) {
            //读取一行数据
            String line = scanner.nextLine();
            //对该行内容进行切分
            String[] fields = line.split(",");
            int year = Integer.parseInt(fields[2].substring(0, 4));
            if (year < 2024) {
                //获取部门名称
                String dept = fields[1];
                //将底薪和奖金转成double类型相加得到月薪
                double salary = Double.parseDouble(fields[3]) + Double.parseDouble(fields[4]);
                //判断该部门在该数组中是否存在
                int index = ArrayUtils.indexOf(deptArr, dept);
                //如果不存在，将该部分名称添加到deptArr中
                if (index == -1) {
                    index = ArrayUtils.add(deptArr, dept);
                    DeptInfo deptInfo = new DeptInfo(); //创建一个类的实例（new对象）
                    infoArr[index] = deptInfo; //将对象添加到infoArr的对应位置
                }
                //根据index从infoArr中取出对应部门的DeptInfo实例（以前new的对象）
                DeptInfo info = infoArr[index];
                //计算总薪水
                info.sumSal = info.sumSal + salary;
                //计算最大薪水
                info.maxSal = Math.max(info.maxSal, salary);
                //计算最小薪水
                info.minSal = Math.min(info.minSal != 0.0 ? info.minSal : salary, salary);
                //计算部门的人生
                info.amount += 1;

            }

        }

        for (int i = 0; i < deptArr.length; i++) {
            if (deptArr[i] != null) {
                System.out.println(deptArr[i] + "的总薪水：" + infoArr[i].sumSal + ", 最大薪水：" + infoArr[i].maxSal + ", 最小薪水：" + infoArr[i].minSal + ", 平均薪水：" + infoArr[i].sumSal / infoArr[i].amount);
            }
        }


        scanner.close();
    }
}

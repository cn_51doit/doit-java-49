package day04;

import day03.ArrayUtils;

public class T04_ArrayCopy {

    public static void main(String[] args) {

        String[] words = {"spark", "hive", "hadoop", "flink"};

        String[] newWords = new String[words.length * 2];

        for (int i = 0; i < words.length; i++) {
            //将原来数组中第i个位置的元素取出来
            String word = words[i];
            //赋值给新的数组的第i个位置
            newWords[i] = word;
        }

        for (int i = 0; i < newWords.length; i++) {
            System.out.println(newWords[i]);
        }

    }
}

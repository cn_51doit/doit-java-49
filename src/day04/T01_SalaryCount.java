package day04;

import day03.ArrayUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class T01_SalaryCount {

    public static void main(String[] args) throws FileNotFoundException {
        //保存部门名称的数组
        String[] deptArr = new String[4];
        //保存部门总薪水的数组
        double[] sumSalArr = new double[4];
        //保存部门最大薪水的数组
        double[] maxSalArr = new double[4];

        //读取数据
        Scanner scanner = new Scanner(new File("./data/emp.txt"));
        while (scanner.hasNextLine()) {
            //读取一行数据
            String line = scanner.nextLine();
            //对该行内容进行切分
            String[] fields = line.split(",");
            int year = Integer.parseInt(fields[2].substring(0, 4));
            if (year < 2024) {
                //获取部门名称
                String dept = fields[1];
                //将底薪和奖金转成double类型相加得到月薪
                double salary = Double.parseDouble(fields[3]) +  Double.parseDouble(fields[4]);
                //判断该部门在该数组中是否存在
                int index = ArrayUtils.indexOf(deptArr, dept);
                //如果不存在，将该部分名称添加到deptArr中
                if (index == -1) {
                    index = ArrayUtils.add(deptArr, dept);
                }
                //根据对应的下标，到第二个数组sumSalArr中查找对应的部门累加薪水
                double sumSal = sumSalArr[index];
                sumSalArr[index] = sumSal + salary;
                //sumSalArr[index] = sumSalArr[index] + salary;
                //比较部门的最大薪水
                double maxSal = maxSalArr[index];
                //maxSalArr[index]  = maxSal > salary ? maxSal : salary;
                maxSalArr[index] = Math.max(maxSal, salary);
            }

        }

        for (int i = 0; i < deptArr.length; i++) {
            if (deptArr[i] != null)
                System.out.println(deptArr[i] + "总薪水：" + sumSalArr[i] + ", 最大薪水：" + maxSalArr[i]);
        }


        scanner.close();
    }
}

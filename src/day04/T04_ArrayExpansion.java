package day04;


import day03.ArrayUtils;

public class T04_ArrayExpansion {

    public static void main(String[] args) {

        String[] words = new String[4];

        ArrayUtils.add(words, "spark");
        ArrayUtils.add(words, "hive");
        ArrayUtils.add(words, "spark");
        ArrayUtils.add(words, "flink");

        int index = ArrayUtils.add(words, "hadoop");

        if (index == -1) {
            String[] newArr = new String[words.length * 2];
            ArrayUtils.copy(words, newArr);
            words = newArr;
        }
        ArrayUtils.add(words, "hadoop");


    }
}

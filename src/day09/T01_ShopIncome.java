package day09;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

/**
 * 统计店铺的累计收入
 */
public class T01_ShopIncome {

    public static void main(String[] args) throws FileNotFoundException {

        //shop1_2019-01 -> 500
        HashMap<String, Double> sidMthToMoney = new HashMap<>();

        //shop1 -> ArrayList("2019-01,500", "2019-03,180", "2019-02,2000")
        HashMap<String, ArrayList<String>> sidToMthAndMoneyList = new HashMap<>();

        File file = new File("data/shop.txt");
        Scanner scanner = new Scanner(file);
        while (scanner.hasNextLine()) {
            //读取一行数据
            String line = scanner.nextLine();
            String[] fields = line.split(",");
            String sid = fields[0]; //店铺ID
            String mth = fields[1].substring(0, 7); //月份
            double money = Double.parseDouble(fields[2]); //金额
            //将店铺ID和月份拼接，作为key
            String key = sid + "_" + mth;
            Double accMoney = sidMthToMoney.getOrDefault(key, 0.0);
            accMoney += money; //累加
            //将累加后的数据在保存到Map中
            sidMthToMoney.put(key, accMoney);
        }

        //对第一个Map中的数据进行循环
        for (Map.Entry<String, Double> entry : sidMthToMoney.entrySet()) {
            String sidAndMth = entry.getKey(); //店铺ID_2019-01
            String sid = sidAndMth.split("_")[0];
            String mth = sidAndMth.split("_")[1];
            Double money = entry.getValue();
            ArrayList<String> list = sidToMthAndMoneyList.get(sid);
            if (list == null) { //该店铺ID的数据，第一次出现
                list = new ArrayList<>();
                //put到sidToMthAndMoneyList中
                sidToMthAndMoneyList.put(sid, list);
            }
            list.add(mth + "_" + money);
        }

        for (Map.Entry<String, ArrayList<String>> entry : sidToMthAndMoneyList.entrySet()) {
            String sid = entry.getKey();
            ArrayList<String> list = entry.getValue();
            //将list中的数据按照日期进行排序
            Collections.sort(list);
            double accMoney = 0.0;
            for (String mthAndMoney : list) {
                String[] fields = mthAndMoney.split("_");
                String mth = fields[0]; //月份
                double money = Double.parseDouble(fields[1]);
                accMoney += money;
                //sid,yyyy-MM,money,accMoney
                System.out.println(sid + "," + mth + "," + money + "," + accMoney);
            }
        }


        scanner.close();

    }
}

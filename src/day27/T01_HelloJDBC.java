package day27;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

public class T01_HelloJDBC {

    public static void main(String[] args) throws Exception {

        //1.注册驱动
        //Class.forName("com.mysql.jdbc.Driver");
        //Class.forName("com.mysql.cj.jdbc.Driver");


        //2.调用DriverManager的getConnection方法，传入mysql的链接URL创建链接对象
        Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/doit49?characterEncoding=utf-8", "root", "123456");

        //3.创建Statement //Statement存在SQL注入的风险
        Statement statement = connection.createStatement();
        //4.执行SQL
        //update tb_category2 set cname = '未知' where id > 1 -- 将参数直接写到SQL中，存在风险
        boolean flag = statement.execute("insert into tb_category2 values (null, '图书')");

        //5.获取返回值
        System.out.println("返回的结果：" + flag);

        //6.释放资源
        statement.close();
        connection.close();



    }
}

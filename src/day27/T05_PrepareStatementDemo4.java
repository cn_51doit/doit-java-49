package day27;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

public class T05_PrepareStatementDemo4 {

    public static void main(String[] args) throws Exception {

        //1.注册驱动
        //Class.forName("com.mysql.jdbc.Driver");
        //Class.forName("com.mysql.cj.jdbc.Driver");

        //2.调用DriverManager的getConnection方法，传入mysql的链接URL创建链接对象
        Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/doit49?characterEncoding=utf-8", "root", "123456");

        //3.创建Statement
        String sql = "delete from emp where sal > ?";
        PreparedStatement statement = connection.prepareStatement(sql);
        //设置参数
        statement.setDouble(1, 5000);

        //4.执行SQL
        int r = statement.executeUpdate();

        System.out.println("受影响的行数：" + r);

        //5.释放资源
        statement.close();
        connection.close();
    }
}

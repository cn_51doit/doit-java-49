package day27;

import java.sql.*;

public class T02_PrepareStatementDemo {

    public static void main(String[] args) throws Exception {

        //1.注册驱动
        //Class.forName("com.mysql.jdbc.Driver");
        //Class.forName("com.mysql.cj.jdbc.Driver");


        //2.调用DriverManager的getConnection方法，传入mysql的链接URL创建链接对象
        Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/doit49?characterEncoding=utf-8", "root", "123456");

        //3.创建Statement
        //对SQL进行预检查的Statement，避免SQL注入的风险
        String sql = "select empno, ename, sal from emp where sal > ?";
        PreparedStatement statement = connection.prepareStatement(sql);
        //设置参数
        statement.setDouble(1, 2500.0); //PreparedStatement参数的下标是从1开始的

        //4.执行SQL，获取返回值
        ResultSet resultSet = statement.executeQuery();

        //5.对返回的结果进行处理
        //next方法，相当于迭代器的hasNext方法
        while (resultSet.next()) {
            int no = resultSet.getInt(1); //列的下标从1开始
            String name = resultSet.getString(2);
            double sal = resultSet.getDouble("sal");
            System.out.println("no: " + no + ", name:" + name + ", sal:" + sal);
        }

        //6.释放资源
        resultSet.close();
        statement.close();
        connection.close();

    }
}

package day27;

import java.io.File;
import java.io.FileNotFoundException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Scanner;

/**
 * 如果数据非常多，一条一条的插入，效率低，最好使用批量插入
 */
public class T09_TransactionalDemo {

    public static void main(String[] args) throws FileNotFoundException, SQLException {




        //2.调用DriverManager的getConnection方法，传入mysql的链接URL创建链接对象


        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/doit49?characterEncoding=utf-8", "root", "123456");
            //开启事务
            connection.setAutoCommit(false);
            //3.创建Statement
            //对SQL进行预检查的Statement，避免SQL注入的风险
            String sql = "insert into tb_orders (sid, dt, money) values (?, ?, ?)";
            statement = connection.prepareStatement(sql);

            statement.setString(1, "shop3");
            statement.setString(2, "2024-05-07");
            statement.setDouble(3, 77.77);

            statement.executeUpdate();

            //人为制造异常
            //int i = 1 / 0;

            statement.setString(1, "shop4");
            statement.setString(2, "2024-05-07");
            statement.setDouble(3, 88.88);

            statement.executeUpdate();

            //提交事务
            connection.commit();


        } catch (Exception e) {
            //throw new RuntimeException(e);
            connection.rollback();
        } finally {
            if (statement != null) {
                statement.close();
            }
            if (connection != null) {
                connection.close();
            }
        }


    }
}

package day27;

import java.sql.*;

public class T03_PrepareStatementDemo2 {

    public static void main(String[] args) throws Exception {

        //1.注册驱动
        //Class.forName("com.mysql.jdbc.Driver");
        //Class.forName("com.mysql.cj.jdbc.Driver");

        //2.调用DriverManager的getConnection方法，传入mysql的链接URL创建链接对象
        Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/doit49?characterEncoding=utf-8", "root", "123456");

        //3.创建Statement
        //对SQL进行预检查的Statement，避免SQL注入的风险
        String sql = "insert into emp (empno, ename, job, mgr, hiredate, sal, deptno) values (?, ?, ?, ?, ?, ?, ?)";
        PreparedStatement statement = connection.prepareStatement(sql);
        //设置参数
        statement.setInt(1, 8888);
        statement.setString(2, "tom");
        statement.setString(3, "manager");
        statement.setInt(4, 7839);
        statement.setDate(5, new Date(System.currentTimeMillis()));
        statement.setDouble(6, 4999.0);
        statement.setInt(7, 10);

        //4.执行SQL
        int r = statement.executeUpdate();

        System.out.println("受影响的行数：" + r);

        //5.释放资源
        statement.close();
        connection.close();
    }
}

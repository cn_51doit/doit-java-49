package day27;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

/**
 * Upsert 没有就插入，有就更新
 * 要操作的表必须有主键
 */
public class T06_UpsertDemo {

    public static void main(String[] args) throws Exception {

        //1.注册驱动
        //Class.forName("com.mysql.jdbc.Driver");
        //Class.forName("com.mysql.cj.jdbc.Driver");

        //2.调用DriverManager的getConnection方法，传入mysql的链接URL创建链接对象
        Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/doit49?characterEncoding=utf-8", "root", "123456");

        //3.创建Statement
        String sql = "insert into tb_wordcount values (?, ?) on duplicate key update counts = ?";
        PreparedStatement statement = connection.prepareStatement(sql);
        //设置参数
        statement.setString(1, "hive");
        statement.setLong(2, 8);
        statement.setLong(3, 80);

        //4.执行SQL
        int r = statement.executeUpdate();

        System.out.println("受影响的行数：" + r);

        //5.释放资源
        statement.close();
        connection.close();
    }
}

package day27;

import java.io.File;
import java.io.FileNotFoundException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Scanner;

/**
 * 如果数据非常多，一条一条的插入，效率低，最好使用批量插入
 */
public class T08_BatchInsertV2 {

    public static void main(String[] args) throws FileNotFoundException, SQLException {

        Scanner scanner = new Scanner(new File("data/shop.txt"));


        //2.调用DriverManager的getConnection方法，传入mysql的链接URL创建链接对象


        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/doit49?characterEncoding=utf-8", "root", "123456");
            connection.setAutoCommit(false);
            //3.创建Statement
            //对SQL进行预检查的Statement，避免SQL注入的风险
            String sql = "insert into tb_orders (sid, dt, money) values (?, ?, ?)";
            statement = connection.prepareStatement(sql);

            int count = 0;
            while (scanner.hasNextLine()) {
                count++;
                String line = scanner.nextLine();
                String[] fields = line.split(",");
                String sid = fields[0];
                String dt = fields[1];
                double money = Double.parseDouble(fields[2]);
                statement.setString(1, sid);
                statement.setString(2, dt);
                statement.setDouble(3, money);
                statement.addBatch(); //将要更新的数据在客户端先攒起来
                if (count % 10 == 0) {
                    statement.executeBatch(); //批量执行
                }
            }
            if (count % 10 != 0) {
                statement.executeBatch(); //批量执行
            }

            //提交事务
            connection.commit();


        } catch (SQLException e) {
            //throw new RuntimeException(e);
            connection.rollback();
        } finally {
            if (statement != null) {
                statement.close();
            }
            if (connection != null) {
                connection.close();
            }
        }


    }
}

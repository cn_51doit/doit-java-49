package day13;

public class FlowBean {

    private String uid;

    private String startTime;

    private String endTime;

    private double flow;

    public FlowBean(){}

    public FlowBean(String uid, String startTime, String endTime, double flow) {
        this.uid = uid;
        this.startTime = startTime;
        this.endTime = endTime;
        this.flow = flow;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public double getFlow() {
        return flow;
    }

    public void setFlow(double flow) {
        this.flow = flow;
    }
}

package day13;

import java.util.HashSet;

/**
 * HashSet的特点：无序、不能重复
 */
public class T08_HashSetDemo {

    public static void main(String[] args) {

        HashSet<String> words = new HashSet<>();
        words.add("spark"); //添加
        words.add("hive");
        words.add("hadoop");
        words.add("flink");
        System.out.println(words.size()); // 4
        words.add("spark");
        System.out.println(words.size()); // 4
        words.add("hbase");
        //遍历数据
        //for(循环出来变量的类型 变量名称 : 数组或集合)
        for (String word : words) {
            System.out.println(word);
        }


    }
}

package day13;

import java.util.TreeSet;

/**
 * TreeSet :
 * ①唯一（去重）
 * ②看按照指定的规则排序，数据放进去自动排序
 */
public class T10_TreeSet {

    public static void main(String[] args) {

        //TreeSet中保存的是String，默认的排序规则就是String的比较规则
        TreeSet<String> wordSet = new TreeSet<>();

        wordSet.add("spark");
        wordSet.add("flink");
        wordSet.add("hive");
        wordSet.add("hbase");
        wordSet.add("spark");

        for (String word : wordSet) {
            System.out.println(word);
        }

    }
}

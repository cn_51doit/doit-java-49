package day13;

import java.util.HashSet;

/**
 * HashSet的特点：无序、不能重复
 */
public class T09_HashSetDemo2 {

    public static void main(String[] args) {

        HashSet<String> words = new HashSet<>();
        words.add("spark");
        words.add("hive");
        words.add("hadoop");
        words.add("flink");
        //移除hive这个单词
        //boolean flag = words.remove("flume"); //该元素如果在Set中，被成功一次返回true
        //System.out.println(flag); //
        String ele = "hive";
        if (words.contains(ele)) { //如果改元素在该Set中，那么就移除
            words.remove(ele);
        }


        for (String word : words) {
            System.out.println(word);
        }

    }
}

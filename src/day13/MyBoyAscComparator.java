package day13;

import java.util.Comparator;

public class MyBoyAscComparator implements Comparator<BoyV4> {

    //按照颜值的升序
    @Override
    public int compare(BoyV4 o1, BoyV4 o2) {
        return Double.compare(o1.getFv(), o2.getFv());
    }
}

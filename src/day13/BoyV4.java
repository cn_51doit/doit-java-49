package day13;

/**
 * 自定义类型实现比较的两种方法
 * 第一种：让自定义的类实现Comparable接口
 *
 * 排序规则，按照颜值的降序
 */
public class BoyV4{

    private String name;
    private int age;
    private double fv;

    public BoyV4() {}

    public BoyV4(String name, int age, double fv) {
        this.name = name;
        this.age = age;
        this.fv = fv;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public double getFv() {
        return fv;
    }

    public void setFv(double fv) {
        this.fv = fv;
    }

    @Override
    public String toString() {
        return
                "name='" + name + '\'' +
                ", age=" + age +
                ", fv=" + fv;
    }
}

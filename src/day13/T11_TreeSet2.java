package day13;

import java.util.Comparator;
import java.util.TreeSet;

/**
 * TreeSet :
 * ①唯一（去重）
 * ②看按照指定的规则排序，数据放进去自动排序
 */
public class T11_TreeSet2 {

    public static void main(String[] args) {

        //TreeSet中保存的是String，默认的排序规则就是String的比较规则
        TreeSet<String> wordSet = new TreeSet<>(new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                //return -o1.compareTo(o2); //降序
                return o2.compareTo(o1); //降序
            }
        });

        wordSet.add("spark");
        wordSet.add("flink");
        wordSet.add("hive");
        wordSet.add("hbase");
        wordSet.add("spark");
        wordSet.add("hadoop");
        wordSet.add("hue");
        wordSet.add("sqoop");

        for (String word : wordSet) {
            System.out.println(word);
        }

    }
}

package day13;

import day12.BoyV3;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Scanner;

public class T03_SortDemo6 {

    private static ArrayList<BoyV3> boyList;

    public static void main(String[] args) throws FileNotFoundException {


        ArrayList<BoyV4> boyList = new ArrayList<>();

        File file = new File("data/boy.txt");
        Scanner scanner = new Scanner(file);

        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            //将读出来的数据进行切分
            String[] fields = line.split(",");
            //将字段转成相应的类型，通过构造方法传参
            BoyV4 boy = new BoyV4(fields[0], Integer.parseInt(fields[1]), Double.parseDouble(fields[2]));
            //将封装好的boy，添加到ArrayList中
            boyList.add(boy);
        }

        //排序，按照ArrayList中实际保存的类型，进行排序
        //自定义的比较规则
        //Comparator comparator = new Comparator<BoyV4>(); //报错，接口中有未实现的方法
        //new 接口，并且重写了方法，就相当于new了一个接口的实现类
//        Comparator<BoyV4> comparator = new Comparator<BoyV4>() {
//            @Override
//            public int compare(BoyV4 o1, BoyV4 o2) {
//                //按照年龄的升序
//                return o1.getAge() - o2.getAge();
//            }
//        };
        Collections.sort(boyList, new Comparator<BoyV4>() {
            @Override
            public int compare(BoyV4 o1, BoyV4 o2) {
                //按照年龄的升序
                return o1.getAge() - o2.getAge();
            }
        });

        for (int index = 0; index < boyList.size(); index++) {
            BoyV4 boy = boyList.get(index);
            System.out.println("排名：" + (index + 1) + " " + boy);
            //System.out.println("排名：" + (index + 1) + ", 姓名：" + boy.getName() + ", 颜值：" + boy.getFv() + ", 年龄：" + boy.getAge());
        }


    }
}

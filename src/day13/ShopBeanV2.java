package day13;

public class ShopBeanV2 {

    private String mth;
    private double money;

    public ShopBeanV2(){}

    public ShopBeanV2(String mth, double money) {
        this.mth = mth;
        this.money = money;
    }

    public String getMth() {
        return mth;
    }

    public void setMth(String mth) {
        this.mth = mth;
    }

    public double getMoney() {
        return money;
    }

    public void setMoney(double money) {
        this.money = money;
    }
}

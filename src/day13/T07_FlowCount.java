package day13;

import java.io.File;
import java.io.FileNotFoundException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class T07_FlowCount {

    public static void main(String[] args) throws FileNotFoundException, ParseException {


        HashMap<String, ArrayList<FlowBean>> idToFlowBeanList = new HashMap<>();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        HashMap<String, ArrayList<FlowBean>> idAndSumFlagToBeanList = new HashMap<>();

        File file = new File("data/data.txt");
        Scanner scanner = new Scanner(file);
        //判断文件中是否有未读取的数据
        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            //切分数据
            String[] fields = line.split(",");
            String uid = fields[0];
            String startTime = fields[1];
            String endTime = fields[2];
            double flow = Double.parseDouble(fields[3]);
            //将数据封装到FlowBean中
            FlowBean flowBean = new FlowBean(uid, startTime, endTime, flow);
            ArrayList<FlowBean> beanList = idToFlowBeanList.get(uid);
            if (beanList == null) {
                beanList = new ArrayList<>();
                //将新的List添加到Map中
                idToFlowBeanList.put(uid, beanList);
            }
            beanList.add(flowBean);
            //排序有哪些问题？
        }

        //遍历每个人的数据，每个人对应一个ArrayList，但是没有按照时间排序
        for (Map.Entry<String, ArrayList<FlowBean>> entry : idToFlowBeanList.entrySet()) {
            String uid = entry.getKey(); //用户ID
            ArrayList<FlowBean> beanList = entry.getValue(); //用一个用户的多个数据
            //按照起始时间进行排序
            beanList.sort(new Comparator<FlowBean>() {
                @Override
                public int compare(FlowBean o1, FlowBean o2) {
                    return o1.getStartTime().compareTo(o2.getStartTime());
                }
            });

            //将数据进行遍历，用下一行的起始时间，减去上一行的结束数据
            int flag = 0;
            int sum_flag = 0;
            for (int index = 0; index < beanList.size(); index++) {
                FlowBean currentBean = beanList.get(index);
                if (index != 0) { //不是第一行
                    //将当前行的数据取出来
                    //取出上一行的bean
                    FlowBean preBean = beanList.get(index - 1);
                    //取出当前行起始时间
                    long start = simpleDateFormat.parse(currentBean.getStartTime()).getTime();
                    //取出上一行的结束时间
                    long end = simpleDateFormat.parse(preBean.getEndTime()).getTime();
                    if (start - end > 1000 * 60 * 10) {
                        flag = 1; // 相邻两条数据大于10分钟
                    } else {
                        flag = 0; // 相邻两条数据没有大于10分钟
                    }

                }
                sum_flag += flag;
                //测试，打印每1条数据
                //System.out.println(uid + "," + currentBean.getStartTime() + "," + currentBean.getEndTime() + "," + flag + "," + sum_flag);
                String uidAndSumFlag = uid + "_" + sum_flag;
                //根据uidAndSumFlag到新的idAndSumFlagToBeanList取查找
                ArrayList<FlowBean> beans = idAndSumFlagToBeanList.get(uidAndSumFlag);
                if (beans == null) {
                    beans = new ArrayList<>();
                    idAndSumFlagToBeanList.put(uidAndSumFlag, beans);
                }
                beans.add(currentBean);
            }
        }

        //遍历第二个HashMap
        for (Map.Entry<String, ArrayList<FlowBean>> entry : idAndSumFlagToBeanList.entrySet()) {
            ArrayList<FlowBean> beanList = entry.getValue();
            //获取第一行的起始时间
            String start = beanList.get(0).getStartTime();
            //获取做好一个的结束时间
            String end = beanList.get(beanList.size() - 1).getEndTime();
            double sumFlow = 0.0;
            for (FlowBean flowBean : beanList) {
                sumFlow += flowBean.getFlow();
            }
            //输出结果
            System.out.println(beanList.get(0).getUid() + "," + start + "," + end + "," + sumFlow);
        }
        scanner.close();
    }
}

package day13;

public class ShopBean implements Comparable<ShopBean>{

    private String mth;
    private double money;

    public ShopBean(){}

    public ShopBean(String mth, double money) {
        this.mth = mth;
        this.money = money;
    }

    @Override
    public int compareTo(ShopBean o) {
        return this.mth.compareTo(o.mth); //按照字符串的字典顺序比较
    }

    public String getMth() {
        return mth;
    }

    public void setMth(String mth) {
        this.mth = mth;
    }

    public double getMoney() {
        return money;
    }

    public void setMoney(double money) {
        this.money = money;
    }
}

package day13;

import day12.BoyV3;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class T012_TreeSet3 {

    private static ArrayList<BoyV3> boyList;

    public static void main(String[] args) throws FileNotFoundException {


        //按照年龄的升序排序
        TreeSet<BoyV4> boySet = new TreeSet<>(new Comparator<BoyV4>() {
            @Override
            public int compare(BoyV4 o1, BoyV4 o2) {
                return o1.getAge() - o2.getAge();
            }
        });

        File file = new File("data/boy.txt");
        Scanner scanner = new Scanner(file);

        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            //将读出来的数据进行切分
            String[] fields = line.split(",");
            //将字段转成相应的类型，通过构造方法传参
            BoyV4 boy = new BoyV4(fields[0], Integer.parseInt(fields[1]), Double.parseDouble(fields[2]));
            //将封装好的boy，添加到ArrayList中
            boySet.add(boy);
        }

        //排序，按照ArrayList中实际保存的类型，进行排序
        //自定义的比较规则
        //Collections.sort(boyList, new MyBoyDescComparator());

        boySet.remove(boySet.last()); //删除当前set中的最后一个

        int rank = 1;
        for (BoyV4 boy : boySet) {
            System.out.println("排名：" + rank + ", 姓名：" + boy.getName() + ", 颜值：" + boy.getFv() + ", 年龄：" + boy.getAge());
            rank++;
        }


    }
}

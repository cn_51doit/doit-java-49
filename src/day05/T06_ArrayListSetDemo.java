package day05;

import java.util.ArrayList;

public class T06_ArrayListSetDemo {

    public static void main(String[] args) {

        ArrayList<Integer> numList = new ArrayList<>();
        numList.add(1);
        numList.add(2);
        numList.add(3); //下标：2
        numList.add(4);
        numList.add(5);
        numList.add(6);

        Integer oldVal = numList.set(2, 3000);
        System.out.println("对应位置的老的值：" + oldVal);

        for (int i = 0; i < numList.size(); i++) {
            Integer res = numList.get(i);
            System.out.println(res);
        }

    }
}

package day05;

import java.util.ArrayList;

public class T03_ArrayListDemo3 {

    public static void main(String[] args) {

        ArrayList<Double> numList = new ArrayList<>();

        numList.add(0, 1.1);
        numList.add(1, 2.2);
        numList.add(2, 3.3);
        numList.add(3, 4.4);
        numList.add(4, 5.5);

        //numList.add(8, 9.9.); //报错

        System.out.println(numList.size());
        //取出3.3
        System.out.println(numList.get(2));

        //在指定的文件插入元素，如果改位置有元素，后面的元素会向后移动一个位置
        numList.add(2, 3333.333);

        System.out.println(numList.get(2));
        System.out.println(numList.size());

        for (int i = 0; i < numList.size(); i++) {
            System.out.println(numList.get(i));
        }

    }
}

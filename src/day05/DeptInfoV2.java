package day05;

public class DeptInfoV2 {

    //部门名称
    public String name;

    //总薪水
    public double sumSal;

    //最大薪水
    public double maxSal;

    //最小薪水
    public double minSal;

    //部门人数
    public int amount;
}

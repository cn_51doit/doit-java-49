package day05;

import java.util.ArrayList;

/**
 * ArrayList是一个有序的变长的集合，底层是使用数组来实现的
 * ArrayList是一个类，封装了很多方法，方便用户使用：add、get、indexOf等等
 * 定义ArrayList可以通过泛型【对类型的约束】指定要存储的数据类型
 */
public class T01_ArrayListDemo {

    public static void main(String[] args) {

        //定义一个ArrayList，指定存储是的类型为String
        ArrayList<String> list = new ArrayList<String>();

        //添加元素
        list.add("spark");
        list.add("spark");
        list.add("spark");
        System.out.println("当前ArrayList的长度：" + list.size());
        list.add("hive");
        list.add("hadoop");
        list.add("hive");
        list.add("flink");
        list.add("hue");
        System.out.println("最后ArrayList的长度：" + list.size());


        //indexOf从头开始，返回该元素第一次出现的下标
        int index = list.indexOf("hive");

        System.out.println("hive的下标：" + index);

        //lastIndexOf从后面开始，返回该元素第一次出现的下标
        int lastIndex = list.lastIndexOf("hive");
        System.out.println("lastIndexOf hive的下标:" + lastIndex);

        //判断该元素在ArrayList中是否存在,存在返回true，不存在返回false
        boolean flag = list.contains("flink");
        System.out.println(flag);




    }
}

package day05;

import java.util.ArrayList;

public class T04_ArrayListDemo4 {

    public static void main(String[] args) {

        //创建一个用来保存User类型的List集合
        ArrayList<User> userList = new ArrayList<>();

        User u1 = new User();
        u1.name = "tom";
        u1.age = 18;
        u1.fv = 99.99;

        userList.add(u1); //下标为：0

        User u2 = new User();
        u2.name = "jerry";
        u2.age = 19;
        u2.fv = 88.88;

        userList.add(u2); //下标为：1

        User u3 = new User();
        u3.name = "kitty";
        u3.age = 20;
        u3.fv = 99.88;

        userList.add(u3); //下标为：2

        //取出指定下标的User
        User user = userList.get(1); //取出下标为1的用户
        System.out.println("当前用户的名称：" + user.name + ", 年龄：" + user.age + ", 颜值：" + user.fv);


        System.out.println("user的地址：" + user);
        System.out.println("u2的地址：" + u2);

    }
}

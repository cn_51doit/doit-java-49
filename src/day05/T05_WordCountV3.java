package day05;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class T05_WordCountV3 {

    public static void main(String[] args) throws FileNotFoundException {

        long startTime = System.currentTimeMillis(); //获取当前的时间
        System.out.println(startTime);
        //int lineCount = 0;
        //int wordCount = 0;
        //保存单词的集合
        ArrayList<String> wordList = new ArrayList<>();
        //保存对应单词的次数的集合
        ArrayList<Integer> countList = new ArrayList<>();

        //创建一个文件对象
        File file = new File("data/words-big.txt");
        //创建读完文件的工具类
        Scanner scanner = new Scanner(file);
        //开始一行一行的读取数据
        while (scanner.hasNextLine()) { //判断文件中是否有未读取的数据，如果返回true，继续读取
            //lineCount++; //统计文件中的行数
            String line = scanner.nextLine(); //读出来一行内容，使用字符串接收
            String[] words = line.split(" "); //将一行内容对应的字符串进行切分，返回多个单词，放到数组中
            for(int i = 0; i < words.length; i++) {
                String word = words[i]; //取出一个单词
                if (!"error".equals(word)) {
                    //判断该单词是否在wordList中存在
                    int index = wordList.indexOf(word);
                    if (index == -1) { //该单词不存在
                        //将该单词添加到wordList中
                        index = wordList.size();
                        wordList.add(word);
                        //或者后面的写法 //index = wordList.indexOf(word);
                        countList.add(index, 0); //如果该单词第一次出现，要将第二个List中对应的位置设置为0
                    }
                    //根据该单词对应的index到第二个ArrayList中，即countList查找该单词对应的次数
                    Integer count = countList.get(index);
                    //count++;
                    //countList
                    countList.set(index, ++count); //给对我的位置设置上累加后的值
                    //wordCount++;
                }
            }

        }

        //System.out.println("该文件中行数：" + lineCount + ", 单词的总类：" + wordList.size());
        for (int i = 0; i < wordList.size(); i++) {
            System.out.println(wordList.get(i) + " 次数：" + countList.get(i));
        }

        long endTime = System.currentTimeMillis();

        //耗时的时间
        System.out.println("耗时：" + (endTime - startTime));

    }
}

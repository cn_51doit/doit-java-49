package day05;

import day04.DeptInfo;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class T08_SalaryCountV3 {

    public static void main(String[] args) throws FileNotFoundException {

        ArrayList<DeptInfoV2> infoList = new ArrayList<>();

        File file = new File("data/emp.txt");
        Scanner scanner = new Scanner(file);

        while (scanner.hasNextLine()) {

            //判断入职日期消息2024年的
            String[] fields = scanner.nextLine().split(",");
            int year = Integer.parseInt(fields[2].split("-")[0]);
            if (year < 2024) {
                String deptName = fields[1];
                double salary = Double.parseDouble(fields[3]) + Double.parseDouble(fields[4]);
                //判断部门名称是否在deptList中
                int index =  indexOf(infoList, deptName); //自定义查找的方法
                if (index == -1) {
                    index = infoList.size();
                    DeptInfoV2 info = new DeptInfoV2();
                    info.name = deptName; //将部分名赋值
                    infoList.add(info);
                }
                DeptInfoV2 info = infoList.get(index);
                info.sumSal = info.sumSal + salary;
                info.maxSal = Math.max(info.maxSal, salary);
                info.minSal = Math.min(info.minSal != 0.0 ? info.minSal : salary, salary);
                info.amount = info.amount + 1;

            }

        }

        for (int i = 0; i < infoList.size(); i++) {
            DeptInfoV2 deptInfo = infoList.get(i);
            System.out.println("部门名称：" + deptInfo.name + " 总薪水：" + deptInfo.sumSal + " , 最大薪水：" + deptInfo.maxSal + " , 最小薪水：" + deptInfo.minSal + " , 平均薪水：" + deptInfo.sumSal / deptInfo.amount);
        }



        scanner.close();


    }

    private static int indexOf(ArrayList<DeptInfoV2> infoList, String deptName) {
        int index = -1;
        for (int i = 0; i < infoList.size(); i++) {
            DeptInfoV2 info = infoList.get(i);
            if (deptName.equals(info.name)) { //根据DeptInfoV2中保存的部门名称，与传进来的部门名称进行比较
                index = i;
                break;
            }
        }
        return index;
    }
}

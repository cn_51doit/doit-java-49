package day05;

import day04.DeptInfo;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class T07_SalaryCountV2 {

    public static void main(String[] args) throws FileNotFoundException {

        //用于保存部门名称的List
        ArrayList<String> deptList = new ArrayList<>();

        //用来保存部门信息的List
        ArrayList<DeptInfo> infoList = new ArrayList<>();

        File file = new File("data/emp.txt");
        Scanner scanner = new Scanner(file);

        while (scanner.hasNextLine()) {

            //判断入职日期消息2024年的
            String[] fields = scanner.nextLine().split(",");
            int year = Integer.parseInt(fields[2].split("-")[0]);
            if (year < 2024) {
                String deptName = fields[1];
                double salary = Double.parseDouble(fields[3]) + Double.parseDouble(fields[4]);
                //判断部门名称是否在deptList中
                int index = deptList.indexOf(deptName);
                if (index == -1) {
                    //该部门名称不在deptList中
                    index = deptList.size();
                    deptList.add(deptName);
                    //创建一个DeptInfo对象，保存对应部门的信息
                    DeptInfo deptInfo = new DeptInfo(); //deptInfo的地址 DeptInfo@xx668888
                    //将默认的deptInfo添加到infoList
                    infoList.add(index, deptInfo);
                }
                //根据index到infoList中查找DeptInfo
                DeptInfo info = infoList.get(index); //info的地址 DeptInfo@xx668888
                info.sumSal = info.sumSal + salary;
                info.maxSal = Math.max(info.maxSal, salary);
                info.minSal = Math.min(info.minSal != 0.0 ? info.minSal : salary, salary);
                info.amount = info.amount + 1;

                //没有将deptInfo set 到 infoList
            }

        }

        for (int i = 0; i < deptList.size(); i++) {
            DeptInfo deptInfo = infoList.get(i);
            System.out.println("部门名称：" + deptList.get(i) + " 总薪水：" + deptInfo.sumSal + " , 最大薪水：" + deptInfo.maxSal + " , 最小薪水：" + deptInfo.minSal + " , 平均薪水：" + deptInfo.sumSal / deptInfo.amount);
        }


        scanner.close();


    }
}

package day05;

import java.util.ArrayList;
import java.util.Collections;

public class T02_ArrayListDemo2 {

    public static void main(String[] args) {

        ArrayList<Integer> lst = new ArrayList<Integer>();

        lst.add(1);
        lst.add(5);
        lst.add(2);
        //lst.add("spark"); //错误，在new ArrayList时，使用泛型约束了该List里面可以存储的数据类型
        lst.add(4);
        lst.add(3);
        lst.add(5); // 里面可以添加重复的元素

        for(int i = 0; i < lst.size(); i++) {
            Integer num = lst.get(i);//调用get方法取出对应下标的元素
            System.out.println(num);
        }

        Integer res = lst.get(5);
        System.out.println(res);

        


    }
}

package com.doitedu.test;


import day10.People;

public class TestPeople {

    public static void main(String[] args) {

        //私有的属性和方法，在不同的包中无法访问（使用）
        People p = new People();

        //p.printName(); //报错误，因为该方法用private修饰了

        //String n = p.getName(); //public 修饰的方法
        //System.out.println(n);

        //p.setHeight(188.88);

        //double height = p.getHeight();
        //System.out.println(height);

        p.setAge(18);

        int a = p.getAge();
        System.out.println(a);


        People p2 = new People();
        p2.setAge(20);
        System.out.println(p2.getAge());

    }
}

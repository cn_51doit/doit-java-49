package day12;

/**
 * 自定义类型实现比较的两种方法
 * 第一种：让自定义的类实现Comparable接口
 *
 * 排序规则，先按照颜值升序排序，如果颜值相当，在颜值年龄升序排序
 */
public class BoyV2 implements Comparable<BoyV2>{

    private String name;
    private int age;
    private double fv;

    public BoyV2() {}

    public BoyV2(String name, int age, double fv) {
        this.name = name;
        this.age = age;
        this.fv = fv;
    }

    @Override
    public int compareTo(BoyV2 o) {
        if (this.fv == o.fv) {
            //如果颜值相等，在比较年龄
//            if (this.age > o.age) {
//                return 1;
//            } else if (this.age < o.age) {
//                return -1;
//            } else {
//                return 0;
//            }
            return this.age - o.age;
        } else {
           //如果颜值不相当，直接按照颜值比较
           if (this.fv > o.fv) {
               return 1;
           } else if (this.fv < o.fv) {
               return -1;
           } else {
               return 0;
           }
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public double getFv() {
        return fv;
    }

    public void setFv(double fv) {
        this.fv = fv;
    }
}

package day12;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class T08_SortDemo3 {

    public static void main(String[] args) throws FileNotFoundException {

        ArrayList<BoyV3> boyList = new ArrayList<>();

        File file = new File("data/boy.txt");
        Scanner scanner = new Scanner(file);

        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            //将读出来的数据进行切分
            String[] fields = line.split(",");
            //将字段转成相应的类型，通过构造方法传参
            BoyV3 boy = new BoyV3(fields[0], Integer.parseInt(fields[1]), Double.parseDouble(fields[2]));
            //将封装好的boy，添加到ArrayList中
            boyList.add(boy);
        }

        //排序，按照ArrayList中实际保存的类型，进行排序
        Collections.sort(boyList);

        for (int index = 0; index < boyList.size(); index++) {
            BoyV3 boy = boyList.get(index);
            System.out.println("排名：" + (index + 1) + ", 姓名：" + boy.getName() + ", 颜值：" + boy.getFv() + ", 年龄：" + boy.getAge());
        }

    }
}

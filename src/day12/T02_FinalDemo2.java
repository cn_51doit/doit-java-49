package day12;

public class T02_FinalDemo2 {

    public static void main(String[] args) {

        Son son = new Son();
        int num = son.num;

        System.out.println(num);

        int num1 = son.getNum();
        System.out.println(num1);


        //final int a = 10;
        final int a;
        a = 10;
        //a = 20; 报错


    }
}

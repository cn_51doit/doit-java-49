package day12;

public class T04_InterfaceDemo {

    public static void main(String[] args) {

        Monkey monkey = new Monkey();

        monkey.breath(); //从Animal继承的
        monkey.eat(); //自己实现的

        monkey.fight(); //从Fightable实现的

        monkey.fly(); //从Flyable实现的


    }
}

package day12;

import java.util.HashMap;

/**
 *  用final修身的引用类型，意味着地址不能被修改，但是类名的属性可以被修改
 */

public class T03_FinalDemo3 {

    public static void main(String[] args) {

        //自定义的类型为引用类型, 用final修身的引用类型，意味着地址不能被修改，但是类名的属性可以被修改
        final Girl girl = new Girl();

        girl.setAge(18);
        girl.setName("珊珊");

        System.out.println("girl1 : name" + girl.getName() + " , age:" + girl.getAge());

        girl.setAge(19);
        girl.setName("姗姗");

        System.out.println("girl2 : name" + girl.getName() + " , age:" + girl.getAge());

        //报错，再一次new 出来的girl，有新的的地址值，不能通过赋值预算符将值赋给用final修身的变量了
        //girl = new Girl();

        final HashMap<String, String> map1 = new HashMap<>();
        map1.put("aaa", "tom");
        map1.put("bbb", "jerry");

        map1.put("aaa", "tomcat");

        System.out.println(map1.get("aaa"));

        //map1 = new HashMap<>();
    }
}

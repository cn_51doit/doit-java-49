package day12;

/**
 * 多态指的是一个对象可以有多种形态
 * 多态的前提必要条件：
 * 1.继承或实现接口，父类引用指向子类对象，或接口指向实现类
 * 2.方法必须重写
 */
public class T05_TestPerson {

    public static void main(String[] args) {

        //Student student = new Student();
        //student.sing();

        //使用父类的类型，接送new出来的子类对象
        //你new的哪个对象的实例，就会调用那个实例的方法
        Person p = new Student();
        //表面上调用父类的方法
        p.sing();


        //不是多态，虽然父类引用指向了子类对象，但是没有方法的重写
        //Animal a = new Monkey();
        //a.breath();

        //Flyable f = new Monkey();
        //f.fly();

        Animal a = ZooFactory.getAnimal();
        a.breath();



    }
}

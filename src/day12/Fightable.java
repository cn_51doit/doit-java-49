package day12;

/**
 * 定义一个接口
 * 接口的作用是定义方法的标准（方法名称、参数列表、返回值类型）
 *
 */
public interface Fightable {

    public void fight(); //定义一个抽象方法
}

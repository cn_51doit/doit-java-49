package day12;

/**
 * 接口中，方法的访问权限默认都是public、并且都是abstract
 */
public interface Flyable {


    //public abstract void fly(); //抽象方法
    // abstract void fly();
    //void fly();

    //java 8后，接口可以有默认实现
    default void fly() {
        System.out.println("开飞机飞");
    }

}

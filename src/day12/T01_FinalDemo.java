package day12;

/**
 * 1.修饰变量后，该变量无法被修改
 */
public class T01_FinalDemo {

    //final意思为最终的，不能被修改
    private final int num = 1000;
    private String name = "tom";


    public static void main(String[] args) {

        T01_FinalDemo demo = new T01_FinalDemo();
        //demo.num = 1888; //final修身的变量，不能被修改

        System.out.println(demo.num);


    }
}

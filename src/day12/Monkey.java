package day12;

public class Monkey extends Animal implements Fightable, Flyable{

    @Override
    public void fight() {
        System.out.println("使用金箍棒干架！！！");
    }

    @Override
    public void fly() {
        System.out.println("乘筋斗云飞");
    }

    @Override
    public void eat() {
        System.out.println("喜欢吃桃子");
    }

    @Override
    public void breath() {
        System.out.println("猴子开始喘气了！！！");
    }
}

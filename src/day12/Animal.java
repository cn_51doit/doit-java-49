package day12;

public abstract class Animal {

    public String name;
    public int age;

    public void breath() {
        System.out.println("呼吸空间");
    }

    public abstract void eat();

}

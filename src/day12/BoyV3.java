package day12;

/**
 * 自定义类型实现比较的两种方法
 * 第一种：让自定义的类实现Comparable接口
 *
 * 排序规则，按照颜值的降序
 */
public class BoyV3 implements Comparable<BoyV3>{

    private String name;
    private int age;
    private double fv;

    public BoyV3() {}

    public BoyV3(String name, int age, double fv) {
        this.name = name;
        this.age = age;
        this.fv = fv;
    }

//    @Override
//    public int compareTo(BoyV3 o) {
//        if (this.fv > o.fv) {
//            return -1; //降序，大的放在前面
//        } else if (this.fv < o.fv) {
//            return 1; //降序，小的放在后面
//        } else {
//            return 0;
//        }
//    }



//    @Override
//    public int compareTo(BoyV3 o) {
//       //return -Double.compare(this.fv, o.fv);
//       return Double.compare(o.fv, this.fv); //颜值的降序
//    }


    /**
     * 先按照颜值的降序排序，如果颜值相等，再按照年龄的升序
     * @param o the object to be compared.
     * @return
     */
    @Override
    public int compareTo(BoyV3 o) {
        if (this.fv == o.fv) {
            //年龄的升序
            return this.age - o.age;
        } else {
            return -Double.compare(this.fv, o.fv);
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public double getFv() {
        return fv;
    }

    public void setFv(double fv) {
        this.fv = fv;
    }
}

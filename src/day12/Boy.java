package day12;

/**
 * 自定义类型实现比较的两种方法
 * 第一种：让自定义的类实现Comparable接口
 */
public class Boy implements Comparable<Boy>{

    private String name;
    private int age;
    private double fv;

    public Boy(){}

    public Boy(String name, int age, double fv) {
        this.name = name;
        this.age = age;
        this.fv = fv;
    }


    @Override
    public int compareTo(Boy o) {
        if (this.fv > o.fv) {
            return 1; //当前对象比传入的对象大，如果是升序，大的放在后面
        } else if (this.fv == o.fv) {
            return 0; //不用改变顺序
        } else {
            return -1; //当前对象比传入的对象小，如果是升序，小的放在前面
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public double getFv() {
        return fv;
    }

    public void setFv(double fv) {
        this.fv = fv;
    }

    //private final int num;

    //public Boy(int num) {
    //    this.num = num; //final修饰的变量也可以在构造方法中赋值
    //}
}

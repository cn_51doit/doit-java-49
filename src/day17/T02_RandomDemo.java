package day17;

import java.util.Random;
import java.util.Scanner;

/**
 * Random生成随机数
 */
public class T02_RandomDemo {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int bound = scanner.nextInt();

        Random random = new Random();

        int num = random.nextInt(bound);

        System.out.println(num);

    }

}

package day17;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class T12_CalenderDemo {

    public static void main(String[] args) throws ParseException {

        Calendar calendar = Calendar.getInstance();
        //2024-02-28
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date date = sdf.parse("2024-02-28");
        calendar.setTime(date); //设置Calendar基准时间

        //在原来的基础上，加2天
        calendar.add(Calendar.DATE, -2);

        //取出计算后的时间
        Date time = calendar.getTime();
        System.out.println(time);


    }
}

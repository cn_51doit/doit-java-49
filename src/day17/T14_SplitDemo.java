package day17;

import java.util.Arrays;

public class T14_SplitDemo {

    public static void main(String[] args) {

        //String str1 = "ab  a   bbb  abc   aa      c";

        //String str1 = "aaa   bbb";
        //String[] fields = str1.split(" +");


        String line = "spark,hive,flink;    hbase flink";

        // 将所有标点符号转换为正则表达式形式
        String regex = "[\\p{Punct}\\p{Space}]+";

        String[] words = line.split(regex);

        System.out.println(words.length);

        System.out.println(Arrays.toString(words));


        //ip地址按照.进行切分
        String str2 = "192.168.22.123";
        //特殊字符要使用两个反斜线进行转义，或者讲特殊字符放到中括号中
        //String[] fields = str2.split("\\."); //正则表达式中.是特殊字符，代表任意字符
        String[] fields = str2.split("[.]"); //正则表达式中.是特殊字符，代表任意字符
        System.out.println(Arrays.toString(fields));

    }
}

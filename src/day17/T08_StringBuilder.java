package day17;

public class T08_StringBuilder {

    public static void main(String[] args) {

        //
        StringBuilder sb = new StringBuilder();

        System.out.println(sb.capacity());

        sb.append("spark");
        sb.append("spark");
        sb.append("hive");
        sb.append("hive");
        sb.append("hive");

        System.out.println(sb.capacity());

        System.out.println(sb.toString());

        sb.insert(2, "hadoop");

        System.out.println(sb.toString());

        sb.delete(2, 4); //删除指定的字符串，前闭后开

        System.out.println(sb.toString());

        sb.reverse(); //字符串反转
        Object obj = (Object) sb;
        System.out.println(obj.toString());





    }
}

package day17;

public class T13_CopyArray {

    public static void main(String[] args) {

        int[] src = {1, 2, 3, 4, 5}; //源数组
        int[] dest = {6, 7, 8, 9, 0}; //目标数组
        //将源数组中从1索引开始，复制3个元素到目的数组中
        System.arraycopy(src, 1, dest, 0, 3);
        for (int i = 0; i < dest.length; i++) {
            System.out.println(dest[i]);
        }


    }
}

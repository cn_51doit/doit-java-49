package day17;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class T11_DateDemo {

    public static void main(String[] args) throws ParseException {

        long currentTime = System.currentTimeMillis();
        System.out.println(currentTime);
        Date date = new Date(currentTime);
        //Date date = new Date();
        System.out.println(date);

        long time = date.getTime();
        System.out.println(time);


        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        //将Date日期转成字符串
        String dtStr = sdf.format(date);
        System.out.println(dtStr);

        //将字符串转成Date
        Date date1 = sdf.parse(dtStr);
        System.out.println(date1);



    }
}

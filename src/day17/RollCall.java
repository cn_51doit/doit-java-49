package day17;

import java.util.Random;

public class RollCall {

    public static void main(String[] args) {

        Random random = new Random();

        String[] names = {"赵昆", "陈彦龙", "高超超", "扬天", "崔江涛", "高强", "柳迎新", "邵宪傑", "段立元", "成嘉", "姚宇"};

        int index = random.nextInt(11);

        String name = names[index];

        System.out.println(name);

    }
}

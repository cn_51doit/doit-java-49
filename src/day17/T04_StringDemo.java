package day17;


import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;

public class T04_StringDemo {

    public static void main(String[] args) {

        char[] cArr = {'h', 'e', 'l', 'l', 'o'};
        //通过字符数组构造一个字符串
        String word = new String(cArr);

        System.out.println(word);

        //将字符串转字节数组
        byte[] bArr = word.getBytes();
        char[] charArray = word.toCharArray();

        System.out.println(Arrays.toString(bArr));

        //通过字节数组构建一个字符串
        String line = new String(bArr, StandardCharsets.UTF_8);
        System.out.println(line);

        String w1 = "SpArk";
        String w2 = "spark";

        System.out.println(w1.equalsIgnoreCase(w2)); //比较内容并且忽略大小写

    }
}

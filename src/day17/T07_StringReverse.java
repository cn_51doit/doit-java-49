package day17;

public class T07_StringReverse {

    public static void main(String[] args) {

        String word = "spark";

        String res = "";
        for (int length = word.length() - 1; length >= 0; length--) {
           res += word.charAt(length);
        }
        System.out.println(res);

    }
}

package day17;

public class T10_EqualsDemo {

    public static void main(String[] args) {

        InfoBean bean1 = new InfoBean("tom", 18);
        InfoBean bean2 = new InfoBean("tom", 18);

        //System.out.println(bean1.equals(bean2)); //Object中equals方法默认是==,比的是地址值

        System.out.println(bean1.equals(bean2));

    }
}

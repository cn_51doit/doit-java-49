package day17;

import java.util.Random;

public class T03_RandomDemo2 {

    public static void main(String[] args) {

        Random random = new Random();
        //int num = random.nextInt(1000000);
        //System.out.println(num);
        //double d = random.nextDouble();
        //int r = (int) (d * 1000000);
        //System.out.println(r);


        int randomNumber = random.nextInt(1000000) + 100000; // 生成一个100000到999999之间的随机数
        System.out.println("六位随机整数: " + randomNumber);

    }
}

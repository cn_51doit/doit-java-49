package day17;

public class T09_ToStringDemo {

    public static void main(String[] args) {

        InfoBean bean = new InfoBean(null, 0);
        bean.setName("tom");
        bean.setAge(18);

        System.out.println(bean); //默认调用对象的toString方法
        //System.out.println(bean.toString());

    }
}

package day17;

public class T13_RegexDemo {

    public static void main(String[] args) {

        String phone = "18611132889";

        //正则表达式规则
        String regex = "1[3456789][0-9]{9}";

        //调用一个字符串的matches，然后传入指定的规则，进行匹配，如果返回ture，说法符合定义好的规则
        boolean flag = phone.matches(regex);

        System.out.println(flag);

        String regex2 = "^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,6}$";

        String email = "123456@qq.com";
        boolean flag2 = email.matches(regex2);
        System.out.println(flag2);

    }
}

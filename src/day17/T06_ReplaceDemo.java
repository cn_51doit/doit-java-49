package day17;

public class T06_ReplaceDemo {

    public static void main(String[] args) {

        String word = "hadoop";

        String result = word.replace('o', 'O');
        System.out.println(result); //hadOOp

        //18611132889 -> 186****2889

        String line = "spark,hive,flink,hadoop,hive";
        String[] words = line.split(",");
//        for (int index = 0; index < words.length; index++) {
//            System.out.println(words[index]);
//        }

        for (String w : words) {
            System.out.println(w);
        }
    }
}

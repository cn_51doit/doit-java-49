package day17;

import java.util.Scanner;

public class T01_ScannerDemo {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        //接收第一个字符串
        //String world = scanner.next();
        //System.out.println(world);

        String line = scanner.nextLine();
        System.out.println(line);

    }
}

package day17;

import java.util.Objects;

public class InfoBean {

    private String name;

    private int age;

    public InfoBean(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

//    @Override
//    public String toString() {
//        return "name :" +name + ", age: "+ age;
//    }


    @Override
    public String toString() {
        return "InfoBean{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }

//    @Override
//    public boolean equals(Object obj) {
//        //第一种情况，同一个对象，内存地址一样
//        if (this == obj) {
//            return true;
//        }
//        if (obj == null) {
//            return false;
//        }
//        //instanceof是否属于同一种类型，如果是相同类型或子类型，返回true
//        //如果两个对象的类型一样，并且数据值相同，返回ture
//        if (obj instanceof InfoBean) {
//            InfoBean bean = (InfoBean) obj;
//            return this.name.equals(bean.name) && this.age == bean.age;
//        }
//        return false;
//    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        InfoBean bean = (InfoBean) o;
        return age == bean.age && Objects.equals(name, bean.name);
    }

    @Override
    public int hashCode() {
        return 0;
    }
}

package day17;

/**
 *  equals方法和==的区别
 */
public class T05_StringDemo2 {

    public static void main(String[] args) {

        String s1 = new String("abc");

        String s2 = new String("abc");

        System.out.println(s1.equals(s2)); //true

        System.out.println(s1 == s2); //比的是地址值


        String s3 = "spark";
        String s4 = "spark";

        System.out.println(s3.equals(s4));
        System.out.println(s3 == s4); // s3 和 s4使用的是用一个地址的字符串（字符串池）


        String concat = s1.concat(s4);
        //s1 += s4;
        System.out.println(concat);
        //System.out.println(s1);

    }
}
